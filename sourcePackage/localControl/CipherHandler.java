package com.sermatec.sunmate.localControl;

import android.content.Intent;
import com.sermatec.sunmate.App;
import com.sermatec.sunmate.localControl.protocol.ProtocolException;
import com.sermatec.sunmate.localControl.protocol.RC4Utils;
import d.h.a.d.j.n;
import d.h.a.d.j.o0;
import d.h.a.d.j.p;
import d.h.a.d.j.q0;
import d.h.a.d.j.u;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import io.netty.util.concurrent.GenericFutureListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/* loaded from: classes.dex */
public class CipherHandler extends ChannelDuplexHandler {

    /* renamed from: b  reason: collision with root package name */
    public boolean f161b;

    /* renamed from: c  reason: collision with root package name */
    public Future<?> f162c;

    /* renamed from: d  reason: collision with root package name */
    public boolean f163d;

    /* renamed from: f  reason: collision with root package name */
    public ConnectionManager f165f;
    public volatile ChannelHandlerContext g;
    public Future<?> j;
    public List<String> k;
    public AtomicBoolean h = new AtomicBoolean(false);
    public final AtomicBoolean i = new AtomicBoolean(false);
    public volatile QueryState l = QueryState.NORMAL_QUERY;

    /* renamed from: e  reason: collision with root package name */
    public u f164e = (u) o0.a("osim", q0.f682a);

    /* renamed from: a  reason: collision with root package name */
    public List<String> f160a = Collections.singletonList("98");

    /* loaded from: classes.dex */
    public enum QueryState {
        NORMAL_QUERY,
        SPECIFIC_QUERY
    }

    /* loaded from: classes.dex */
    public class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        public int f166a = 0;

        /* renamed from: com.sermatec.sunmate.localControl.CipherHandler$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public class C0008a implements ChannelFutureListener {

            /* renamed from: a  reason: collision with root package name */
            public final /* synthetic */ String f168a;

            public C0008a(String str) {
                this.f168a = str;
            }

            /* renamed from: a */
            public void operationComplete(ChannelFuture channelFuture) {
                if (channelFuture.isSuccess()) {
                    String str = "query command " + this.f168a + " operate success.";
                    return;
                }
                String str2 = "query command " + this.f168a + " operate fail.";
                CipherHandler.this.f165f.i(channelFuture.cause());
            }
        }

        public a() {
        }

        @Override // java.lang.Runnable
        public void run() {
            List list = CipherHandler.this.k;
            int i = this.f166a;
            this.f166a = i + 1;
            String str = (String) list.get(i);
            try {
                try {
                    p pVar = CipherHandler.this.f164e.d().get(str);
                    if (pVar != null) {
                        CipherHandler.this.g.writeAndFlush(pVar.j()).addListener((GenericFutureListener<? extends io.netty.util.concurrent.Future<? super Void>>) new C0008a(str));
                    }
                } catch (ProtocolException e2) {
                    throw new RuntimeException(e2);
                }
            } finally {
                if (this.f166a == CipherHandler.this.k.size()) {
                    this.f166a = 0;
                }
            }
        }
    }

    /* loaded from: classes.dex */
    public class b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        public int f170a;

        /* renamed from: b  reason: collision with root package name */
        public int f171b = 0;

        /* loaded from: classes.dex */
        public class a implements ChannelFutureListener {

            /* renamed from: a  reason: collision with root package name */
            public final /* synthetic */ String f173a;

            public a(String str) {
                this.f173a = str;
            }

            /* renamed from: a */
            public void operationComplete(ChannelFuture channelFuture) {
                if (channelFuture.isSuccess()) {
                    String str = "query command " + this.f173a + " operate success.";
                    return;
                }
                String str2 = "query command " + this.f173a + " operate fail.";
                CipherHandler.this.f165f.i(channelFuture.cause());
            }
        }

        public b() {
        }

        @Override // java.lang.Runnable
        public void run() {
            List list = CipherHandler.this.f160a;
            int i = this.f171b;
            this.f171b = i + 1;
            String str = (String) list.get(i);
            if (!CipherHandler.this.f163d) {
                int i2 = this.f170a;
                if (i2 == 2) {
                    String str2 = "after twice query, didn't receive explicit data, change to encrypt way to query. remote " + CipherHandler.this.g.channel().remoteAddress();
                    CipherHandler.this.f161b = true;
                } else if (i2 == 4) {
                    String str3 = "after twice encrypt query, didn't receive encrypt data, so closing this channel. remote " + CipherHandler.this.g.channel().remoteAddress();
                    CipherHandler.this.f162c.cancel(false);
                    CipherHandler.this.f165f.i(new Exception("after twice encrypt query, didn't receive encrypt data"));
                    return;
                }
                this.f170a++;
            }
            try {
                try {
                    CipherHandler.this.g.writeAndFlush(CipherHandler.this.f164e.d().get(str).j()).addListener((GenericFutureListener<? extends io.netty.util.concurrent.Future<? super Void>>) new a(str));
                } catch (ProtocolException e2) {
                    throw new RuntimeException(e2);
                }
            } finally {
                if (this.f171b == CipherHandler.this.f160a.size()) {
                    this.f171b = 0;
                }
            }
        }
    }

    /* loaded from: classes.dex */
    public static /* synthetic */ class c {

        /* renamed from: a  reason: collision with root package name */
        public static final /* synthetic */ int[] f175a;

        static {
            int[] iArr = new int[QueryState.values().length];
            f175a = iArr;
            try {
                iArr[QueryState.NORMAL_QUERY.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                f175a[QueryState.SPECIFIC_QUERY.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
        }
    }

    public CipherHandler(ConnectionManager connectionManager) {
        this.f165f = connectionManager;
    }

    @Override // io.netty.channel.ChannelInboundHandlerAdapter, io.netty.channel.ChannelInboundHandler
    public final void channelActive(ChannelHandlerContext channelHandlerContext) {
        this.g = channelHandlerContext;
        m();
        channelHandlerContext.fireChannelActive();
    }

    @Override // io.netty.channel.ChannelInboundHandlerAdapter, io.netty.channel.ChannelInboundHandler
    public void channelInactive(ChannelHandlerContext channelHandlerContext) {
        this.g = null;
        this.f165f.i(new Exception("connect reset"));
        q();
        super.channelInactive(channelHandlerContext);
    }

    public final void i() {
        if (this.h.compareAndSet(true, false)) {
            this.f162c.cancel(false);
        }
    }

    public final void j() {
        if (this.i.compareAndSet(true, false)) {
            this.j.cancel(false);
        }
    }

    public final ByteBuf k() {
        HashMap hashMap = new HashMap();
        Calendar calendar = Calendar.getInstance();
        hashMap.put("year", String.valueOf(calendar.get(1)));
        hashMap.put("month", String.valueOf(calendar.get(2) + 1));
        hashMap.put("day", String.valueOf(calendar.get(5)));
        hashMap.put("hour", String.valueOf(calendar.get(11)));
        hashMap.put("minute", String.valueOf(calendar.get(12)));
        hashMap.put("second", String.valueOf(calendar.get(13)));
        hashMap.put("commandType", "68");
        return new n(this.f164e, null, hashMap).b().c();
    }

    public boolean l() {
        int i = c.f175a[this.l.ordinal()];
        if (i == 1) {
            return this.h.get();
        }
        if (i != 2) {
            return false;
        }
        return this.i.get();
    }

    public void m() {
        if (this.g != null && this.h.compareAndSet(false, true)) {
            this.l = QueryState.NORMAL_QUERY;
            j();
            this.f162c = this.g.executor().scheduleWithFixedDelay((Runnable) new b(), 0L, 600L, TimeUnit.MILLISECONDS);
        }
    }

    public void n(List<String> list) {
        if (this.f163d && this.g != null && list != null && list.size() > 0 && this.i.compareAndSet(false, true)) {
            this.k = new ArrayList(list);
            this.l = QueryState.SPECIFIC_QUERY;
            i();
            this.j = this.g.executor().scheduleWithFixedDelay((Runnable) new a(), 0L, 600L, TimeUnit.MILLISECONDS);
        }
    }

    public boolean o(boolean z) {
        boolean z2 = this.f161b;
        this.f161b = z;
        return z2;
    }

    public void p() {
        int i = c.f175a[this.l.ordinal()];
        if (i == 1) {
            m();
        } else if (i == 2) {
            n(this.k);
        }
    }

    public void q() {
        int i = c.f175a[this.l.ordinal()];
        if (i == 1) {
            i();
        } else if (i == 2) {
            j();
        }
    }

    public void r() {
        q();
        this.l = QueryState.NORMAL_QUERY;
    }

    @Override // io.netty.channel.ChannelInboundHandlerAdapter, io.netty.channel.ChannelInboundHandler
    public void userEventTriggered(ChannelHandlerContext channelHandlerContext, Object obj) {
        if (obj instanceof d.h.a.d.c) {
            this.f164e = ((d.h.a.d.c) obj).b();
            this.f160a = new ArrayList(this.f164e.e());
            this.f163d = true;
            this.f165f.m(this.f164e);
            this.f165f.l();
            App.a().sendBroadcast(new Intent("conn_ok"));
            channelHandlerContext.writeAndFlush(k());
        } else if ((obj instanceof d.h.a.d.b) && ((d.h.a.d.b) obj).b()) {
            this.f162c.cancel(true);
        }
        channelHandlerContext.fireUserEventTriggered(obj);
    }

    @Override // io.netty.channel.ChannelDuplexHandler, io.netty.channel.ChannelOutboundHandler
    public void write(ChannelHandlerContext channelHandlerContext, Object obj, ChannelPromise channelPromise) {
        if (!(obj instanceof ByteBuf) || !this.f161b) {
            channelHandlerContext.write(obj, channelPromise);
            return;
        }
        ByteBuf byteBuf = (ByteBuf) obj;
        byte[] bArr = new byte[byteBuf.readableBytes()];
        byteBuf.readBytes(bArr);
        byteBuf.readerIndex(0);
        byteBuf.writerIndex(0);
        byteBuf.writeBytes(RC4Utils.b(bArr));
        channelHandlerContext.write(byteBuf, channelPromise);
    }
}
