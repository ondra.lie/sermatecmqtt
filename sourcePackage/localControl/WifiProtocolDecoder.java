package com.sermatec.sunmate.localControl;

import android.util.Log;
import androidx.core.app.NotificationCompat;
import com.sermatec.sunmate.localControl.protocol.RC4Utils;
import d.h.a.d.f;
import d.h.a.d.g;
import d.h.a.d.h;
import d.h.a.d.j.a0;
import d.h.a.d.j.k0;
import d.h.a.d.j.l0;
import d.h.a.d.j.o;
import d.h.a.d.j.o0;
import d.h.a.d.j.p0;
import d.h.a.d.j.q;
import d.h.a.d.j.q0;
import d.h.a.d.j.v;
import e.b.l;
import e.b.m;
import e.b.n;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.concurrent.GenericFutureListener;
import io.netty.util.internal.logging.MessageFormatter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/* loaded from: classes.dex */
public class WifiProtocolDecoder extends ChannelInboundHandlerAdapter {

    /* renamed from: b  reason: collision with root package name */
    public boolean f187b;

    /* renamed from: c  reason: collision with root package name */
    public l0 f188c;

    /* renamed from: d  reason: collision with root package name */
    public l0 f189d;

    /* renamed from: f  reason: collision with root package name */
    public String f191f;

    /* renamed from: a  reason: collision with root package name */
    public c f186a = new c();

    /* renamed from: e  reason: collision with root package name */
    public l<Integer> f190e = l.b(this.f186a);
    public b g = new b(this, null);

    /* loaded from: classes.dex */
    public enum State {
        INITIAL,
        SEND_DATA,
        SEND_START,
        SEND_END,
        END
    }

    /* loaded from: classes.dex */
    public static /* synthetic */ class a {

        /* renamed from: a  reason: collision with root package name */
        public static final /* synthetic */ int[] f192a;

        static {
            int[] iArr = new int[State.values().length];
            f192a = iArr;
            try {
                iArr[State.INITIAL.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                f192a[State.SEND_DATA.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                f192a[State.SEND_START.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    /* loaded from: classes.dex */
    public class b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        public int f193a;

        public b() {
        }

        @Override // java.lang.Runnable
        public void run() {
            if (!WifiProtocolDecoder.this.f186a.g() && System.currentTimeMillis() - WifiProtocolDecoder.this.f186a.h > 30000) {
                int i = this.f193a;
                this.f193a = i + 1;
                if (i < 3) {
                    try {
                        WifiProtocolDecoder.this.f186a.l();
                    } catch (Exception e2) {
                        WifiProtocolDecoder.this.f186a.f(false, e2.getMessage());
                    }
                } else {
                    WifiProtocolDecoder.this.f186a.f(false, "machine ack timeout.");
                }
            }
        }

        public /* synthetic */ b(WifiProtocolDecoder wifiProtocolDecoder, a aVar) {
            this();
        }
    }

    /* loaded from: classes.dex */
    public class c implements n<Integer>, GenericFutureListener {

        /* renamed from: b  reason: collision with root package name */
        public Future f196b;
        public String j;
        public HashMap<String, Object> k;
        public RandomAccessFile m;
        public ChannelHandlerContext n;
        public byte[] o;
        public m<Integer> p;

        /* renamed from: a  reason: collision with root package name */
        public final String f195a = "UpgradeStateProcessor";

        /* renamed from: c  reason: collision with root package name */
        public int f197c = 0;

        /* renamed from: d  reason: collision with root package name */
        public int f198d = 0;

        /* renamed from: e  reason: collision with root package name */
        public int f199e = 0;

        /* renamed from: f  reason: collision with root package name */
        public byte[] f200f = new byte[2];
        public byte[] g = new byte[2];
        public long h = System.currentTimeMillis();
        public State i = State.END;
        public int l = 1;

        public c() {
        }

        @Override // e.b.n
        public void a(m<Integer> mVar) {
            this.p = mVar;
        }

        public final void e(ByteBuf byteBuf) {
            this.n.writeAndFlush(byteBuf).addListener((GenericFutureListener<? extends io.netty.util.concurrent.Future<? super Void>>) this);
        }

        public void f(boolean z, String str) {
            try {
                this.f196b.cancel(false);
                if (z) {
                    this.n.close();
                    String str2 = "upgrade success, close this channel. state -> " + this;
                    k(100);
                } else {
                    String str3 = "upgrade fail. reason -> " + str + ", state -> " + this;
                    CipherHandler cipherHandler = (CipherHandler) this.n.pipeline().get(CipherHandler.class);
                    if (cipherHandler != null) {
                        cipherHandler.o(WifiProtocolDecoder.this.f187b);
                    }
                    this.k.put(NotificationCompat.CATEGORY_STATUS, 0);
                    this.k.put(NotificationCompat.CATEGORY_MESSAGE, str);
                    m<Integer> mVar = this.p;
                    if (mVar != null) {
                        mVar.onError(new Exception(str));
                    }
                }
                this.k.put("end", new Date());
                this.i = State.END;
                this.k = null;
                this.n = null;
                RandomAccessFile randomAccessFile = this.m;
                if (randomAccessFile != null) {
                    randomAccessFile.close();
                }
            } catch (IOException e2) {
                Log.e("UpgradeStateProcessor", "close file error, when end upgradeProcessor", e2);
            }
        }

        public boolean g() {
            return this.i == State.END;
        }

        public void h() {
            WifiProtocolDecoder.this.g.f193a = 0;
            try {
                int i = a.f192a[this.i.ordinal()];
                if (i == 1) {
                    j();
                } else if (i == 2) {
                    m();
                } else if (i == 3) {
                    n();
                }
            } catch (Exception e2) {
                f(false, e2.getMessage());
            }
        }

        public void i(String str) {
            if ("1000".equals(this.j) && this.i == State.SEND_DATA && this.l == 1) {
                if (("002D".equals(str) && Arrays.equals(this.f200f, v.f714a)) || ("002A".equals(str) && Arrays.equals(this.g, v.f714a))) {
                    h();
                } else if ("00A5".equals(str)) {
                    f(false, "upgrade fail, ack: " + str);
                } else {
                    f(false, "version validate fail, ack: " + str);
                }
            } else if (!"002D".equals(str)) {
                f(false, "upgrade fail, ack: " + str);
            } else if (this.i == State.SEND_END) {
                f(true, null);
            } else {
                h();
            }
        }

        public final void j() {
            String str = this.j;
            str.hashCode();
            if (str.equals("1000")) {
                byte[] bArr = new byte[4];
                this.m.seek(98316L);
                this.m.read(bArr, 0, 2);
                this.m.seek(81932L);
                this.m.read(bArr, 2, 2);
                this.k.put("1000-0-0", bArr);
            } else if (str.equals("2000")) {
                this.k.put("2000-0-0", "0000");
            }
            p(this.j);
            this.i = State.SEND_DATA;
            k(1);
            this.m.seek(this.f197c);
        }

        public void k(int i) {
            m<Integer> mVar = this.p;
            if (mVar != null) {
                mVar.onNext(Integer.valueOf(i));
                if (i == 100) {
                    this.p.onComplete();
                }
            }
        }

        public final void l() {
            if (this.o != null) {
                ByteBuf ioBuffer = this.n.alloc().ioBuffer();
                ioBuffer.writeBytes(this.o);
                String str = "upgrade, retry send data -> " + o.b(this.o, ByteOrder.BIG_ENDIAN);
                e(ioBuffer);
            }
        }

        public final void m() {
            String s = o.s(this.l, 4);
            int min = (int) Math.min(this.m.length() - this.m.getFilePointer(), 256L);
            byte[] bArr = new byte[min];
            this.m.read(bArr);
            this.k.put("0001-0-0", bArr);
            this.k.put("realCommandType", s);
            p("0001");
            double filePointer = this.m.getFilePointer() - this.f197c;
            Double.isNaN(filePointer);
            double length = this.m.length() - this.f197c;
            Double.isNaN(length);
            k(Math.min(98, Math.max(2, (int) ((filePointer * 100.0d) / length))));
            if (min < 256) {
                this.i = State.SEND_START;
                return;
            }
            this.l++;
            this.i = State.SEND_DATA;
        }

        public final void n() {
            String str;
            String str2 = this.j;
            str2.hashCode();
            if (str2.equals("1000")) {
                this.m.seek(this.f198d);
                byte[] bArr = new byte[2];
                this.m.read(bArr);
                int b2 = a0.b(this.m, this.f197c);
                if (b2 != o.h(bArr, 0, ByteOrder.LITTLE_ENDIAN)) {
                    f(false, "before run command, validate fail.");
                    return;
                }
                byte[] bArr2 = new byte[4];
                this.m.seek(this.f199e);
                this.m.read(bArr2);
                this.k.put("E000-0-0", bArr2);
                this.k.put("E000-1-0", String.valueOf(this.m.length() - this.f197c));
                this.k.put("E000-2-0", String.valueOf(b2));
                str = "E000";
            } else if (!str2.equals("2000")) {
                str = null;
            } else {
                this.k.put("F000-0-0", String.valueOf(this.m.length()));
                this.k.put("F000-1-0", String.valueOf(a0.b(this.m, 0)));
                str = "F000";
            }
            p(str);
            this.i = State.SEND_END;
            k(99);
        }

        public void o(HashMap<String, Object> hashMap, Future future, ChannelHandlerContext channelHandlerContext) {
            try {
                this.i = State.INITIAL;
                this.f196b = future;
                this.n = channelHandlerContext;
                this.k = hashMap;
                this.j = (String) hashMap.get("commandType");
                this.l = 1;
                this.h = System.currentTimeMillis();
                this.m = new RandomAccessFile((String) hashMap.get("path"), "r");
                if ("1000".equals(this.j)) {
                    if (!"1".equals(WifiProtocolDecoder.this.f191f) && !"3".equals(WifiProtocolDecoder.this.f191f)) {
                        if (!"2".equals(WifiProtocolDecoder.this.f191f) && !"5".equals(WifiProtocolDecoder.this.f191f)) {
                            throw new Exception("Unknown deviceType");
                        }
                        this.f197c = ((Integer) hashMap.get("dataOffset")).intValue();
                        this.f199e = 81920;
                        this.f198d = 81926;
                        this.m.seek(81924L);
                        this.m.read(this.f200f);
                        this.m.seek(81932L);
                        this.m.read(this.g);
                        return;
                    }
                    this.f197c = 114688;
                    this.f199e = 98304;
                    this.f198d = 98310;
                    this.m.seek(98308L);
                    this.m.read(this.f200f);
                    this.m.seek(98316L);
                    this.m.read(this.g);
                    return;
                }
                this.f197c = 0;
                this.f199e = 0;
            } catch (Exception e2) {
                f(false, e2.getMessage());
            }
        }

        @Override // io.netty.util.concurrent.GenericFutureListener
        public void operationComplete(io.netty.util.concurrent.Future future) {
            if (!future.isSuccess()) {
                f(false, future.cause() != null ? future.cause().getMessage() : "command sent to machine fail.");
            }
        }

        public final void p(String str) {
            this.k.put("commandType", str);
            p0 b2 = new d.h.a.d.j.n(WifiProtocolDecoder.this.f189d, null, this.k).b();
            if (b2.a() == null) {
                ByteBuf c2 = b2.c();
                byte[] bArr = new byte[c2.readableBytes()];
                this.o = bArr;
                c2.getBytes(0, bArr);
                e(c2);
                return;
            }
            throw new Exception("command encode fail.", b2.a());
        }

        public String toString() {
            return "UpgradeStateProcessor{dataOffset=" + this.f197c + ", lastAckReceiveTimeStamp=" + this.h + ", state=" + this.i + ", commandType='" + this.j + "', currentUpgradeSeq=" + this.l + ", protocol=" + WifiProtocolDecoder.this.f189d.b() + MessageFormatter.DELIM_STOP;
        }
    }

    public WifiProtocolDecoder() {
        q0 q0Var = q0.f682a;
        this.f188c = o0.a("osim", q0Var);
        this.f189d = o0.a("osim-upgrade", q0Var);
    }

    @Override // io.netty.channel.ChannelInboundHandlerAdapter, io.netty.channel.ChannelInboundHandler
    public void channelRead(ChannelHandlerContext channelHandlerContext, Object obj) {
        l0 l0Var;
        if (obj instanceof h) {
            h<String, ByteBuf, Boolean> hVar = (h) obj;
            g<String, ByteBuf> f2 = f(hVar);
            if ("98".equals(q.c(f2.b()))) {
                ((CipherHandler) channelHandlerContext.pipeline().get(CipherHandler.class)).o(hVar.c().booleanValue());
            }
            try {
                if (f2.a().equals("osim-upgrade")) {
                    this.f186a.h = System.currentTimeMillis();
                    k0.m(f2.b());
                    l0Var = this.f189d;
                } else {
                    l0Var = this.f188c;
                }
                p0 a2 = new d.h.a.d.j.n(l0Var, f2.b(), null).a();
                if (a2.a() != null) {
                    Log.e("WifiProtocolDecoder", "decode fail, exception: " + a2.a().getMessage());
                    return;
                }
                Map<String, String> h = h(a2.b());
                if (!this.f186a.g() && f2.a().equals("osim-upgrade")) {
                    this.f186a.i(h.get("0000-0-0"));
                }
                channelHandlerContext.fireChannelRead(h);
            } finally {
                f2.b().release();
            }
        } else {
            channelHandlerContext.fireChannelRead(obj);
        }
    }

    public final g<String, ByteBuf> f(h<String, ByteBuf, Boolean> hVar) {
        if (!hVar.c().booleanValue()) {
            return new g<>(hVar.a(), hVar.b());
        }
        byte[] bArr = new byte[hVar.b().readableBytes()];
        hVar.b().readBytes(bArr);
        hVar.b().readerIndex(0);
        hVar.b().writerIndex(0);
        hVar.b().writeBytes(RC4Utils.a(bArr));
        return new g<>(hVar.a(), hVar.b());
    }

    public l<Integer> g() {
        return this.f190e;
    }

    public final Map<String, String> h(Map<String, Object> map) {
        HashMap hashMap = new HashMap();
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            hashMap.put(entry.getKey(), String.valueOf(entry.getValue()));
        }
        return hashMap;
    }

    @Override // io.netty.channel.ChannelInboundHandlerAdapter, io.netty.channel.ChannelInboundHandler
    public void userEventTriggered(ChannelHandlerContext channelHandlerContext, Object obj) {
        if (obj instanceof d.h.a.d.b) {
            d.h.a.d.b bVar = (d.h.a.d.b) obj;
            HashMap<String, Object> a2 = bVar.a();
            if (bVar.b()) {
                this.f187b = ((CipherHandler) channelHandlerContext.pipeline().get(CipherHandler.class)).o(false);
                this.f186a.o(a2, channelHandlerContext.executor().scheduleAtFixedRate((Runnable) this.g, 2000L, 2000L, TimeUnit.MILLISECONDS), channelHandlerContext);
                this.f186a.h();
            }
        } else if (obj instanceof d.h.a.d.c) {
            d.h.a.d.c cVar = (d.h.a.d.c) obj;
            String a3 = cVar.a();
            this.f191f = a3;
            f.d(a3);
            this.f188c = cVar.b();
            this.f189d = o0.a("osim-upgrade", cVar.c());
        } else {
            channelHandlerContext.fireUserEventTriggered(obj);
        }
    }
}
