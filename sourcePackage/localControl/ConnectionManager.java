package com.sermatec.sunmate.localControl;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.util.Log;
import androidx.databinding.BaseObservable;
import com.sermatec.sunmate.App;
import d.h.a.d.e;
import d.h.a.d.i;
import d.h.a.d.j.l0;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import java.util.List;

@SuppressLint({"RestrictedApi"})
/* loaded from: classes.dex */
public class ConnectionManager extends BaseObservable {

    /* renamed from: a  reason: collision with root package name */
    public static final ConnectionManager f176a = new ConnectionManager();

    /* renamed from: b  reason: collision with root package name */
    public Channel f177b;

    /* renamed from: c  reason: collision with root package name */
    public ChannelFuture f178c;
    public l0 g;

    /* renamed from: d  reason: collision with root package name */
    public EventLoopGroup f179d = new NioEventLoopGroup(2);

    /* renamed from: f  reason: collision with root package name */
    public volatile ConnectionStatus f181f = ConnectionStatus.UNCONNECTED;

    /* renamed from: e  reason: collision with root package name */
    public Bootstrap f180e = new Bootstrap().group(this.f179d).channel(NioSocketChannel.class).option(ChannelOption.SO_KEEPALIVE, Boolean.TRUE).option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 3000).handler(new a());

    /* loaded from: classes.dex */
    public enum ConnectionStatus {
        UNCONNECTED,
        CONNECTING,
        CONNECTED
    }

    /* loaded from: classes.dex */
    public class a extends ChannelInitializer<SocketChannel> {
        public a() {
        }

        /* renamed from: a */
        public void initChannel(SocketChannel socketChannel) {
            socketChannel.pipeline().addLast(new CipherHandler(ConnectionManager.this), new e(), new WifiProtocolDecoder(), new i());
        }
    }

    /* loaded from: classes.dex */
    public class b implements GenericFutureListener<Future<? super Void>> {
        public b() {
        }

        @Override // io.netty.util.concurrent.GenericFutureListener
        public void operationComplete(Future<? super Void> future) {
            if (!future.isSuccess()) {
                ConnectionManager.this.i(future.cause());
            }
        }
    }

    /* loaded from: classes.dex */
    public class c implements GenericFutureListener<ChannelFuture> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ boolean f184a;

        public c(boolean z) {
            this.f184a = z;
        }

        /* renamed from: a */
        public void operationComplete(ChannelFuture channelFuture) {
            if (channelFuture.isSuccess()) {
                ConnectionManager.this.f177b = channelFuture.channel();
                return;
            }
            if (!this.f184a) {
                App.a().sendBroadcast(new Intent("conn_no_condition"));
            }
            ConnectionManager.this.f181f = ConnectionStatus.UNCONNECTED;
            ConnectionManager.this.notifyChange();
        }
    }

    private ConnectionManager() {
    }

    public static ConnectionManager e() {
        return f176a;
    }

    /* JADX WARN: Type inference failed for: r4v2, types: [io.netty.channel.ChannelFuture] */
    public boolean c(boolean z) {
        if (this.f181f != ConnectionStatus.UNCONNECTED) {
            return false;
        }
        this.f181f = ConnectionStatus.CONNECTING;
        notifyChange();
        this.f178c = this.f180e.connect("10.10.100.254", 8899).addListener((GenericFutureListener<? extends Future<? super Void>>) new c(z));
        return true;
    }

    public Channel d() {
        return this.f177b;
    }

    public l0 f() {
        return this.g;
    }

    public boolean g() {
        return this.f181f == ConnectionStatus.CONNECTED;
    }

    public boolean h() {
        CipherHandler cipherHandler;
        if (this.f181f != ConnectionStatus.CONNECTED || (cipherHandler = (CipherHandler) this.f177b.pipeline().get(CipherHandler.class)) == null) {
            return false;
        }
        return cipherHandler.l();
    }

    public void i(Throwable th) {
        Log.e("ConnectionManager", th.getMessage(), th);
        if (this.f181f == ConnectionStatus.CONNECTED) {
            this.f181f = ConnectionStatus.UNCONNECTED;
            notifyChange();
            this.f177b.close();
            App.a().sendBroadcast(new Intent("conn_no"));
        } else if (this.f181f == ConnectionStatus.CONNECTING) {
            this.f181f = ConnectionStatus.UNCONNECTED;
            notifyChange();
            this.f177b.close();
            App.a().sendBroadcast(new Intent("conn_fail"));
        }
    }

    public void j(List<String> list) {
        CipherHandler cipherHandler;
        if (this.f181f == ConnectionStatus.CONNECTED && (cipherHandler = (CipherHandler) this.f177b.pipeline().get(CipherHandler.class)) != null) {
            cipherHandler.n(list);
        }
    }

    public ChannelFuture k(ByteBuf byteBuf) {
        if (this.f181f == ConnectionStatus.CONNECTED) {
            ChannelFuture writeAndFlush = this.f177b.writeAndFlush(byteBuf);
            writeAndFlush.addListener((GenericFutureListener<? extends Future<? super Void>>) new b());
            return writeAndFlush;
        }
        byteBuf.release();
        return null;
    }

    public void l() {
        this.f181f = ConnectionStatus.CONNECTED;
        notifyChange();
    }

    public void m(l0 l0Var) {
        this.g = l0Var;
    }

    public void n() {
        CipherHandler cipherHandler;
        if (this.f181f == ConnectionStatus.CONNECTED && (cipherHandler = (CipherHandler) this.f177b.pipeline().get(CipherHandler.class)) != null) {
            cipherHandler.p();
        }
    }

    public void o() {
        CipherHandler cipherHandler;
        if (this.f181f == ConnectionStatus.CONNECTED && (cipherHandler = (CipherHandler) this.f177b.pipeline().get(CipherHandler.class)) != null) {
            cipherHandler.m();
        }
    }

    public void p() {
        CipherHandler cipherHandler;
        if (this.f181f == ConnectionStatus.CONNECTED && (cipherHandler = (CipherHandler) this.f177b.pipeline().get(CipherHandler.class)) != null) {
            cipherHandler.q();
        }
    }

    public void q() {
        CipherHandler cipherHandler;
        if (this.f181f == ConnectionStatus.CONNECTED && (cipherHandler = (CipherHandler) this.f177b.pipeline().get(CipherHandler.class)) != null) {
            cipherHandler.r();
        }
    }
}
