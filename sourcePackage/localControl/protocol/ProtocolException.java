package com.sermatec.sunmate.localControl.protocol;

/* loaded from: classes.dex */
public class ProtocolException extends Exception {
    public ProtocolException(String str) {
        super(str);
    }

    public ProtocolException(String str, Throwable th) {
        super(str, th);
    }
}
