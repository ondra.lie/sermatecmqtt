package com.sermatec.sunmate.localControl.protocol;

import b.b.a.a;
import com.alibaba.fastjson.annotation.JSONField;
import d.h.a.d.j.p;
import io.netty.util.internal.logging.MessageFormatter;
import java.io.Serializable;
import java.nio.ByteOrder;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* loaded from: classes.dex */
public class Field implements Comparable<Field>, Serializable {
    private int act;
    private int bitPosition;
    private int byteLen;
    @JSONField(deserialize = false)
    private ByteOrder byteOrder;
    private p command;
    private String compatConverter;
    private String converter;
    private String defaultValue;
    private int endBit;
    private int fromBit;
    private int group;
    private String groupTag;
    private String name;
    private int order;
    private int precision;
    private int repeat;
    private int repeatGroup;
    private int repeatRef;
    private boolean same;
    private String tag;
    @JSONField(deserialize = false)
    private FieldType type;
    private String unitType;
    private float unitValue;
    private String validate;

    /* loaded from: classes.dex */
    public enum ACT {
        READ(1),
        WRITE(2);
        
        private int act;

        ACT(int i) {
            this.act = i;
        }
    }

    /* loaded from: classes.dex */
    public enum FieldType {
        INT("int"),
        U_INT("uInt"),
        BIT_RANGE("bitRange"),
        BIT("bit"),
        LONG("long"),
        PRESERVER("preserve"),
        HEX("hex"),
        STRING("string"),
        ONE_POSITION("onePosition"),
        ZERO_POSITION("zeroPosition"),
        BYTES("bytes");
        
        public static final Map<String, FieldType> FIELD_TYPE_MAP;
        private String name;

        static {
            FieldType fieldType = INT;
            FieldType fieldType2 = U_INT;
            FieldType fieldType3 = BIT_RANGE;
            FieldType fieldType4 = BIT;
            FieldType fieldType5 = LONG;
            FieldType fieldType6 = PRESERVER;
            FieldType fieldType7 = HEX;
            FieldType fieldType8 = STRING;
            FieldType fieldType9 = ONE_POSITION;
            FieldType fieldType10 = ZERO_POSITION;
            FieldType fieldType11 = BYTES;
            HashMap hashMap = new HashMap();
            hashMap.put(fieldType.name, fieldType);
            hashMap.put(fieldType3.name, fieldType3);
            hashMap.put(fieldType4.name, fieldType4);
            hashMap.put(fieldType5.name, fieldType5);
            hashMap.put(fieldType6.name, fieldType6);
            hashMap.put(fieldType7.name, fieldType7);
            hashMap.put(fieldType8.name, fieldType8);
            hashMap.put(fieldType9.name, fieldType9);
            hashMap.put(fieldType10.name, fieldType10);
            hashMap.put(fieldType2.name, fieldType2);
            hashMap.put(fieldType11.name, fieldType11);
            FIELD_TYPE_MAP = Collections.unmodifiableMap(hashMap);
        }

        FieldType(String str) {
            this.name = str;
        }
    }

    public Field(int i, int i2, String str, int i3, String str2, float f2, ByteOrder byteOrder, FieldType fieldType, int i4, int i5, int i6, boolean z, int i7, int i8, int i9, String str3, Integer num, String str4, String str5, String str6, int i10, String str7, String str8) {
        this.order = -1;
        this.byteLen = -1;
        this.unitValue = 1.0f;
        this.precision = 2;
        this.byteOrder = ByteOrder.BIG_ENDIAN;
        this.type = FieldType.INT;
        this.fromBit = -1;
        this.endBit = -1;
        this.bitPosition = -1;
        this.same = false;
        this.repeat = -1;
        this.repeatRef = -1;
        this.repeatGroup = -1;
        this.order = i;
        this.act = i2;
        this.name = str;
        int i11 = i3;
        this.byteLen = i11;
        this.unitType = str2;
        this.unitValue = f2;
        this.byteOrder = byteOrder;
        this.type = fieldType;
        this.fromBit = i4;
        this.endBit = i5;
        this.bitPosition = i6;
        this.same = z;
        this.repeat = i7;
        this.repeatRef = i8;
        this.repeatGroup = i9;
        this.validate = str3;
        this.group = num != null ? num.intValue() : i11;
        this.tag = str4;
        this.groupTag = str5;
        this.converter = str6;
        this.precision = i10;
        this.defaultValue = str7;
        this.compatConverter = str8;
    }

    public Field copy() {
        return new Field(this.order, this.act, this.name, this.byteLen, this.unitType, this.unitValue, this.byteOrder, this.type, this.fromBit, this.endBit, this.bitPosition, this.same, this.repeat, this.repeatRef, this.repeatGroup, this.validate, Integer.valueOf(this.group), this.tag, this.groupTag, this.converter, this.precision, this.defaultValue, this.compatConverter);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Field field = (Field) obj;
        return this.order == field.order && this.act == field.act && this.byteLen == field.byteLen && Float.compare(field.unitValue, this.unitValue) == 0 && this.fromBit == field.fromBit && this.endBit == field.endBit && this.bitPosition == field.bitPosition && this.same == field.same && this.repeat == field.repeat && this.repeatRef == field.repeatRef && this.repeatGroup == field.repeatGroup && this.group == field.group && this.name.equals(field.name) && a.a(this.unitType, field.unitType) && this.byteOrder.equals(field.byteOrder) && this.type == field.type && a.a(this.validate, field.validate) && a.a(this.tag, field.tag) && a.a(this.groupTag, field.groupTag) && a.a(this.converter, field.converter) && a.a(this.defaultValue, field.defaultValue) && a.a(this.compatConverter, field.compatConverter);
    }

    public String generateFieldKey() {
        return this.command.type() + "-" + this.order;
    }

    public String generateFieldKeyWithId(int i) {
        return this.command.type() + "-" + this.order + "-" + i;
    }

    public int getAct() {
        return this.act;
    }

    public int getBitPosition() {
        return this.bitPosition;
    }

    public int getByteLen() {
        return this.byteLen;
    }

    public ByteOrder getByteOrder() {
        return this.byteOrder;
    }

    public p getCommand() {
        return this.command;
    }

    public String getCompatConverter() {
        return this.compatConverter;
    }

    public String getConverter() {
        return this.converter;
    }

    public String getDefaultValue() {
        return this.defaultValue;
    }

    public int getEndBit() {
        return this.endBit;
    }

    public int getFromBit() {
        return this.fromBit;
    }

    public int getGroup() {
        return this.group;
    }

    public String getGroupTag() {
        return this.groupTag;
    }

    public String getName() {
        return this.name;
    }

    public int getOrder() {
        return this.order;
    }

    public int getPrecision() {
        return this.precision;
    }

    public int getRepeat() {
        return this.repeat;
    }

    public int getRepeatGroup() {
        return this.repeatGroup;
    }

    public int getRepeatRef() {
        return this.repeatRef;
    }

    public String getTag() {
        return this.tag;
    }

    public FieldType getType() {
        return this.type;
    }

    public String getUnitType() {
        return this.unitType;
    }

    public float getUnitValue() {
        return this.unitValue;
    }

    public String getValidate() {
        return this.validate;
    }

    public int hashCode() {
        return generateFieldKey().hashCode();
    }

    public boolean isSame() {
        return this.same;
    }

    public void setAct(int i) {
        this.act = i;
    }

    public void setBitPosition(int i) {
        this.bitPosition = i;
    }

    public void setByteLen(int i) {
        this.byteLen = i;
    }

    public void setByteOrder(ByteOrder byteOrder) {
        this.byteOrder = byteOrder;
    }

    public void setCommand(p pVar) {
        this.command = pVar;
    }

    public void setCompatConverter(String str) {
        this.compatConverter = str;
    }

    public void setConverter(String str) {
        this.converter = str;
    }

    public void setDefaultValue(String str) {
        this.defaultValue = str;
    }

    public void setEndBit(int i) {
        this.endBit = i;
    }

    public void setFromBit(int i) {
        this.fromBit = i;
    }

    public void setGroup(int i) {
        this.group = i;
    }

    public void setGroupTag(String str) {
        this.groupTag = str;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void setOrder(int i) {
        this.order = i;
    }

    public void setPrecision(int i) {
        this.precision = i;
    }

    public void setRepeat(int i) {
        this.repeat = i;
    }

    public void setRepeatGroup(int i) {
        this.repeatGroup = i;
    }

    public void setRepeatRef(int i) {
        this.repeatRef = i;
    }

    public void setSame(boolean z) {
        this.same = z;
    }

    public void setTag(String str) {
        this.tag = str;
    }

    public void setType(FieldType fieldType) {
        this.type = fieldType;
    }

    public void setUnitType(String str) {
        this.unitType = str;
    }

    public void setUnitValue(float f2) {
        this.unitValue = f2;
    }

    public void setValidate(String str) {
        this.validate = str;
    }

    public String toString() {
        return "Field{command=" + this.command + ", order=" + this.order + ", act=" + this.act + ", name='" + this.name + "', byteLen=" + this.byteLen + ", unitType='" + this.unitType + "', unitValue=" + this.unitValue + ", byteOrder=" + this.byteOrder + ", type=" + this.type + ", fromBit=" + this.fromBit + ", endBit=" + this.endBit + ", bitPosition=" + this.bitPosition + ", same=" + this.same + ", repeat=" + this.repeat + ", repeatDef=" + this.repeatRef + ", repeatGroup=" + this.repeatGroup + ", validate='" + this.validate + "', group='" + this.group + "', tag='" + this.tag + "', groupTag='" + this.groupTag + "', converter='" + this.converter + "', defaultValue='" + this.defaultValue + "', compatConverter='" + this.compatConverter + '\'' + MessageFormatter.DELIM_STOP;
    }

    public int compareTo(Field field) {
        return this.order - field.order;
    }

    public Field() {
        this.order = -1;
        this.byteLen = -1;
        this.unitValue = 1.0f;
        this.precision = 2;
        this.byteOrder = ByteOrder.BIG_ENDIAN;
        this.type = FieldType.INT;
        this.fromBit = -1;
        this.endBit = -1;
        this.bitPosition = -1;
        this.same = false;
        this.repeat = -1;
        this.repeatRef = -1;
        this.repeatGroup = -1;
    }
}
