package com.sermatec.sunmate.localControl.protocol;

import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;

/* loaded from: classes.dex */
public class RC4Utils {

    /* renamed from: a  reason: collision with root package name */
    public static final ThreadLocal<Cipher> f201a = new ThreadLocal<>();

    /* renamed from: b  reason: collision with root package name */
    public static final Key f202b = new SecretKey() { // from class: com.sermatec.sunmate.localControl.protocol.RC4Utils.1
        private final byte[] key = "Sermatec2019@Gsstes2019".getBytes();

        @Override // java.security.Key
        public String getAlgorithm() {
            return "RC4";
        }

        @Override // java.security.Key
        public byte[] getEncoded() {
            return this.key;
        }

        @Override // java.security.Key
        public String getFormat() {
            return "RAW";
        }
    };

    public static byte[] a(byte[] bArr) {
        Cipher c2 = c();
        if (c2 == null) {
            return null;
        }
        try {
            c2.init(2, f202b);
            return c2.doFinal(bArr);
        } catch (Exception e2) {
            throw new RuntimeException("InvalidKeyException", e2);
        }
    }

    public static byte[] b(byte[] bArr) {
        Cipher c2 = c();
        if (c2 == null) {
            return null;
        }
        try {
            c2.init(1, f202b);
            return c2.doFinal(bArr);
        } catch (Exception e2) {
            throw new RuntimeException("InvalidKeyException", e2);
        }
    }

    public static Cipher c() {
        ThreadLocal<Cipher> threadLocal = f201a;
        Cipher cipher = threadLocal.get();
        if (cipher != null) {
            return cipher;
        }
        try {
            Cipher cipher2 = Cipher.getInstance("RC4");
            threadLocal.set(cipher2);
            return cipher2;
        } catch (Exception e2) {
            throw new RuntimeException("Algorithm RC4 not exists", e2);
        }
    }
}
