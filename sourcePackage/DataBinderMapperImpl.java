package com.sermatec.sunmate;

import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.DataBinderMapper;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import com.sermatec.sunmate.databinding.BatDataFragmentBindingImpl;
import com.sermatec.sunmate.databinding.FragmentHomeBindingImpl;
import com.sermatec.sunmate.databinding.GridDataFragmentBindingImpl;
import com.sermatec.sunmate.databinding.LoadDataFragmentBindingImpl;
import com.sermatec.sunmate.databinding.ParallelDataFragmentBindingImpl;
import com.sermatec.sunmate.databinding.PvDataFragmentBindingImpl;
import com.sermatec.sunmate.databinding.SysDataFragmentBindingImpl;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* loaded from: classes.dex */
public class DataBinderMapperImpl extends DataBinderMapper {

    /* renamed from: a  reason: collision with root package name */
    public static final SparseIntArray f103a;

    /* loaded from: classes.dex */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        public static final SparseArray<String> f104a;

        static {
            SparseArray<String> sparseArray = new SparseArray<>(2);
            f104a = sparseArray;
            sparseArray.put(0, "_all");
            sparseArray.put(1, "data");
        }
    }

    /* loaded from: classes.dex */
    public static class b {

        /* renamed from: a  reason: collision with root package name */
        public static final HashMap<String, Integer> f105a;

        static {
            HashMap<String, Integer> hashMap = new HashMap<>(7);
            f105a = hashMap;
            hashMap.put("layout/bat_data_fragment_0", Integer.valueOf((int) R.layout.bat_data_fragment));
            hashMap.put("layout/fragment_home_0", Integer.valueOf((int) R.layout.fragment_home));
            hashMap.put("layout/grid_data_fragment_0", Integer.valueOf((int) R.layout.grid_data_fragment));
            hashMap.put("layout/load_data_fragment_0", Integer.valueOf((int) R.layout.load_data_fragment));
            hashMap.put("layout/parallel_data_fragment_0", Integer.valueOf((int) R.layout.parallel_data_fragment));
            hashMap.put("layout/pv_data_fragment_0", Integer.valueOf((int) R.layout.pv_data_fragment));
            hashMap.put("layout/sys_data_fragment_0", Integer.valueOf((int) R.layout.sys_data_fragment));
        }
    }

    static {
        SparseIntArray sparseIntArray = new SparseIntArray(7);
        f103a = sparseIntArray;
        sparseIntArray.put(R.layout.bat_data_fragment, 1);
        sparseIntArray.put(R.layout.fragment_home, 2);
        sparseIntArray.put(R.layout.grid_data_fragment, 3);
        sparseIntArray.put(R.layout.load_data_fragment, 4);
        sparseIntArray.put(R.layout.parallel_data_fragment, 5);
        sparseIntArray.put(R.layout.pv_data_fragment, 6);
        sparseIntArray.put(R.layout.sys_data_fragment, 7);
    }

    @Override // androidx.databinding.DataBinderMapper
    public List<DataBinderMapper> collectDependencies() {
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(new androidx.databinding.library.baseAdapters.DataBinderMapperImpl());
        return arrayList;
    }

    @Override // androidx.databinding.DataBinderMapper
    public String convertBrIdToString(int i) {
        return a.f104a.get(i);
    }

    @Override // androidx.databinding.DataBinderMapper
    public ViewDataBinding getDataBinder(DataBindingComponent dataBindingComponent, View view, int i) {
        int i2 = f103a.get(i);
        if (i2 <= 0) {
            return null;
        }
        Object tag = view.getTag();
        if (tag != null) {
            switch (i2) {
                case 1:
                    if ("layout/bat_data_fragment_0".equals(tag)) {
                        return new BatDataFragmentBindingImpl(dataBindingComponent, view);
                    }
                    throw new IllegalArgumentException("The tag for bat_data_fragment is invalid. Received: " + tag);
                case 2:
                    if ("layout/fragment_home_0".equals(tag)) {
                        return new FragmentHomeBindingImpl(dataBindingComponent, view);
                    }
                    throw new IllegalArgumentException("The tag for fragment_home is invalid. Received: " + tag);
                case 3:
                    if ("layout/grid_data_fragment_0".equals(tag)) {
                        return new GridDataFragmentBindingImpl(dataBindingComponent, view);
                    }
                    throw new IllegalArgumentException("The tag for grid_data_fragment is invalid. Received: " + tag);
                case 4:
                    if ("layout/load_data_fragment_0".equals(tag)) {
                        return new LoadDataFragmentBindingImpl(dataBindingComponent, view);
                    }
                    throw new IllegalArgumentException("The tag for load_data_fragment is invalid. Received: " + tag);
                case 5:
                    if ("layout/parallel_data_fragment_0".equals(tag)) {
                        return new ParallelDataFragmentBindingImpl(dataBindingComponent, view);
                    }
                    throw new IllegalArgumentException("The tag for parallel_data_fragment is invalid. Received: " + tag);
                case 6:
                    if ("layout/pv_data_fragment_0".equals(tag)) {
                        return new PvDataFragmentBindingImpl(dataBindingComponent, view);
                    }
                    throw new IllegalArgumentException("The tag for pv_data_fragment is invalid. Received: " + tag);
                case 7:
                    if ("layout/sys_data_fragment_0".equals(tag)) {
                        return new SysDataFragmentBindingImpl(dataBindingComponent, view);
                    }
                    throw new IllegalArgumentException("The tag for sys_data_fragment is invalid. Received: " + tag);
                default:
                    return null;
            }
        } else {
            throw new RuntimeException("view must have a tag");
        }
    }

    @Override // androidx.databinding.DataBinderMapper
    public int getLayoutId(String str) {
        Integer num;
        if (str == null || (num = b.f105a.get(str)) == null) {
            return 0;
        }
        return num.intValue();
    }

    @Override // androidx.databinding.DataBinderMapper
    public ViewDataBinding getDataBinder(DataBindingComponent dataBindingComponent, View[] viewArr, int i) {
        if (viewArr == null || viewArr.length == 0 || f103a.get(i) <= 0 || viewArr[0].getTag() != null) {
            return null;
        }
        throw new RuntimeException("view must have a tag");
    }
}
