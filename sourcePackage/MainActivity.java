package com.sermatec.sunmate;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.motion.widget.Key;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.view.GravityCompat;
import androidx.databinding.Observable;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.sermatec.sunmate.MainActivity;
import com.sermatec.sunmate.localControl.ConnectionManager;
import com.sermatec.sunmate.ui.data.DataViewModel;
import com.sermatec.sunmate.ui.notifications.Notifications;
import com.sermatec.sunmate.ui.system.LanguageFragment;
import d.h.a.d.d;
import d.h.a.d.i;
import e.b.l;
import e.b.y.g;
import java.util.concurrent.TimeUnit;

/* loaded from: classes.dex */
public class MainActivity extends AppCompatActivity {

    /* renamed from: a  reason: collision with root package name */
    public NavController f106a;

    /* renamed from: b  reason: collision with root package name */
    public AppBarConfiguration f107b;

    /* renamed from: c  reason: collision with root package name */
    public DrawerLayout f108c;

    /* renamed from: d  reason: collision with root package name */
    public LinearLayout f109d;

    /* renamed from: e  reason: collision with root package name */
    public Observable.OnPropertyChangedCallback f110e;

    /* renamed from: f  reason: collision with root package name */
    public TextView f111f;
    public ObjectAnimator g;
    public boolean h;
    public MenuItem i;
    public e.b.v.b j;

    /* loaded from: classes.dex */
    public class a implements View.OnClickListener {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ NavController f112a;

        public a(NavController navController) {
            this.f112a = navController;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            this.f112a.navigateUp();
        }
    }

    /* loaded from: classes.dex */
    public class b implements NavController.OnDestinationChangedListener {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ TextView f114a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ AppCompatImageView f115b;

        public b(TextView textView, AppCompatImageView appCompatImageView) {
            this.f114a = textView;
            this.f115b = appCompatImageView;
        }

        @Override // androidx.navigation.NavController.OnDestinationChangedListener
        public void onDestinationChanged(@NonNull NavController navController, @NonNull NavDestination navDestination, @Nullable Bundle bundle) {
            this.f114a.setText(navDestination.getLabel());
            if (navDestination.getId() == R.id.sys_setting) {
                this.f115b.setVisibility(8);
            } else {
                this.f115b.setVisibility(0);
            }
        }
    }

    /* loaded from: classes.dex */
    public class c extends Observable.OnPropertyChangedCallback {
        public c() {
        }

        /* JADX INFO: Access modifiers changed from: private */
        /* renamed from: a */
        public /* synthetic */ void b() {
            if (ConnectionManager.e().g()) {
                MainActivity.this.f109d.setVisibility(8);
                MainActivity.this.h = false;
                if (MainActivity.this.g != null) {
                    MainActivity.this.g.cancel();
                    MainActivity.this.g = null;
                    return;
                }
                return;
            }
            MainActivity.this.f109d.setVisibility(0);
            MainActivity.this.h = true;
            if (MainActivity.this.g == null) {
                MainActivity.this.f();
            }
        }

        @Override // androidx.databinding.Observable.OnPropertyChangedCallback
        public void onPropertyChanged(Observable observable, int i) {
            MainActivity.this.runOnUiThread(new Runnable() { // from class: d.h.a.a
                @Override // java.lang.Runnable
                public final void run() {
                    MainActivity.c.this.b();
                }
            });
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: g */
    public /* synthetic */ void h() {
        if (this.h) {
            int measuredWidth = ((View) this.f111f.getParent()).getMeasuredWidth();
            TextView textView = this.f111f;
            ObjectAnimator ofFloat = ObjectAnimator.ofFloat(textView, Key.TRANSLATION_X, measuredWidth, -textView.getMeasuredWidth());
            ofFloat.setInterpolator(new LinearInterpolator());
            ofFloat.setRepeatCount(-1);
            ofFloat.setDuration(3000L);
            ofFloat.start();
            this.g = ofFloat;
            this.h = false;
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: i */
    public /* synthetic */ void j(Long l) {
        if (ConnectionManager.e().g() && this.i != null) {
            i iVar = (i) ConnectionManager.e().d().pipeline().get(i.class);
            if (iVar == null || (iVar.f().isEmpty() && iVar.d().isEmpty())) {
                Drawable mutate = this.i.getIcon().mutate();
                DrawableCompat.setTint(mutate, getResources().getColor(R.color.colorLighter));
                this.i.setIcon(mutate);
                return;
            }
            Drawable mutate2 = this.i.getIcon().mutate();
            DrawableCompat.setTint(mutate2, getResources().getColor(17170454));
            this.i.setIcon(mutate2);
        }
    }

    public final void f() {
        this.f111f.post(new Runnable() { // from class: d.h.a.b
            @Override // java.lang.Runnable
            public final void run() {
                MainActivity.this.h();
            }
        });
    }

    @Override // androidx.activity.ComponentActivity, android.app.Activity
    public void onBackPressed() {
        if (getOnBackPressedDispatcher().hasEnabledCallbacks()) {
            super.onBackPressed();
        } else if (isTaskRoot()) {
            moveTaskToBack(false);
        } else {
            finish();
        }
    }

    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }
        LanguageFragment.b(this);
        d.b(this);
        i.i(this);
        i.k((DataViewModel) new ViewModelProvider(this).get(DataViewModel.class));
        setContentView(R.layout.activity_main);
        setSupportActionBar((Toolbar) findViewById(R.id.my_toolbar));
        this.f108c = (DrawerLayout) findViewById(R.id.draw_layout);
        this.f107b = new AppBarConfiguration.Builder(R.id.navigation_home, R.id.navigation_data, R.id.navigation_report, R.id.navigation_control).setOpenableLayout(this.f108c).build();
        NavController findNavController = Navigation.findNavController(this, R.id.nav_host_fragment);
        this.f106a = findNavController;
        NavigationUI.setupActionBarWithNavController(this, findNavController, this.f107b);
        NavigationUI.setupWithNavController((BottomNavigationView) findViewById(R.id.nav_view), this.f106a);
        NavController findNavController2 = Navigation.findNavController(this, R.id.left_nav_host_fragment);
        AppCompatImageView appCompatImageView = (AppCompatImageView) findViewById(R.id.left_back);
        appCompatImageView.setOnClickListener(new a(findNavController2));
        findNavController2.addOnDestinationChangedListener(new b((TextView) findViewById(R.id.left_title), appCompatImageView));
        this.f111f = (TextView) findViewById(R.id.connectTip);
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.connectTipContainer);
        this.f109d = linearLayout;
        linearLayout.setBackgroundColor(Color.argb(76, 0, 0, 0));
        this.f110e = new c();
        ConnectionManager.e().addOnPropertyChangedCallback(this.f110e);
        ConnectionManager.e().notifyChange();
        this.j = l.f(1L, TimeUnit.SECONDS).g(e.b.u.c.a.a()).i(new g() { // from class: d.h.a.c
            @Override // e.b.y.g
            public final void accept(Object obj) {
                MainActivity.this.j((Long) obj);
            }
        });
    }

    @Override // android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        this.i = menu.getItem(0);
        return super.onCreateOptionsMenu(menu);
    }

    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onDestroy() {
        ConnectionManager.e().removeOnPropertyChangedCallback(this.f110e);
        this.j.dispose();
        super.onDestroy();
    }

    @Override // android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == 16908332 && this.f107b.getTopLevelDestinations().contains(Integer.valueOf(this.f106a.getCurrentDestination().getId()))) {
            int drawerLockMode = this.f108c.getDrawerLockMode(GravityCompat.START);
            if (this.f108c.isDrawerVisible(GravityCompat.START) && drawerLockMode != 2) {
                this.f108c.closeDrawer(GravityCompat.START);
            } else if (drawerLockMode != 1) {
                this.f108c.openDrawer(GravityCompat.START);
            }
            return true;
        } else if (menuItem.getItemId() != R.id.warn) {
            return false;
        } else {
            startActivity(new Intent(this, Notifications.class));
            return true;
        }
    }

    @Override // androidx.appcompat.app.AppCompatActivity
    public boolean onSupportNavigateUp() {
        return Navigation.findNavController(this, R.id.nav_host_fragment).navigateUp() || super.onSupportNavigateUp();
    }
}
