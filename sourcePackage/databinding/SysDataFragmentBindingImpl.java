package com.sermatec.sunmate.databinding;

import android.util.SparseIntArray;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import com.sermatec.sunmate.R;
import com.sermatec.sunmate.ui.data.DataViewModel;

/* loaded from: classes.dex */
public class SysDataFragmentBindingImpl extends SysDataFragmentBinding {
    @Nullable
    public static final ViewDataBinding.IncludedLayouts h = null;
    @Nullable
    public static final SparseIntArray i;
    @NonNull
    public final NestedScrollView j;
    @NonNull
    public final AppCompatTextView k;
    @NonNull
    public final AppCompatTextView l;
    @NonNull
    public final AppCompatTextView m;
    @NonNull
    public final AppCompatTextView n;
    public long o;

    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        i = sparseIntArray;
        sparseIntArray.put(R.id.device_parallel_state, 10);
    }

    public SysDataFragmentBindingImpl(@Nullable DataBindingComponent dataBindingComponent, @NonNull View view) {
        this(dataBindingComponent, view, ViewDataBinding.mapBindings(dataBindingComponent, view, 11, h, i));
    }

    @Override // com.sermatec.sunmate.databinding.SysDataFragmentBinding
    public void c(@Nullable DataViewModel dataViewModel) {
        this.g = dataViewModel;
        synchronized (this) {
            this.o |= 512;
        }
        notifyPropertyChanged(1);
        super.requestRebind();
    }

    public final boolean d(DataViewModel.CustomObservableField customObservableField, int i2) {
        if (i2 != 0) {
            return false;
        }
        synchronized (this) {
            this.o |= 64;
        }
        return true;
    }

    public final boolean e(DataViewModel.CustomObservableField customObservableField, int i2) {
        if (i2 != 0) {
            return false;
        }
        synchronized (this) {
            this.o |= 4;
        }
        return true;
    }

    /* JADX WARN: Removed duplicated region for block: B:19:0x0047  */
    /* JADX WARN: Removed duplicated region for block: B:28:0x0063  */
    /* JADX WARN: Removed duplicated region for block: B:37:0x007f  */
    /* JADX WARN: Removed duplicated region for block: B:46:0x009b  */
    /* JADX WARN: Removed duplicated region for block: B:55:0x00b7  */
    /* JADX WARN: Removed duplicated region for block: B:64:0x00d3  */
    /* JADX WARN: Removed duplicated region for block: B:73:0x00ef  */
    /* JADX WARN: Removed duplicated region for block: B:82:0x010d  */
    @Override // androidx.databinding.ViewDataBinding
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void executeBindings() {
        /*
            Method dump skipped, instructions count: 411
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sermatec.sunmate.databinding.SysDataFragmentBindingImpl.executeBindings():void");
    }

    public final boolean f(DataViewModel.CustomObservableField customObservableField, int i2) {
        if (i2 != 0) {
            return false;
        }
        synchronized (this) {
            this.o |= 1;
        }
        return true;
    }

    public final boolean g(DataViewModel.CustomObservableField customObservableField, int i2) {
        if (i2 != 0) {
            return false;
        }
        synchronized (this) {
            this.o |= 128;
        }
        return true;
    }

    public final boolean h(DataViewModel.CustomObservableField customObservableField, int i2) {
        if (i2 != 0) {
            return false;
        }
        synchronized (this) {
            this.o |= 8;
        }
        return true;
    }

    @Override // androidx.databinding.ViewDataBinding
    public boolean hasPendingBindings() {
        synchronized (this) {
            return this.o != 0;
        }
    }

    public final boolean i(DataViewModel.CustomObservableField customObservableField, int i2) {
        if (i2 != 0) {
            return false;
        }
        synchronized (this) {
            this.o |= 32;
        }
        return true;
    }

    @Override // androidx.databinding.ViewDataBinding
    public void invalidateAll() {
        synchronized (this) {
            this.o = 1024L;
        }
        requestRebind();
    }

    public final boolean j(DataViewModel.CustomObservableField customObservableField, int i2) {
        if (i2 != 0) {
            return false;
        }
        synchronized (this) {
            this.o |= 16;
        }
        return true;
    }

    public final boolean k(DataViewModel.CustomObservableField customObservableField, int i2) {
        if (i2 != 0) {
            return false;
        }
        synchronized (this) {
            this.o |= 256;
        }
        return true;
    }

    public final boolean l(DataViewModel.CustomObservableField customObservableField, int i2) {
        if (i2 != 0) {
            return false;
        }
        synchronized (this) {
            this.o |= 2;
        }
        return true;
    }

    @Override // androidx.databinding.ViewDataBinding
    public boolean onFieldChange(int i2, Object obj, int i3) {
        switch (i2) {
            case 0:
                return f((DataViewModel.CustomObservableField) obj, i3);
            case 1:
                return l((DataViewModel.CustomObservableField) obj, i3);
            case 2:
                return e((DataViewModel.CustomObservableField) obj, i3);
            case 3:
                return h((DataViewModel.CustomObservableField) obj, i3);
            case 4:
                return j((DataViewModel.CustomObservableField) obj, i3);
            case 5:
                return i((DataViewModel.CustomObservableField) obj, i3);
            case 6:
                return d((DataViewModel.CustomObservableField) obj, i3);
            case 7:
                return g((DataViewModel.CustomObservableField) obj, i3);
            case 8:
                return k((DataViewModel.CustomObservableField) obj, i3);
            default:
                return false;
        }
    }

    @Override // androidx.databinding.ViewDataBinding
    public boolean setVariable(int i2, @Nullable Object obj) {
        if (1 != i2) {
            return false;
        }
        c((DataViewModel) obj);
        return true;
    }

    public SysDataFragmentBindingImpl(DataBindingComponent dataBindingComponent, View view, Object[] objArr) {
        super(dataBindingComponent, view, 9, (AppCompatTextView) objArr[1], (AppCompatTextView) objArr[2], (LinearLayoutCompat) objArr[10], (AppCompatTextView) objArr[5], (AppCompatTextView) objArr[4], (AppCompatTextView) objArr[3]);
        this.o = -1L;
        this.f154a.setTag(null);
        this.f155b.setTag(null);
        this.f157d.setTag(null);
        NestedScrollView nestedScrollView = (NestedScrollView) objArr[0];
        this.j = nestedScrollView;
        nestedScrollView.setTag(null);
        AppCompatTextView appCompatTextView = (AppCompatTextView) objArr[6];
        this.k = appCompatTextView;
        appCompatTextView.setTag(null);
        AppCompatTextView appCompatTextView2 = (AppCompatTextView) objArr[7];
        this.l = appCompatTextView2;
        appCompatTextView2.setTag(null);
        AppCompatTextView appCompatTextView3 = (AppCompatTextView) objArr[8];
        this.m = appCompatTextView3;
        appCompatTextView3.setTag(null);
        AppCompatTextView appCompatTextView4 = (AppCompatTextView) objArr[9];
        this.n = appCompatTextView4;
        appCompatTextView4.setTag(null);
        this.f158e.setTag(null);
        this.f159f.setTag(null);
        setRootTag(view);
        invalidateAll();
    }
}
