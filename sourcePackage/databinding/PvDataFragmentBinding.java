package com.sermatec.sunmate.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.sermatec.sunmate.R;
import com.sermatec.sunmate.ui.data.DataViewModel;

/* loaded from: classes.dex */
public abstract class PvDataFragmentBinding extends ViewDataBinding {
    @NonNull

    /* renamed from: a  reason: collision with root package name */
    public final AppCompatTextView f148a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final AppCompatTextView f149b;
    @NonNull

    /* renamed from: c  reason: collision with root package name */
    public final AppCompatTextView f150c;
    @NonNull

    /* renamed from: d  reason: collision with root package name */
    public final AppCompatTextView f151d;
    @NonNull

    /* renamed from: e  reason: collision with root package name */
    public final AppCompatTextView f152e;
    @NonNull

    /* renamed from: f  reason: collision with root package name */
    public final AppCompatTextView f153f;
    @NonNull
    public final AppCompatTextView g;
    @NonNull
    public final AppCompatTextView h;
    @NonNull
    public final AppCompatTextView i;
    @NonNull
    public final AppCompatTextView j;
    @NonNull
    public final AppCompatTextView k;
    @NonNull
    public final AppCompatTextView l;
    @Bindable
    public DataViewModel m;

    public PvDataFragmentBinding(Object obj, View view, int i, AppCompatTextView appCompatTextView, AppCompatTextView appCompatTextView2, AppCompatTextView appCompatTextView3, AppCompatTextView appCompatTextView4, AppCompatTextView appCompatTextView5, AppCompatTextView appCompatTextView6, AppCompatTextView appCompatTextView7, AppCompatTextView appCompatTextView8, AppCompatTextView appCompatTextView9, AppCompatTextView appCompatTextView10, AppCompatTextView appCompatTextView11, AppCompatTextView appCompatTextView12) {
        super(obj, view, i);
        this.f148a = appCompatTextView;
        this.f149b = appCompatTextView2;
        this.f150c = appCompatTextView3;
        this.f151d = appCompatTextView4;
        this.f152e = appCompatTextView5;
        this.f153f = appCompatTextView6;
        this.g = appCompatTextView7;
        this.h = appCompatTextView8;
        this.i = appCompatTextView9;
        this.j = appCompatTextView10;
        this.k = appCompatTextView11;
        this.l = appCompatTextView12;
    }

    @NonNull
    public static PvDataFragmentBinding a(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, boolean z) {
        return b(layoutInflater, viewGroup, z, DataBindingUtil.getDefaultComponent());
    }

    @NonNull
    @Deprecated
    public static PvDataFragmentBinding b(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, boolean z, @Nullable Object obj) {
        return (PvDataFragmentBinding) ViewDataBinding.inflateInternal(layoutInflater, R.layout.pv_data_fragment, viewGroup, z, obj);
    }

    public abstract void c(@Nullable DataViewModel dataViewModel);
}
