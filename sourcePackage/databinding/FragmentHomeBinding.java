package com.sermatec.sunmate.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.sermatec.sunmate.R;
import com.sermatec.sunmate.ui.MainPicture;
import com.sermatec.sunmate.ui.data.DataViewModel;
import com.youth.banner.Banner;

/* loaded from: classes.dex */
public abstract class FragmentHomeBinding extends ViewDataBinding {
    @NonNull

    /* renamed from: a  reason: collision with root package name */
    public final Banner f124a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final ImageView f125b;
    @NonNull

    /* renamed from: c  reason: collision with root package name */
    public final ImageView f126c;
    @NonNull

    /* renamed from: d  reason: collision with root package name */
    public final ImageView f127d;
    @NonNull

    /* renamed from: e  reason: collision with root package name */
    public final ImageView f128e;
    @NonNull

    /* renamed from: f  reason: collision with root package name */
    public final TextView f129f;
    @NonNull
    public final TextView g;
    @NonNull
    public final TextView h;
    @NonNull
    public final TextView i;
    @NonNull
    public final MainPicture j;
    @Bindable
    public DataViewModel k;

    public FragmentHomeBinding(Object obj, View view, int i, Banner banner, ImageView imageView, ImageView imageView2, ImageView imageView3, ImageView imageView4, TextView textView, TextView textView2, TextView textView3, TextView textView4, MainPicture mainPicture) {
        super(obj, view, i);
        this.f124a = banner;
        this.f125b = imageView;
        this.f126c = imageView2;
        this.f127d = imageView3;
        this.f128e = imageView4;
        this.f129f = textView;
        this.g = textView2;
        this.h = textView3;
        this.i = textView4;
        this.j = mainPicture;
    }

    @NonNull
    public static FragmentHomeBinding a(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, boolean z) {
        return b(layoutInflater, viewGroup, z, DataBindingUtil.getDefaultComponent());
    }

    @NonNull
    @Deprecated
    public static FragmentHomeBinding b(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, boolean z, @Nullable Object obj) {
        return (FragmentHomeBinding) ViewDataBinding.inflateInternal(layoutInflater, R.layout.fragment_home, viewGroup, z, obj);
    }

    public abstract void c(@Nullable DataViewModel dataViewModel);
}
