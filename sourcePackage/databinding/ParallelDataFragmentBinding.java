package com.sermatec.sunmate.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.sermatec.sunmate.R;
import com.sermatec.sunmate.ui.data.DataViewModel;

/* loaded from: classes.dex */
public abstract class ParallelDataFragmentBinding extends ViewDataBinding {
    @NonNull

    /* renamed from: a  reason: collision with root package name */
    public final AppCompatTextView f142a;
    @Bindable

    /* renamed from: b  reason: collision with root package name */
    public DataViewModel f143b;

    public ParallelDataFragmentBinding(Object obj, View view, int i, AppCompatTextView appCompatTextView) {
        super(obj, view, i);
        this.f142a = appCompatTextView;
    }

    @NonNull
    public static ParallelDataFragmentBinding a(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, boolean z) {
        return b(layoutInflater, viewGroup, z, DataBindingUtil.getDefaultComponent());
    }

    @NonNull
    @Deprecated
    public static ParallelDataFragmentBinding b(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, boolean z, @Nullable Object obj) {
        return (ParallelDataFragmentBinding) ViewDataBinding.inflateInternal(layoutInflater, R.layout.parallel_data_fragment, viewGroup, z, obj);
    }

    public abstract void c(@Nullable DataViewModel dataViewModel);
}
