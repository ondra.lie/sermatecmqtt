package com.sermatec.sunmate.databinding;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import com.sermatec.sunmate.R;
import com.sermatec.sunmate.ui.MainPicture;
import com.sermatec.sunmate.ui.data.DataViewModel;
import com.youth.banner.Banner;

/* loaded from: classes.dex */
public class FragmentHomeBindingImpl extends FragmentHomeBinding {
    @Nullable
    public static final ViewDataBinding.IncludedLayouts l = null;
    @Nullable
    public static final SparseIntArray m;
    @NonNull
    public final ScrollView n;
    @NonNull
    public final TextView o;
    @NonNull
    public final TextView p;
    @NonNull
    public final TextView q;
    @NonNull
    public final TextView r;
    @NonNull
    public final TextView s;
    @NonNull
    public final TextView t;
    public long u;

    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        m = sparseIntArray;
        sparseIntArray.put(R.id.banner, 9);
        sparseIntArray.put(R.id.mainPicture, 10);
        sparseIntArray.put(R.id.imageForPv, 11);
        sparseIntArray.put(R.id.imageForGrid, 12);
        sparseIntArray.put(R.id.imageForLoad, 13);
        sparseIntArray.put(R.id.loadEnergyUnit, 14);
        sparseIntArray.put(R.id.loadEnergyUnit2, 15);
        sparseIntArray.put(R.id.imageForFee, 16);
    }

    public FragmentHomeBindingImpl(@Nullable DataBindingComponent dataBindingComponent, @NonNull View view) {
        this(dataBindingComponent, view, ViewDataBinding.mapBindings(dataBindingComponent, view, 17, l, m));
    }

    @Override // com.sermatec.sunmate.databinding.FragmentHomeBinding
    public void c(@Nullable DataViewModel dataViewModel) {
        this.k = dataViewModel;
        synchronized (this) {
            this.u |= 256;
        }
        notifyPropertyChanged(1);
        super.requestRebind();
    }

    public final boolean d(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.u |= 128;
        }
        return true;
    }

    public final boolean e(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.u |= 8;
        }
        return true;
    }

    /* JADX WARN: Removed duplicated region for block: B:19:0x0045  */
    /* JADX WARN: Removed duplicated region for block: B:28:0x0061  */
    /* JADX WARN: Removed duplicated region for block: B:37:0x007d  */
    /* JADX WARN: Removed duplicated region for block: B:46:0x0099  */
    /* JADX WARN: Removed duplicated region for block: B:55:0x00b5  */
    /* JADX WARN: Removed duplicated region for block: B:64:0x00d1  */
    /* JADX WARN: Removed duplicated region for block: B:73:0x00ef  */
    @Override // androidx.databinding.ViewDataBinding
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void executeBindings() {
        /*
            Method dump skipped, instructions count: 367
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sermatec.sunmate.databinding.FragmentHomeBindingImpl.executeBindings():void");
    }

    public final boolean f(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.u |= 4;
        }
        return true;
    }

    public final boolean g(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.u |= 64;
        }
        return true;
    }

    public final boolean h(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.u |= 2;
        }
        return true;
    }

    @Override // androidx.databinding.ViewDataBinding
    public boolean hasPendingBindings() {
        synchronized (this) {
            return this.u != 0;
        }
    }

    public final boolean i(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.u |= 1;
        }
        return true;
    }

    @Override // androidx.databinding.ViewDataBinding
    public void invalidateAll() {
        synchronized (this) {
            this.u = 512L;
        }
        requestRebind();
    }

    public final boolean j(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.u |= 32;
        }
        return true;
    }

    public final boolean k(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.u |= 16;
        }
        return true;
    }

    @Override // androidx.databinding.ViewDataBinding
    public boolean onFieldChange(int i, Object obj, int i2) {
        switch (i) {
            case 0:
                return i((DataViewModel.CustomObservableField) obj, i2);
            case 1:
                return h((DataViewModel.CustomObservableField) obj, i2);
            case 2:
                return f((DataViewModel.CustomObservableField) obj, i2);
            case 3:
                return e((DataViewModel.CustomObservableField) obj, i2);
            case 4:
                return k((DataViewModel.CustomObservableField) obj, i2);
            case 5:
                return j((DataViewModel.CustomObservableField) obj, i2);
            case 6:
                return g((DataViewModel.CustomObservableField) obj, i2);
            case 7:
                return d((DataViewModel.CustomObservableField) obj, i2);
            default:
                return false;
        }
    }

    @Override // androidx.databinding.ViewDataBinding
    public boolean setVariable(int i, @Nullable Object obj) {
        if (1 != i) {
            return false;
        }
        c((DataViewModel) obj);
        return true;
    }

    public FragmentHomeBindingImpl(DataBindingComponent dataBindingComponent, View view, Object[] objArr) {
        super(dataBindingComponent, view, 8, (Banner) objArr[9], (ImageView) objArr[16], (ImageView) objArr[12], (ImageView) objArr[13], (ImageView) objArr[11], (TextView) objArr[5], (TextView) objArr[14], (TextView) objArr[15], (TextView) objArr[6], (MainPicture) objArr[10]);
        this.u = -1L;
        this.f129f.setTag(null);
        this.i.setTag(null);
        ScrollView scrollView = (ScrollView) objArr[0];
        this.n = scrollView;
        scrollView.setTag(null);
        TextView textView = (TextView) objArr[1];
        this.o = textView;
        textView.setTag(null);
        TextView textView2 = (TextView) objArr[2];
        this.p = textView2;
        textView2.setTag(null);
        TextView textView3 = (TextView) objArr[3];
        this.q = textView3;
        textView3.setTag(null);
        TextView textView4 = (TextView) objArr[4];
        this.r = textView4;
        textView4.setTag(null);
        TextView textView5 = (TextView) objArr[7];
        this.s = textView5;
        textView5.setTag(null);
        TextView textView6 = (TextView) objArr[8];
        this.t = textView6;
        textView6.setTag(null);
        setRootTag(view);
        invalidateAll();
    }
}
