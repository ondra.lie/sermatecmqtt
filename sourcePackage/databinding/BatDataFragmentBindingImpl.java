package com.sermatec.sunmate.databinding;

import android.util.SparseIntArray;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import com.sermatec.sunmate.R;
import com.sermatec.sunmate.ui.data.DataViewModel;

/* loaded from: classes.dex */
public class BatDataFragmentBindingImpl extends BatDataFragmentBinding {
    @Nullable
    public static final ViewDataBinding.IncludedLayouts v = null;
    @Nullable
    public static final SparseIntArray w;
    @NonNull
    public final NestedScrollView x;
    @NonNull
    public final AppCompatTextView y;
    public long z;

    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        w = sparseIntArray;
        sparseIntArray.put(R.id.batVolUnit, 13);
        sparseIntArray.put(R.id.batCurUnit, 14);
        sparseIntArray.put(R.id.batTemUnit, 15);
        sparseIntArray.put(R.id.batSocUnit, 16);
        sparseIntArray.put(R.id.batSohUnit, 17);
        sparseIntArray.put(R.id.batMaxCCurUnit, 18);
        sparseIntArray.put(R.id.batMaxDCurUnit, 19);
        sparseIntArray.put(R.id.batECVolUnit, 20);
        sparseIntArray.put(R.id.batEDVolUnit, 21);
    }

    public BatDataFragmentBindingImpl(@Nullable DataBindingComponent dataBindingComponent, @NonNull View view) {
        this(dataBindingComponent, view, ViewDataBinding.mapBindings(dataBindingComponent, view, 22, v, w));
    }

    @Override // com.sermatec.sunmate.databinding.BatDataFragmentBinding
    public void c(@Nullable DataViewModel dataViewModel) {
        this.u = dataViewModel;
        synchronized (this) {
            this.z |= 4096;
        }
        notifyPropertyChanged(1);
        super.requestRebind();
    }

    public final boolean d(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.z |= 8;
        }
        return true;
    }

    public final boolean e(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.z |= 256;
        }
        return true;
    }

    /* JADX WARN: Removed duplicated region for block: B:100:0x014f  */
    /* JADX WARN: Removed duplicated region for block: B:109:0x016e  */
    /* JADX WARN: Removed duplicated region for block: B:19:0x004b  */
    /* JADX WARN: Removed duplicated region for block: B:28:0x0067  */
    /* JADX WARN: Removed duplicated region for block: B:37:0x0083  */
    /* JADX WARN: Removed duplicated region for block: B:46:0x009f  */
    /* JADX WARN: Removed duplicated region for block: B:55:0x00bb  */
    /* JADX WARN: Removed duplicated region for block: B:64:0x00d7  */
    /* JADX WARN: Removed duplicated region for block: B:73:0x00f5  */
    /* JADX WARN: Removed duplicated region for block: B:82:0x0111  */
    /* JADX WARN: Removed duplicated region for block: B:91:0x0130  */
    @Override // androidx.databinding.ViewDataBinding
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void executeBindings() {
        /*
            Method dump skipped, instructions count: 546
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sermatec.sunmate.databinding.BatDataFragmentBindingImpl.executeBindings():void");
    }

    public final boolean f(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.z |= 32;
        }
        return true;
    }

    public final boolean g(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.z |= 1024;
        }
        return true;
    }

    public final boolean h(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.z |= 2048;
        }
        return true;
    }

    @Override // androidx.databinding.ViewDataBinding
    public boolean hasPendingBindings() {
        synchronized (this) {
            return this.z != 0;
        }
    }

    public final boolean i(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.z |= 4;
        }
        return true;
    }

    @Override // androidx.databinding.ViewDataBinding
    public void invalidateAll() {
        synchronized (this) {
            this.z = 8192L;
        }
        requestRebind();
    }

    public final boolean j(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.z |= 64;
        }
        return true;
    }

    public final boolean k(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.z |= 16;
        }
        return true;
    }

    public final boolean l(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.z |= 1;
        }
        return true;
    }

    public final boolean m(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.z |= 128;
        }
        return true;
    }

    public final boolean n(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.z |= 2;
        }
        return true;
    }

    public final boolean o(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.z |= 512;
        }
        return true;
    }

    @Override // androidx.databinding.ViewDataBinding
    public boolean onFieldChange(int i, Object obj, int i2) {
        switch (i) {
            case 0:
                return l((DataViewModel.CustomObservableField) obj, i2);
            case 1:
                return n((DataViewModel.CustomObservableField) obj, i2);
            case 2:
                return i((DataViewModel.CustomObservableField) obj, i2);
            case 3:
                return d((DataViewModel.CustomObservableField) obj, i2);
            case 4:
                return k((DataViewModel.CustomObservableField) obj, i2);
            case 5:
                return f((DataViewModel.CustomObservableField) obj, i2);
            case 6:
                return j((DataViewModel.CustomObservableField) obj, i2);
            case 7:
                return m((DataViewModel.CustomObservableField) obj, i2);
            case 8:
                return e((DataViewModel.CustomObservableField) obj, i2);
            case 9:
                return o((DataViewModel.CustomObservableField) obj, i2);
            case 10:
                return g((DataViewModel.CustomObservableField) obj, i2);
            case 11:
                return h((DataViewModel.CustomObservableField) obj, i2);
            default:
                return false;
        }
    }

    @Override // androidx.databinding.ViewDataBinding
    public boolean setVariable(int i, @Nullable Object obj) {
        if (1 != i) {
            return false;
        }
        c((DataViewModel) obj);
        return true;
    }

    public BatDataFragmentBindingImpl(DataBindingComponent dataBindingComponent, View view, Object[] objArr) {
        super(dataBindingComponent, view, 12, (AppCompatTextView) objArr[11], (AppCompatTextView) objArr[2], (AppCompatTextView) objArr[14], (AppCompatTextView) objArr[8], (AppCompatTextView) objArr[20], (AppCompatTextView) objArr[9], (AppCompatTextView) objArr[21], (AppCompatTextView) objArr[6], (AppCompatTextView) objArr[18], (AppCompatTextView) objArr[7], (AppCompatTextView) objArr[19], (AppCompatTextView) objArr[4], (AppCompatTextView) objArr[16], (AppCompatTextView) objArr[5], (AppCompatTextView) objArr[17], (AppCompatTextView) objArr[10], (AppCompatTextView) objArr[3], (AppCompatTextView) objArr[15], (AppCompatTextView) objArr[1], (AppCompatTextView) objArr[13]);
        this.z = -1L;
        this.f118a.setTag(null);
        this.f119b.setTag(null);
        this.f121d.setTag(null);
        this.f123f.setTag(null);
        this.h.setTag(null);
        this.j.setTag(null);
        this.l.setTag(null);
        this.n.setTag(null);
        this.p.setTag(null);
        this.q.setTag(null);
        this.s.setTag(null);
        NestedScrollView nestedScrollView = (NestedScrollView) objArr[0];
        this.x = nestedScrollView;
        nestedScrollView.setTag(null);
        AppCompatTextView appCompatTextView = (AppCompatTextView) objArr[12];
        this.y = appCompatTextView;
        appCompatTextView.setTag(null);
        setRootTag(view);
        invalidateAll();
    }
}
