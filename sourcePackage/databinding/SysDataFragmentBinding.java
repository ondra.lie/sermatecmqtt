package com.sermatec.sunmate.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.sermatec.sunmate.R;
import com.sermatec.sunmate.ui.data.DataViewModel;

/* loaded from: classes.dex */
public abstract class SysDataFragmentBinding extends ViewDataBinding {
    @NonNull

    /* renamed from: a  reason: collision with root package name */
    public final AppCompatTextView f154a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final AppCompatTextView f155b;
    @NonNull

    /* renamed from: c  reason: collision with root package name */
    public final LinearLayoutCompat f156c;
    @NonNull

    /* renamed from: d  reason: collision with root package name */
    public final AppCompatTextView f157d;
    @NonNull

    /* renamed from: e  reason: collision with root package name */
    public final AppCompatTextView f158e;
    @NonNull

    /* renamed from: f  reason: collision with root package name */
    public final AppCompatTextView f159f;
    @Bindable
    public DataViewModel g;

    public SysDataFragmentBinding(Object obj, View view, int i, AppCompatTextView appCompatTextView, AppCompatTextView appCompatTextView2, LinearLayoutCompat linearLayoutCompat, AppCompatTextView appCompatTextView3, AppCompatTextView appCompatTextView4, AppCompatTextView appCompatTextView5) {
        super(obj, view, i);
        this.f154a = appCompatTextView;
        this.f155b = appCompatTextView2;
        this.f156c = linearLayoutCompat;
        this.f157d = appCompatTextView3;
        this.f158e = appCompatTextView4;
        this.f159f = appCompatTextView5;
    }

    @NonNull
    public static SysDataFragmentBinding a(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, boolean z) {
        return b(layoutInflater, viewGroup, z, DataBindingUtil.getDefaultComponent());
    }

    @NonNull
    @Deprecated
    public static SysDataFragmentBinding b(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, boolean z, @Nullable Object obj) {
        return (SysDataFragmentBinding) ViewDataBinding.inflateInternal(layoutInflater, R.layout.sys_data_fragment, viewGroup, z, obj);
    }

    public abstract void c(@Nullable DataViewModel dataViewModel);
}
