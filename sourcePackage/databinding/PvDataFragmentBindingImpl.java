package com.sermatec.sunmate.databinding;

import android.util.SparseIntArray;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import com.sermatec.sunmate.R;
import com.sermatec.sunmate.ui.data.DataViewModel;

/* loaded from: classes.dex */
public class PvDataFragmentBindingImpl extends PvDataFragmentBinding {
    @Nullable
    public static final ViewDataBinding.IncludedLayouts n = null;
    @Nullable
    public static final SparseIntArray o;
    @NonNull
    public final NestedScrollView p;
    public long q;

    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        o = sparseIntArray;
        sparseIntArray.put(R.id.pv1VolUnit, 7);
        sparseIntArray.put(R.id.pv1CurUnit, 8);
        sparseIntArray.put(R.id.pv1PowerUnit, 9);
        sparseIntArray.put(R.id.pv2VolUnit, 10);
        sparseIntArray.put(R.id.pv2CurUnit, 11);
        sparseIntArray.put(R.id.pv2PowerUnit, 12);
    }

    public PvDataFragmentBindingImpl(@Nullable DataBindingComponent dataBindingComponent, @NonNull View view) {
        this(dataBindingComponent, view, ViewDataBinding.mapBindings(dataBindingComponent, view, 13, n, o));
    }

    @Override // com.sermatec.sunmate.databinding.PvDataFragmentBinding
    public void c(@Nullable DataViewModel dataViewModel) {
        this.m = dataViewModel;
        synchronized (this) {
            this.q |= 64;
        }
        notifyPropertyChanged(1);
        super.requestRebind();
    }

    public final boolean d(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.q |= 2;
        }
        return true;
    }

    public final boolean e(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.q |= 8;
        }
        return true;
    }

    /* JADX WARN: Removed duplicated region for block: B:19:0x0041  */
    /* JADX WARN: Removed duplicated region for block: B:28:0x005d  */
    /* JADX WARN: Removed duplicated region for block: B:37:0x0079  */
    /* JADX WARN: Removed duplicated region for block: B:46:0x0095  */
    /* JADX WARN: Removed duplicated region for block: B:55:0x00b3  */
    @Override // androidx.databinding.ViewDataBinding
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void executeBindings() {
        /*
            Method dump skipped, instructions count: 281
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sermatec.sunmate.databinding.PvDataFragmentBindingImpl.executeBindings():void");
    }

    public final boolean f(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.q |= 4;
        }
        return true;
    }

    public final boolean g(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.q |= 1;
        }
        return true;
    }

    public final boolean h(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.q |= 16;
        }
        return true;
    }

    @Override // androidx.databinding.ViewDataBinding
    public boolean hasPendingBindings() {
        synchronized (this) {
            return this.q != 0;
        }
    }

    public final boolean i(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.q |= 32;
        }
        return true;
    }

    @Override // androidx.databinding.ViewDataBinding
    public void invalidateAll() {
        synchronized (this) {
            this.q = 128L;
        }
        requestRebind();
    }

    @Override // androidx.databinding.ViewDataBinding
    public boolean onFieldChange(int i, Object obj, int i2) {
        if (i == 0) {
            return g((DataViewModel.CustomObservableField) obj, i2);
        }
        if (i == 1) {
            return d((DataViewModel.CustomObservableField) obj, i2);
        }
        if (i == 2) {
            return f((DataViewModel.CustomObservableField) obj, i2);
        }
        if (i == 3) {
            return e((DataViewModel.CustomObservableField) obj, i2);
        }
        if (i == 4) {
            return h((DataViewModel.CustomObservableField) obj, i2);
        }
        if (i != 5) {
            return false;
        }
        return i((DataViewModel.CustomObservableField) obj, i2);
    }

    @Override // androidx.databinding.ViewDataBinding
    public boolean setVariable(int i, @Nullable Object obj) {
        if (1 != i) {
            return false;
        }
        c((DataViewModel) obj);
        return true;
    }

    public PvDataFragmentBindingImpl(DataBindingComponent dataBindingComponent, View view, Object[] objArr) {
        super(dataBindingComponent, view, 6, (AppCompatTextView) objArr[2], (AppCompatTextView) objArr[8], (AppCompatTextView) objArr[3], (AppCompatTextView) objArr[9], (AppCompatTextView) objArr[1], (AppCompatTextView) objArr[7], (AppCompatTextView) objArr[5], (AppCompatTextView) objArr[11], (AppCompatTextView) objArr[6], (AppCompatTextView) objArr[12], (AppCompatTextView) objArr[4], (AppCompatTextView) objArr[10]);
        this.q = -1L;
        NestedScrollView nestedScrollView = (NestedScrollView) objArr[0];
        this.p = nestedScrollView;
        nestedScrollView.setTag(null);
        this.f148a.setTag(null);
        this.f150c.setTag(null);
        this.f152e.setTag(null);
        this.g.setTag(null);
        this.i.setTag(null);
        this.k.setTag(null);
        setRootTag(view);
        invalidateAll();
    }
}
