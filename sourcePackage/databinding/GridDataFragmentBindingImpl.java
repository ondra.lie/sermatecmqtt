package com.sermatec.sunmate.databinding;

import android.util.SparseIntArray;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import com.sermatec.sunmate.R;
import com.sermatec.sunmate.ui.data.DataViewModel;
import io.netty.handler.codec.http.multipart.DefaultHttpDataFactory;

/* loaded from: classes.dex */
public class GridDataFragmentBindingImpl extends GridDataFragmentBinding {
    @Nullable
    public static final ViewDataBinding.IncludedLayouts C = null;
    @Nullable
    public static final SparseIntArray D;
    @NonNull
    public final NestedScrollView E;
    public long F;

    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        D = sparseIntArray;
        sparseIntArray.put(R.id.gridActivePowerUnit, 15);
        sparseIntArray.put(R.id.gridReactivePowerUnit, 16);
        sparseIntArray.put(R.id.gridTotalPowerUnit, 17);
        sparseIntArray.put(R.id.gridABLineVolUnit, 18);
        sparseIntArray.put(R.id.gridBCLineVolUnit, 19);
        sparseIntArray.put(R.id.gridCALineVolUnit, 20);
        sparseIntArray.put(R.id.gridAPhraseVolUnit, 21);
        sparseIntArray.put(R.id.gridBPhraseVolUnit, 22);
        sparseIntArray.put(R.id.gridCPhraseVolUnit, 23);
        sparseIntArray.put(R.id.gridAPhraseCurUnit, 24);
        sparseIntArray.put(R.id.gridBPhraseCurUnit, 25);
        sparseIntArray.put(R.id.gridCPhraseCurUnit, 26);
        sparseIntArray.put(R.id.gridFreUnit, 27);
    }

    public GridDataFragmentBindingImpl(@Nullable DataBindingComponent dataBindingComponent, @NonNull View view) {
        this(dataBindingComponent, view, ViewDataBinding.mapBindings(dataBindingComponent, view, 28, C, D));
    }

    @Override // com.sermatec.sunmate.databinding.GridDataFragmentBinding
    public void c(@Nullable DataViewModel dataViewModel) {
        this.B = dataViewModel;
        synchronized (this) {
            this.F |= DefaultHttpDataFactory.MINSIZE;
        }
        notifyPropertyChanged(1);
        super.requestRebind();
    }

    public final boolean d(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.F |= 8;
        }
        return true;
    }

    public final boolean e(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.F |= 1;
        }
        return true;
    }

    /* JADX WARN: Removed duplicated region for block: B:100:0x0163  */
    /* JADX WARN: Removed duplicated region for block: B:109:0x0182  */
    /* JADX WARN: Removed duplicated region for block: B:115:0x019b  */
    /* JADX WARN: Removed duplicated region for block: B:119:0x01aa  */
    /* JADX WARN: Removed duplicated region for block: B:125:0x01c3  */
    /* JADX WARN: Removed duplicated region for block: B:129:0x01cf  */
    /* JADX WARN: Removed duplicated region for block: B:19:0x0059  */
    /* JADX WARN: Removed duplicated region for block: B:28:0x0075  */
    /* JADX WARN: Removed duplicated region for block: B:37:0x0091  */
    /* JADX WARN: Removed duplicated region for block: B:46:0x00ad  */
    /* JADX WARN: Removed duplicated region for block: B:55:0x00c9  */
    /* JADX WARN: Removed duplicated region for block: B:64:0x00e5  */
    /* JADX WARN: Removed duplicated region for block: B:73:0x0101  */
    /* JADX WARN: Removed duplicated region for block: B:82:0x011f  */
    /* JADX WARN: Removed duplicated region for block: B:91:0x0141  */
    @Override // androidx.databinding.ViewDataBinding
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void executeBindings() {
        /*
            Method dump skipped, instructions count: 686
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sermatec.sunmate.databinding.GridDataFragmentBindingImpl.executeBindings():void");
    }

    public final boolean f(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.F |= 64;
        }
        return true;
    }

    public final boolean g(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.F |= 4;
        }
        return true;
    }

    public final boolean h(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.F |= 32;
        }
        return true;
    }

    @Override // androidx.databinding.ViewDataBinding
    public boolean hasPendingBindings() {
        synchronized (this) {
            return this.F != 0;
        }
    }

    public final boolean i(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.F |= 8192;
        }
        return true;
    }

    @Override // androidx.databinding.ViewDataBinding
    public void invalidateAll() {
        synchronized (this) {
            this.F = 32768L;
        }
        requestRebind();
    }

    public final boolean j(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.F |= 2;
        }
        return true;
    }

    public final boolean k(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.F |= 128;
        }
        return true;
    }

    public final boolean l(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.F |= 2048;
        }
        return true;
    }

    public final boolean m(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.F |= 512;
        }
        return true;
    }

    public final boolean n(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.F |= 256;
        }
        return true;
    }

    public final boolean o(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.F |= 1024;
        }
        return true;
    }

    @Override // androidx.databinding.ViewDataBinding
    public boolean onFieldChange(int i, Object obj, int i2) {
        switch (i) {
            case 0:
                return e((DataViewModel.CustomObservableField) obj, i2);
            case 1:
                return j((DataViewModel.CustomObservableField) obj, i2);
            case 2:
                return g((DataViewModel.CustomObservableField) obj, i2);
            case 3:
                return d((DataViewModel.CustomObservableField) obj, i2);
            case 4:
                return q((DataViewModel.CustomObservableField) obj, i2);
            case 5:
                return h((DataViewModel.CustomObservableField) obj, i2);
            case 6:
                return f((DataViewModel.CustomObservableField) obj, i2);
            case 7:
                return k((DataViewModel.CustomObservableField) obj, i2);
            case 8:
                return n((DataViewModel.CustomObservableField) obj, i2);
            case 9:
                return m((DataViewModel.CustomObservableField) obj, i2);
            case 10:
                return o((DataViewModel.CustomObservableField) obj, i2);
            case 11:
                return l((DataViewModel.CustomObservableField) obj, i2);
            case 12:
                return p((DataViewModel.CustomObservableField) obj, i2);
            case 13:
                return i((DataViewModel.CustomObservableField) obj, i2);
            default:
                return false;
        }
    }

    public final boolean p(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.F |= 4096;
        }
        return true;
    }

    public final boolean q(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.F |= 16;
        }
        return true;
    }

    @Override // androidx.databinding.ViewDataBinding
    public boolean setVariable(int i, @Nullable Object obj) {
        if (1 != i) {
            return false;
        }
        c((DataViewModel) obj);
        return true;
    }

    public GridDataFragmentBindingImpl(DataBindingComponent dataBindingComponent, View view, Object[] objArr) {
        super(dataBindingComponent, view, 14, (AppCompatTextView) objArr[4], (AppCompatTextView) objArr[18], (AppCompatTextView) objArr[10], (AppCompatTextView) objArr[24], (AppCompatTextView) objArr[7], (AppCompatTextView) objArr[21], (AppCompatTextView) objArr[1], (AppCompatTextView) objArr[15], (AppCompatTextView) objArr[5], (AppCompatTextView) objArr[19], (AppCompatTextView) objArr[11], (AppCompatTextView) objArr[25], (AppCompatTextView) objArr[8], (AppCompatTextView) objArr[22], (AppCompatTextView) objArr[6], (AppCompatTextView) objArr[20], (AppCompatTextView) objArr[12], (AppCompatTextView) objArr[26], (AppCompatTextView) objArr[9], (AppCompatTextView) objArr[23], (AppCompatTextView) objArr[13], (AppCompatTextView) objArr[27], (AppCompatTextView) objArr[14], (AppCompatTextView) objArr[2], (AppCompatTextView) objArr[16], (AppCompatTextView) objArr[3], (AppCompatTextView) objArr[17]);
        this.F = -1L;
        this.f130a.setTag(null);
        this.f132c.setTag(null);
        this.f134e.setTag(null);
        this.g.setTag(null);
        this.i.setTag(null);
        this.k.setTag(null);
        this.m.setTag(null);
        this.o.setTag(null);
        this.q.setTag(null);
        this.s.setTag(null);
        this.u.setTag(null);
        this.w.setTag(null);
        this.x.setTag(null);
        this.z.setTag(null);
        NestedScrollView nestedScrollView = (NestedScrollView) objArr[0];
        this.E = nestedScrollView;
        nestedScrollView.setTag(null);
        setRootTag(view);
        invalidateAll();
    }
}
