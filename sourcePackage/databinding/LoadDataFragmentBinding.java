package com.sermatec.sunmate.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.sermatec.sunmate.R;
import com.sermatec.sunmate.ui.data.DataViewModel;

/* loaded from: classes.dex */
public abstract class LoadDataFragmentBinding extends ViewDataBinding {
    @NonNull

    /* renamed from: a  reason: collision with root package name */
    public final AppCompatTextView f136a;
    @NonNull

    /* renamed from: b  reason: collision with root package name */
    public final AppCompatTextView f137b;
    @NonNull

    /* renamed from: c  reason: collision with root package name */
    public final AppCompatTextView f138c;
    @NonNull

    /* renamed from: d  reason: collision with root package name */
    public final AppCompatTextView f139d;
    @NonNull

    /* renamed from: e  reason: collision with root package name */
    public final AppCompatTextView f140e;
    @NonNull

    /* renamed from: f  reason: collision with root package name */
    public final AppCompatTextView f141f;
    @NonNull
    public final AppCompatTextView g;
    @NonNull
    public final AppCompatTextView h;
    @NonNull
    public final AppCompatTextView i;
    @NonNull
    public final AppCompatTextView j;
    @NonNull
    public final AppCompatTextView k;
    @NonNull
    public final AppCompatTextView l;
    @NonNull
    public final AppCompatTextView m;
    @NonNull
    public final AppCompatTextView n;
    @NonNull
    public final AppCompatTextView o;
    @NonNull
    public final AppCompatTextView p;
    @NonNull
    public final AppCompatTextView q;
    @NonNull
    public final AppCompatTextView r;
    @NonNull
    public final AppCompatTextView s;
    @NonNull
    public final AppCompatTextView t;
    @NonNull
    public final AppCompatTextView u;
    @Bindable
    public DataViewModel v;

    public LoadDataFragmentBinding(Object obj, View view, int i, AppCompatTextView appCompatTextView, AppCompatTextView appCompatTextView2, AppCompatTextView appCompatTextView3, AppCompatTextView appCompatTextView4, AppCompatTextView appCompatTextView5, AppCompatTextView appCompatTextView6, AppCompatTextView appCompatTextView7, AppCompatTextView appCompatTextView8, AppCompatTextView appCompatTextView9, AppCompatTextView appCompatTextView10, AppCompatTextView appCompatTextView11, AppCompatTextView appCompatTextView12, AppCompatTextView appCompatTextView13, AppCompatTextView appCompatTextView14, AppCompatTextView appCompatTextView15, AppCompatTextView appCompatTextView16, AppCompatTextView appCompatTextView17, AppCompatTextView appCompatTextView18, AppCompatTextView appCompatTextView19, AppCompatTextView appCompatTextView20, AppCompatTextView appCompatTextView21) {
        super(obj, view, i);
        this.f136a = appCompatTextView;
        this.f137b = appCompatTextView2;
        this.f138c = appCompatTextView3;
        this.f139d = appCompatTextView4;
        this.f140e = appCompatTextView5;
        this.f141f = appCompatTextView6;
        this.g = appCompatTextView7;
        this.h = appCompatTextView8;
        this.i = appCompatTextView9;
        this.j = appCompatTextView10;
        this.k = appCompatTextView11;
        this.l = appCompatTextView12;
        this.m = appCompatTextView13;
        this.n = appCompatTextView14;
        this.o = appCompatTextView15;
        this.p = appCompatTextView16;
        this.q = appCompatTextView17;
        this.r = appCompatTextView18;
        this.s = appCompatTextView19;
        this.t = appCompatTextView20;
        this.u = appCompatTextView21;
    }

    @NonNull
    public static LoadDataFragmentBinding a(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, boolean z) {
        return b(layoutInflater, viewGroup, z, DataBindingUtil.getDefaultComponent());
    }

    @NonNull
    @Deprecated
    public static LoadDataFragmentBinding b(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, boolean z, @Nullable Object obj) {
        return (LoadDataFragmentBinding) ViewDataBinding.inflateInternal(layoutInflater, R.layout.load_data_fragment, viewGroup, z, obj);
    }

    public abstract void c(@Nullable DataViewModel dataViewModel);
}
