package com.sermatec.sunmate.databinding;

import android.util.SparseIntArray;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import com.sermatec.sunmate.R;
import com.sermatec.sunmate.ui.data.DataViewModel;
import io.netty.handler.codec.http.multipart.DefaultHttpDataFactory;

/* loaded from: classes.dex */
public class ParallelDataFragmentBindingImpl extends ParallelDataFragmentBinding {
    @Nullable

    /* renamed from: c  reason: collision with root package name */
    public static final ViewDataBinding.IncludedLayouts f144c = null;
    @Nullable

    /* renamed from: d  reason: collision with root package name */
    public static final SparseIntArray f145d;
    @NonNull

    /* renamed from: e  reason: collision with root package name */
    public final NestedScrollView f146e;
    @NonNull

    /* renamed from: f  reason: collision with root package name */
    public final AppCompatTextView f147f;
    @NonNull
    public final AppCompatTextView g;
    @NonNull
    public final AppCompatTextView h;
    @NonNull
    public final AppCompatTextView i;
    @NonNull
    public final AppCompatTextView j;
    @NonNull
    public final AppCompatTextView k;
    @NonNull
    public final AppCompatTextView l;
    @NonNull
    public final AppCompatTextView m;
    @NonNull
    public final AppCompatTextView n;
    @NonNull
    public final AppCompatTextView o;
    @NonNull
    public final AppCompatTextView p;
    @NonNull
    public final AppCompatTextView q;
    @NonNull
    public final AppCompatTextView r;
    @NonNull
    public final AppCompatTextView s;
    public long t;

    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        f145d = sparseIntArray;
        sparseIntArray.put(R.id.pv1VolUnit, 15);
    }

    public ParallelDataFragmentBindingImpl(@Nullable DataBindingComponent dataBindingComponent, @NonNull View view) {
        this(dataBindingComponent, view, ViewDataBinding.mapBindings(dataBindingComponent, view, 16, f144c, f145d));
    }

    @Override // com.sermatec.sunmate.databinding.ParallelDataFragmentBinding
    public void c(@Nullable DataViewModel dataViewModel) {
        this.f143b = dataViewModel;
        synchronized (this) {
            this.t |= DefaultHttpDataFactory.MINSIZE;
        }
        notifyPropertyChanged(1);
        super.requestRebind();
    }

    public final boolean d(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.t |= 8192;
        }
        return true;
    }

    public final boolean e(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.t |= 512;
        }
        return true;
    }

    /* JADX WARN: Removed duplicated region for block: B:100:0x015f  */
    /* JADX WARN: Removed duplicated region for block: B:109:0x017e  */
    /* JADX WARN: Removed duplicated region for block: B:115:0x0197  */
    /* JADX WARN: Removed duplicated region for block: B:119:0x01a6  */
    /* JADX WARN: Removed duplicated region for block: B:125:0x01bf  */
    /* JADX WARN: Removed duplicated region for block: B:129:0x01cb  */
    /* JADX WARN: Removed duplicated region for block: B:19:0x0059  */
    /* JADX WARN: Removed duplicated region for block: B:28:0x0075  */
    /* JADX WARN: Removed duplicated region for block: B:37:0x0091  */
    /* JADX WARN: Removed duplicated region for block: B:46:0x00ad  */
    /* JADX WARN: Removed duplicated region for block: B:55:0x00c9  */
    /* JADX WARN: Removed duplicated region for block: B:64:0x00e5  */
    /* JADX WARN: Removed duplicated region for block: B:73:0x0104  */
    /* JADX WARN: Removed duplicated region for block: B:82:0x0123  */
    /* JADX WARN: Removed duplicated region for block: B:91:0x0140  */
    @Override // androidx.databinding.ViewDataBinding
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void executeBindings() {
        /*
            Method dump skipped, instructions count: 683
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sermatec.sunmate.databinding.ParallelDataFragmentBindingImpl.executeBindings():void");
    }

    public final boolean f(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.t |= 2048;
        }
        return true;
    }

    public final boolean g(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.t |= 1;
        }
        return true;
    }

    public final boolean h(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.t |= 1024;
        }
        return true;
    }

    @Override // androidx.databinding.ViewDataBinding
    public boolean hasPendingBindings() {
        synchronized (this) {
            return this.t != 0;
        }
    }

    public final boolean i(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.t |= 64;
        }
        return true;
    }

    @Override // androidx.databinding.ViewDataBinding
    public void invalidateAll() {
        synchronized (this) {
            this.t = 32768L;
        }
        requestRebind();
    }

    public final boolean j(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.t |= 128;
        }
        return true;
    }

    public final boolean k(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.t |= 2;
        }
        return true;
    }

    public final boolean l(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.t |= 32;
        }
        return true;
    }

    public final boolean m(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.t |= 4096;
        }
        return true;
    }

    public final boolean n(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.t |= 4;
        }
        return true;
    }

    public final boolean o(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.t |= 8;
        }
        return true;
    }

    @Override // androidx.databinding.ViewDataBinding
    public boolean onFieldChange(int i, Object obj, int i2) {
        switch (i) {
            case 0:
                return g((DataViewModel.CustomObservableField) obj, i2);
            case 1:
                return k((DataViewModel.CustomObservableField) obj, i2);
            case 2:
                return n((DataViewModel.CustomObservableField) obj, i2);
            case 3:
                return o((DataViewModel.CustomObservableField) obj, i2);
            case 4:
                return q((DataViewModel.CustomObservableField) obj, i2);
            case 5:
                return l((DataViewModel.CustomObservableField) obj, i2);
            case 6:
                return i((DataViewModel.CustomObservableField) obj, i2);
            case 7:
                return j((DataViewModel.CustomObservableField) obj, i2);
            case 8:
                return p((DataViewModel.CustomObservableField) obj, i2);
            case 9:
                return e((DataViewModel.CustomObservableField) obj, i2);
            case 10:
                return h((DataViewModel.CustomObservableField) obj, i2);
            case 11:
                return f((DataViewModel.CustomObservableField) obj, i2);
            case 12:
                return m((DataViewModel.CustomObservableField) obj, i2);
            case 13:
                return d((DataViewModel.CustomObservableField) obj, i2);
            default:
                return false;
        }
    }

    public final boolean p(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.t |= 256;
        }
        return true;
    }

    public final boolean q(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.t |= 16;
        }
        return true;
    }

    @Override // androidx.databinding.ViewDataBinding
    public boolean setVariable(int i, @Nullable Object obj) {
        if (1 != i) {
            return false;
        }
        c((DataViewModel) obj);
        return true;
    }

    public ParallelDataFragmentBindingImpl(DataBindingComponent dataBindingComponent, View view, Object[] objArr) {
        super(dataBindingComponent, view, 14, (AppCompatTextView) objArr[15]);
        this.t = -1L;
        NestedScrollView nestedScrollView = (NestedScrollView) objArr[0];
        this.f146e = nestedScrollView;
        nestedScrollView.setTag(null);
        AppCompatTextView appCompatTextView = (AppCompatTextView) objArr[1];
        this.f147f = appCompatTextView;
        appCompatTextView.setTag(null);
        AppCompatTextView appCompatTextView2 = (AppCompatTextView) objArr[10];
        this.g = appCompatTextView2;
        appCompatTextView2.setTag(null);
        AppCompatTextView appCompatTextView3 = (AppCompatTextView) objArr[11];
        this.h = appCompatTextView3;
        appCompatTextView3.setTag(null);
        AppCompatTextView appCompatTextView4 = (AppCompatTextView) objArr[12];
        this.i = appCompatTextView4;
        appCompatTextView4.setTag(null);
        AppCompatTextView appCompatTextView5 = (AppCompatTextView) objArr[13];
        this.j = appCompatTextView5;
        appCompatTextView5.setTag(null);
        AppCompatTextView appCompatTextView6 = (AppCompatTextView) objArr[14];
        this.k = appCompatTextView6;
        appCompatTextView6.setTag(null);
        AppCompatTextView appCompatTextView7 = (AppCompatTextView) objArr[2];
        this.l = appCompatTextView7;
        appCompatTextView7.setTag(null);
        AppCompatTextView appCompatTextView8 = (AppCompatTextView) objArr[3];
        this.m = appCompatTextView8;
        appCompatTextView8.setTag(null);
        AppCompatTextView appCompatTextView9 = (AppCompatTextView) objArr[4];
        this.n = appCompatTextView9;
        appCompatTextView9.setTag(null);
        AppCompatTextView appCompatTextView10 = (AppCompatTextView) objArr[5];
        this.o = appCompatTextView10;
        appCompatTextView10.setTag(null);
        AppCompatTextView appCompatTextView11 = (AppCompatTextView) objArr[6];
        this.p = appCompatTextView11;
        appCompatTextView11.setTag(null);
        AppCompatTextView appCompatTextView12 = (AppCompatTextView) objArr[7];
        this.q = appCompatTextView12;
        appCompatTextView12.setTag(null);
        AppCompatTextView appCompatTextView13 = (AppCompatTextView) objArr[8];
        this.r = appCompatTextView13;
        appCompatTextView13.setTag(null);
        AppCompatTextView appCompatTextView14 = (AppCompatTextView) objArr[9];
        this.s = appCompatTextView14;
        appCompatTextView14.setTag(null);
        setRootTag(view);
        invalidateAll();
    }
}
