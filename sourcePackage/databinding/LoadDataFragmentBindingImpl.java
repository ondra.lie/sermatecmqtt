package com.sermatec.sunmate.databinding;

import android.util.SparseIntArray;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import com.sermatec.sunmate.R;
import com.sermatec.sunmate.ui.data.DataViewModel;

/* loaded from: classes.dex */
public class LoadDataFragmentBindingImpl extends LoadDataFragmentBinding {
    @Nullable
    public static final ViewDataBinding.IncludedLayouts w = null;
    @Nullable
    public static final SparseIntArray x;
    @NonNull
    public final NestedScrollView y;
    public long z;

    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        x = sparseIntArray;
        sparseIntArray.put(R.id.loadActivePowerUnit, 12);
        sparseIntArray.put(R.id.loadReactivePowerUnit, 13);
        sparseIntArray.put(R.id.loadTotalPowerUnit, 14);
        sparseIntArray.put(R.id.loadABLineVolUnit, 15);
        sparseIntArray.put(R.id.loadBCLineVolUnit, 16);
        sparseIntArray.put(R.id.loadCALineVolUnit, 17);
        sparseIntArray.put(R.id.loadAPhraseCurUnit, 18);
        sparseIntArray.put(R.id.loadBPhraseCurUnit, 19);
        sparseIntArray.put(R.id.loadCPhraseCurUnit, 20);
        sparseIntArray.put(R.id.loadFreUnit, 21);
    }

    public LoadDataFragmentBindingImpl(@Nullable DataBindingComponent dataBindingComponent, @NonNull View view) {
        this(dataBindingComponent, view, ViewDataBinding.mapBindings(dataBindingComponent, view, 22, w, x));
    }

    @Override // com.sermatec.sunmate.databinding.LoadDataFragmentBinding
    public void c(@Nullable DataViewModel dataViewModel) {
        this.v = dataViewModel;
        synchronized (this) {
            this.z |= 2048;
        }
        notifyPropertyChanged(1);
        super.requestRebind();
    }

    public final boolean d(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.z |= 1;
        }
        return true;
    }

    public final boolean e(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.z |= 256;
        }
        return true;
    }

    /* JADX WARN: Removed duplicated region for block: B:100:0x014d  */
    /* JADX WARN: Removed duplicated region for block: B:19:0x0049  */
    /* JADX WARN: Removed duplicated region for block: B:28:0x0065  */
    /* JADX WARN: Removed duplicated region for block: B:37:0x0081  */
    /* JADX WARN: Removed duplicated region for block: B:46:0x009d  */
    /* JADX WARN: Removed duplicated region for block: B:55:0x00b9  */
    /* JADX WARN: Removed duplicated region for block: B:64:0x00d7  */
    /* JADX WARN: Removed duplicated region for block: B:73:0x00f3  */
    /* JADX WARN: Removed duplicated region for block: B:82:0x010f  */
    /* JADX WARN: Removed duplicated region for block: B:91:0x012e  */
    @Override // androidx.databinding.ViewDataBinding
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void executeBindings() {
        /*
            Method dump skipped, instructions count: 501
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sermatec.sunmate.databinding.LoadDataFragmentBindingImpl.executeBindings():void");
    }

    public final boolean f(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.z |= 1024;
        }
        return true;
    }

    public final boolean g(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.z |= 128;
        }
        return true;
    }

    public final boolean h(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.z |= 4;
        }
        return true;
    }

    @Override // androidx.databinding.ViewDataBinding
    public boolean hasPendingBindings() {
        synchronized (this) {
            return this.z != 0;
        }
    }

    public final boolean i(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.z |= 32;
        }
        return true;
    }

    @Override // androidx.databinding.ViewDataBinding
    public void invalidateAll() {
        synchronized (this) {
            this.z = 4096L;
        }
        requestRebind();
    }

    public final boolean j(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.z |= 16;
        }
        return true;
    }

    public final boolean k(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.z |= 64;
        }
        return true;
    }

    public final boolean l(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.z |= 8;
        }
        return true;
    }

    public final boolean m(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.z |= 2;
        }
        return true;
    }

    public final boolean n(DataViewModel.CustomObservableField customObservableField, int i) {
        if (i != 0) {
            return false;
        }
        synchronized (this) {
            this.z |= 512;
        }
        return true;
    }

    @Override // androidx.databinding.ViewDataBinding
    public boolean onFieldChange(int i, Object obj, int i2) {
        switch (i) {
            case 0:
                return d((DataViewModel.CustomObservableField) obj, i2);
            case 1:
                return m((DataViewModel.CustomObservableField) obj, i2);
            case 2:
                return h((DataViewModel.CustomObservableField) obj, i2);
            case 3:
                return l((DataViewModel.CustomObservableField) obj, i2);
            case 4:
                return j((DataViewModel.CustomObservableField) obj, i2);
            case 5:
                return i((DataViewModel.CustomObservableField) obj, i2);
            case 6:
                return k((DataViewModel.CustomObservableField) obj, i2);
            case 7:
                return g((DataViewModel.CustomObservableField) obj, i2);
            case 8:
                return e((DataViewModel.CustomObservableField) obj, i2);
            case 9:
                return n((DataViewModel.CustomObservableField) obj, i2);
            case 10:
                return f((DataViewModel.CustomObservableField) obj, i2);
            default:
                return false;
        }
    }

    @Override // androidx.databinding.ViewDataBinding
    public boolean setVariable(int i, @Nullable Object obj) {
        if (1 != i) {
            return false;
        }
        c((DataViewModel) obj);
        return true;
    }

    public LoadDataFragmentBindingImpl(DataBindingComponent dataBindingComponent, View view, Object[] objArr) {
        super(dataBindingComponent, view, 11, (AppCompatTextView) objArr[15], (AppCompatTextView) objArr[7], (AppCompatTextView) objArr[18], (AppCompatTextView) objArr[4], (AppCompatTextView) objArr[1], (AppCompatTextView) objArr[12], (AppCompatTextView) objArr[16], (AppCompatTextView) objArr[8], (AppCompatTextView) objArr[19], (AppCompatTextView) objArr[5], (AppCompatTextView) objArr[17], (AppCompatTextView) objArr[9], (AppCompatTextView) objArr[20], (AppCompatTextView) objArr[6], (AppCompatTextView) objArr[10], (AppCompatTextView) objArr[21], (AppCompatTextView) objArr[11], (AppCompatTextView) objArr[2], (AppCompatTextView) objArr[13], (AppCompatTextView) objArr[3], (AppCompatTextView) objArr[14]);
        this.z = -1L;
        this.f137b.setTag(null);
        this.f139d.setTag(null);
        this.f140e.setTag(null);
        this.h.setTag(null);
        this.j.setTag(null);
        this.l.setTag(null);
        this.n.setTag(null);
        this.o.setTag(null);
        this.q.setTag(null);
        this.r.setTag(null);
        this.t.setTag(null);
        NestedScrollView nestedScrollView = (NestedScrollView) objArr[0];
        this.y = nestedScrollView;
        nestedScrollView.setTag(null);
        setRootTag(view);
        invalidateAll();
    }
}
