package com.sermatec.sunmate;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.multidex.MultiDexApplication;
import d.h.a.d.a;
import d.h.a.d.j.o0;
import d.h.a.e.e.d;

/* loaded from: classes.dex */
public class App extends MultiDexApplication {

    /* renamed from: a  reason: collision with root package name */
    public static App f102a;

    public static App a() {
        return f102a;
    }

    @Override // android.app.Application
    public void onCreate() {
        super.onCreate();
        int b2 = d.b(this);
        if (AppCompatDelegate.getDefaultNightMode() != b2) {
            AppCompatDelegate.setDefaultNightMode(b2);
        }
        registerActivityLifecycleCallbacks(new a());
        o0.e(this);
        f102a = this;
    }
}
