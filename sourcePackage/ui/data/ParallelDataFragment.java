package com.sermatec.sunmate.ui.data;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import com.sermatec.sunmate.databinding.ParallelDataFragmentBinding;

/* loaded from: classes.dex */
public class ParallelDataFragment extends Fragment {
    @Override // androidx.fragment.app.Fragment
    @Nullable
    public View onCreateView(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, @Nullable Bundle bundle) {
        ParallelDataFragmentBinding a2 = ParallelDataFragmentBinding.a(layoutInflater, viewGroup, false);
        a2.c((DataViewModel) new ViewModelProvider(requireActivity()).get(DataViewModel.class));
        return a2.getRoot();
    }
}
