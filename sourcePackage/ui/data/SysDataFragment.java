package com.sermatec.sunmate.ui.data;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.databinding.Observable;
import androidx.databinding.ObservableInt;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import com.sermatec.sunmate.R;
import com.sermatec.sunmate.databinding.SysDataFragmentBinding;
import com.sermatec.sunmate.ui.data.SysDataFragment;

/* loaded from: classes.dex */
public class SysDataFragment extends Fragment {

    /* renamed from: a  reason: collision with root package name */
    public Observable.OnPropertyChangedCallback f310a;

    /* renamed from: b  reason: collision with root package name */
    public DataViewModel f311b;

    /* loaded from: classes.dex */
    public class a extends Observable.OnPropertyChangedCallback {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ View f312a;

        public a(View view) {
            this.f312a = view;
        }

        /* JADX INFO: Access modifiers changed from: private */
        /* renamed from: a */
        public /* synthetic */ void b(View view) {
            if (SysDataFragment.this.f311b.G0.get() == 0) {
                view.setVisibility(8);
            } else {
                view.setVisibility(0);
            }
        }

        @Override // androidx.databinding.Observable.OnPropertyChangedCallback
        public void onPropertyChanged(Observable observable, int i) {
            final View view = this.f312a;
            Runnable aVar = new Runnable() { // from class: d.h.a.e.b.a
                @Override // java.lang.Runnable
                public final void run() {
                    SysDataFragment.a.this.b(view);
                }
            };
            if (Looper.myLooper() == Looper.getMainLooper()) {
                aVar.run();
            } else {
                new Handler(Looper.getMainLooper()).post(aVar);
            }
        }
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        SysDataFragmentBinding a2 = SysDataFragmentBinding.a(layoutInflater, viewGroup, false);
        DataViewModel dataViewModel = (DataViewModel) new ViewModelProvider(requireActivity()).get(DataViewModel.class);
        this.f311b = dataViewModel;
        a2.c(dataViewModel);
        View root = a2.getRoot();
        View findViewById = root.findViewById(R.id.device_parallel_state);
        ObservableInt observableInt = this.f311b.G0;
        a aVar = new a(findViewById);
        this.f310a = aVar;
        observableInt.addOnPropertyChangedCallback(aVar);
        this.f311b.G0.notifyChange();
        return root;
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroyView() {
        this.f311b.G0.removeOnPropertyChangedCallback(this.f310a);
        super.onDestroyView();
    }
}
