package com.sermatec.sunmate.ui.data;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import com.sermatec.sunmate.R;

/* loaded from: classes.dex */
public class DataFragment extends Fragment implements View.OnClickListener {
    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bat /* 2131296358 */:
                Navigation.findNavController(view).navigate(R.id.data_to_bat_data);
                return;
            case R.id.grid /* 2131296547 */:
                Navigation.findNavController(view).navigate(R.id.data_to_grid_data);
                return;
            case R.id.load /* 2131296633 */:
                Navigation.findNavController(view).navigate(R.id.data_to_load_data);
                return;
            case R.id.parallel /* 2131296747 */:
                Navigation.findNavController(view).navigate(R.id.data_to_parallel_data);
                return;
            case R.id.pv /* 2131296770 */:
                Navigation.findNavController(view).navigate(R.id.data_to_pv_data);
                return;
            case R.id.sys /* 2131296863 */:
                Navigation.findNavController(view).navigate(R.id.data_to_sys_data);
                return;
            default:
                return;
        }
    }

    @Override // androidx.fragment.app.Fragment
    @Nullable
    public View onCreateView(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, @Nullable Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.data_fragment, viewGroup, false);
        inflate.findViewById(R.id.pv).setOnClickListener(this);
        inflate.findViewById(R.id.grid).setOnClickListener(this);
        inflate.findViewById(R.id.load).setOnClickListener(this);
        inflate.findViewById(R.id.bat).setOnClickListener(this);
        inflate.findViewById(R.id.sys).setOnClickListener(this);
        inflate.findViewById(R.id.parallel).setOnClickListener(this);
        return inflate;
    }
}
