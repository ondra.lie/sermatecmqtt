package com.sermatec.sunmate.ui.data;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import com.sermatec.sunmate.databinding.GridDataFragmentBinding;

/* loaded from: classes.dex */
public class GridDataFragment extends Fragment {
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        GridDataFragmentBinding a2 = GridDataFragmentBinding.a(layoutInflater, viewGroup, false);
        a2.c((DataViewModel) ViewModelProviders.of(requireActivity()).get(DataViewModel.class));
        return a2.getRoot();
    }
}
