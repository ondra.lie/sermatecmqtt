package com.sermatec.sunmate.ui.data;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableDouble;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;
import androidx.lifecycle.ViewModel;
import com.google.android.material.shadow.ShadowDrawableWrapper;

/* loaded from: classes.dex */
public class DataViewModel extends ViewModel {
    public final CustomObservableField D0;
    public final ObservableBoolean H0;
    public final ObservableField<Object> Z0;
    public final ObservableDouble f0;
    public final ObservableDouble g0;
    public final CustomObservableField h0;
    public final ObservableDouble i0;
    public final ObservableDouble j0;
    public final ObservableDouble k0;
    public final ObservableBoolean l0;
    public final ObservableBoolean m0;
    public final ObservableBoolean n0;
    public final ObservableBoolean o0;
    public final ObservableBoolean p0;
    public final ObservableBoolean q0;

    /* renamed from: a  reason: collision with root package name */
    public final CustomObservableField f304a = new CustomObservableField("0");

    /* renamed from: b  reason: collision with root package name */
    public final CustomObservableField f305b = new CustomObservableField("0");

    /* renamed from: c  reason: collision with root package name */
    public final CustomObservableField f306c = new CustomObservableField("0");

    /* renamed from: d  reason: collision with root package name */
    public final CustomObservableField f307d = new CustomObservableField("0");

    /* renamed from: e  reason: collision with root package name */
    public final CustomObservableField f308e = new CustomObservableField("0");

    /* renamed from: f  reason: collision with root package name */
    public final CustomObservableField f309f = new CustomObservableField("0");
    public final CustomObservableField g = new CustomObservableField("0");
    public final CustomObservableField h = new CustomObservableField("0");
    public final CustomObservableField i = new CustomObservableField("0");
    public final CustomObservableField j = new CustomObservableField("0");
    public final CustomObservableField k = new CustomObservableField("0");
    public final CustomObservableField l = new CustomObservableField("0");
    public final CustomObservableField m = new CustomObservableField("0");
    public final CustomObservableField n = new CustomObservableField("0");
    public final CustomObservableField o = new CustomObservableField("0");
    public final CustomObservableField p = new CustomObservableField("0");
    public final CustomObservableField q = new CustomObservableField("0");
    public final CustomObservableField r = new CustomObservableField("0");
    public final CustomObservableField s = new CustomObservableField("0");
    public final CustomObservableField t = new CustomObservableField("0");
    public final CustomObservableField u = new CustomObservableField("0");
    public final CustomObservableField v = new CustomObservableField("0");
    public final CustomObservableField w = new CustomObservableField("0");
    public final CustomObservableField x = new CustomObservableField("0");
    public final CustomObservableField y = new CustomObservableField("0");
    public final CustomObservableField z = new CustomObservableField("0");
    public final CustomObservableField A = new CustomObservableField("0");
    public final CustomObservableField B = new CustomObservableField("0");
    public final CustomObservableField C = new CustomObservableField("0");
    public final CustomObservableField D = new CustomObservableField("0");
    public final CustomObservableField E = new CustomObservableField("0");
    public final CustomObservableField F = new CustomObservableField("0");
    public final CustomObservableField G = new CustomObservableField("0");
    public final CustomObservableField H = new CustomObservableField("0");
    public final CustomObservableField I = new CustomObservableField("0");
    public final CustomObservableField J = new CustomObservableField("0");
    public final CustomObservableField K = new CustomObservableField("0");
    public final CustomObservableField L = new CustomObservableField("0");
    public final CustomObservableField M = new CustomObservableField("0");
    public final CustomObservableField N = new CustomObservableField("0");
    public final CustomObservableField O = new CustomObservableField("0");
    public final CustomObservableField P = new CustomObservableField("");
    public final CustomObservableField Q = new CustomObservableField("");
    public final CustomObservableField R = new CustomObservableField("");
    public final CustomObservableField S = new CustomObservableField("");
    public final CustomObservableField T = new CustomObservableField("");
    public final CustomObservableField U = new CustomObservableField("");
    public final CustomObservableField V = new CustomObservableField("0");
    public final CustomObservableField W = new CustomObservableField("");
    public final CustomObservableField X = new CustomObservableField("0");
    public final CustomObservableField Y = new CustomObservableField("0");
    public final CustomObservableField Z = new CustomObservableField("0");
    public final CustomObservableField a0 = new CustomObservableField("0");
    public final CustomObservableField b0 = new CustomObservableField("0");
    public final CustomObservableField c0 = new CustomObservableField("0");
    public final CustomObservableField d0 = new CustomObservableField("0");
    public final CustomObservableField e0 = new CustomObservableField("0");
    public final ObservableField<float[]> r0 = new ObservableField<>();
    public final ObservableField<float[]> s0 = new ObservableField<>();
    public final ObservableField<float[]> t0 = new ObservableField<>();
    public final ObservableField<float[]> u0 = new ObservableField<>();
    public final ObservableField<float[]> v0 = new ObservableField<>();
    public final ObservableField<float[]> w0 = new ObservableField<>();
    public final ObservableField<float[]> x0 = new ObservableField<>();
    public final ObservableField<float[]> y0 = new ObservableField<>();
    public final ObservableField<float[]> z0 = new ObservableField<>();
    public final ObservableField<float[]> A0 = new ObservableField<>();
    public final ObservableField<float[]> B0 = new ObservableField<>();
    public final ObservableField<float[]> C0 = new ObservableField<>();
    public final ObservableInt E0 = new ObservableInt(-1);
    public final CustomObservableField F0 = new CustomObservableField("");
    public final ObservableInt G0 = new ObservableInt();
    public final CustomObservableField I0 = new CustomObservableField("");
    public final CustomObservableField J0 = new CustomObservableField("");
    public final CustomObservableField K0 = new CustomObservableField("");
    public final CustomObservableField L0 = new CustomObservableField("0");
    public final CustomObservableField M0 = new CustomObservableField("0");
    public final CustomObservableField N0 = new CustomObservableField("0");
    public final CustomObservableField O0 = new CustomObservableField("0");
    public final CustomObservableField P0 = new CustomObservableField("0");
    public final CustomObservableField Q0 = new CustomObservableField("0");
    public final CustomObservableField R0 = new CustomObservableField("0");
    public final CustomObservableField S0 = new CustomObservableField("0");
    public final CustomObservableField T0 = new CustomObservableField("0");
    public final CustomObservableField U0 = new CustomObservableField("0");
    public final CustomObservableField V0 = new CustomObservableField("0");
    public final CustomObservableField W0 = new CustomObservableField("0");
    public final CustomObservableField X0 = new CustomObservableField("0");
    public final CustomObservableField Y0 = new CustomObservableField("0");

    /* loaded from: classes.dex */
    public static class CustomObservableField extends ObservableField<String> {
        public CustomObservableField(String str) {
            super(str);
        }

        private boolean isNotEqual(String str, String str2) {
            return str != null && !str.equals(str2);
        }

        public void set(String str) {
            if (isNotEqual(str, get())) {
                super.set((CustomObservableField) str);
            }
        }
    }

    public DataViewModel() {
        ObservableDouble observableDouble = new ObservableDouble((double) ShadowDrawableWrapper.COS_45);
        this.f0 = observableDouble;
        ObservableDouble observableDouble2 = new ObservableDouble((double) ShadowDrawableWrapper.COS_45);
        this.g0 = observableDouble2;
        CustomObservableField customObservableField = new CustomObservableField("0");
        this.h0 = customObservableField;
        ObservableDouble observableDouble3 = new ObservableDouble((double) ShadowDrawableWrapper.COS_45);
        this.i0 = observableDouble3;
        ObservableDouble observableDouble4 = new ObservableDouble((double) ShadowDrawableWrapper.COS_45);
        this.j0 = observableDouble4;
        ObservableDouble observableDouble5 = new ObservableDouble((double) ShadowDrawableWrapper.COS_45);
        this.k0 = observableDouble5;
        ObservableBoolean observableBoolean = new ObservableBoolean(false);
        this.l0 = observableBoolean;
        ObservableBoolean observableBoolean2 = new ObservableBoolean(false);
        this.m0 = observableBoolean2;
        ObservableBoolean observableBoolean3 = new ObservableBoolean(false);
        this.n0 = observableBoolean3;
        ObservableBoolean observableBoolean4 = new ObservableBoolean(false);
        this.o0 = observableBoolean4;
        ObservableBoolean observableBoolean5 = new ObservableBoolean(false);
        this.p0 = observableBoolean5;
        ObservableBoolean observableBoolean6 = new ObservableBoolean(false);
        this.q0 = observableBoolean6;
        CustomObservableField customObservableField2 = new CustomObservableField("");
        this.D0 = customObservableField2;
        ObservableBoolean observableBoolean7 = new ObservableBoolean();
        this.H0 = observableBoolean7;
        this.Z0 = new ObservableField<>(observableBoolean, observableBoolean2, observableBoolean3, observableBoolean4, observableBoolean5, observableBoolean6, observableDouble, observableDouble2, customObservableField, observableDouble3, observableDouble5, observableDouble4, this.f308e, this.f309f, this.u, this.g, this.J, customObservableField2, observableBoolean7);
    }

    public final void a() {
        this.f304a.set("0");
        this.f305b.set("0");
        this.f306c.set("0");
        this.f307d.set("0");
        this.f308e.set("0");
        this.f309f.set("0");
        this.g.set("0");
        this.h.set("0");
        this.i.set("0");
        this.j.set("0");
        this.k.set("0");
        this.l.set("0");
        this.m.set("0");
        this.n.set("0");
        this.o.set("0");
        this.p.set("0");
        this.q.set("0");
        this.r.set("0");
        this.s.set("0");
        this.t.set("0");
        this.u.set("0");
        this.v.set("0");
        this.w.set("0");
        this.x.set("0");
        this.y.set("0");
        this.z.set("0");
        this.A.set("0");
        this.B.set("0");
        this.C.set("0");
        this.D.set("0");
        this.E.set("0");
        this.F.set("0");
        this.G.set("0");
        this.H.set("0");
        this.I.set("0");
        this.J.set("0");
        this.K.set("0");
        this.L.set("0");
        this.M.set("0");
        this.N.set("0");
        this.O.set("0");
        this.P.set("");
        this.Q.set("");
        this.R.set("");
        this.S.set("");
        this.T.set("");
        this.U.set("");
        this.V.set("0");
        this.W.set("");
        this.X.set("0");
        this.Y.set("0");
        this.Z.set("0");
        this.a0.set("0");
        this.b0.set("0");
        this.c0.set("0");
        this.d0.set("0");
        this.e0.set("0");
        this.f0.set(ShadowDrawableWrapper.COS_45);
        this.g0.set(ShadowDrawableWrapper.COS_45);
        this.h0.set("0");
        this.i0.set(ShadowDrawableWrapper.COS_45);
        this.j0.set(ShadowDrawableWrapper.COS_45);
        this.k0.set(ShadowDrawableWrapper.COS_45);
        this.l0.set(false);
        this.m0.set(false);
        this.n0.set(false);
        this.o0.set(false);
        this.p0.set(false);
        this.q0.set(false);
        this.r0.set(null);
        this.s0.set(null);
        this.t0.set(null);
        this.u0.set(null);
        this.v0.set(null);
        this.w0.set(null);
        this.x0.set(null);
        this.y0.set(null);
        this.z0.set(null);
        this.A0.set(null);
        this.B0.set(null);
        this.C0.set(null);
        this.D0.set("");
        this.E0.set(-1);
        this.F0.set("");
        this.G0.set(0);
        this.H0.set(false);
        this.I0.set("");
        this.J0.set("");
        this.K0.set("");
        this.L0.set("0");
        this.M0.set("0");
        this.N0.set("0");
        this.O0.set("0");
        this.P0.set("0");
        this.Q0.set("0");
        this.R0.set("0");
        this.S0.set("0");
        this.T0.set("0");
        this.U0.set("0");
        this.V0.set("0");
        this.W0.set("0");
        this.X0.set("0");
        this.Y0.set("0");
    }
}
