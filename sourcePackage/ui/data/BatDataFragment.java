package com.sermatec.sunmate.ui.data;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import com.sermatec.sunmate.databinding.BatDataFragmentBinding;

/* loaded from: classes.dex */
public class BatDataFragment extends Fragment {
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        BatDataFragmentBinding a2 = BatDataFragmentBinding.a(layoutInflater, viewGroup, false);
        a2.c((DataViewModel) ViewModelProviders.of(requireActivity()).get(DataViewModel.class));
        return a2.getRoot();
    }
}
