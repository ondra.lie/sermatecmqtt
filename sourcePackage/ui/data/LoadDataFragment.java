package com.sermatec.sunmate.ui.data;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import com.sermatec.sunmate.databinding.LoadDataFragmentBinding;

/* loaded from: classes.dex */
public class LoadDataFragment extends Fragment {
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        LoadDataFragmentBinding a2 = LoadDataFragmentBinding.a(layoutInflater, viewGroup, false);
        a2.c((DataViewModel) ViewModelProviders.of(requireActivity()).get(DataViewModel.class));
        return a2.getRoot();
    }
}
