package com.sermatec.sunmate.ui.notifications;

import android.os.Bundle;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import com.google.android.material.tabs.TabLayout;
import com.sermatec.sunmate.R;
import com.sermatec.sunmate.localControl.ConnectionManager;
import com.sermatec.sunmate.ui.notifications.WarnFragment;
import java.util.Arrays;
import java.util.List;

/* loaded from: classes.dex */
public class Notifications extends AppCompatActivity {

    /* renamed from: a  reason: collision with root package name */
    public static final List<String> f320a = Arrays.asList("1E", "1F");

    /* renamed from: b  reason: collision with root package name */
    public boolean f321b;

    /* loaded from: classes.dex */
    public class a extends FragmentPagerAdapter {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ String[] f322a;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(FragmentManager fragmentManager, String[] strArr) {
            super(fragmentManager);
            this.f322a = strArr;
        }

        @Override // androidx.viewpager.widget.PagerAdapter
        public int getCount() {
            return 2;
        }

        @Override // androidx.fragment.app.FragmentPagerAdapter
        @NonNull
        public Fragment getItem(int i) {
            if (i == 0) {
                return new WarnFragment();
            }
            return new WarnFragment.BatteryWarnFragment();
        }

        @Override // androidx.viewpager.widget.PagerAdapter
        public CharSequence getPageTitle(int i) {
            return this.f322a[i];
        }
    }

    /* loaded from: classes.dex */
    public class b implements View.OnClickListener {
        public b() {
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            Notifications.this.onBackPressed();
        }
    }

    public final void a() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new b());
    }

    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.warns);
        a();
        ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
        viewPager.setAdapter(new a(getSupportFragmentManager(), new String[]{getString(R.string.sys_warn), getString(R.string.bat_warn)}));
        ((TabLayout) findViewById(R.id.tabs)).setupWithViewPager(viewPager);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayOptions(supportActionBar.getDisplayOptions() & (-9));
        }
        this.f321b = ConnectionManager.e().h();
        ConnectionManager.e().j(f320a);
    }

    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onDestroy() {
        ConnectionManager.e().q();
        if (this.f321b) {
            ConnectionManager.e().o();
        }
        super.onDestroy();
    }
}
