package com.sermatec.sunmate.ui.notifications;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.sermatec.sunmate.R;
import com.sermatec.sunmate.entity.Warn;
import com.sermatec.sunmate.localControl.ConnectionManager;
import com.sermatec.sunmate.ui.notifications.WarnFragment;
import d.h.a.d.i;
import e.b.l;
import e.b.v.b;
import e.b.y.g;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/* loaded from: classes.dex */
public class WarnFragment extends Fragment {

    /* renamed from: a  reason: collision with root package name */
    public SwipeRefreshLayout f325a;

    /* renamed from: b  reason: collision with root package name */
    public ArrayAdapter<Warn> f326b;

    /* renamed from: c  reason: collision with root package name */
    public b f327c;

    /* loaded from: classes.dex */
    public static class BatteryWarnFragment extends WarnFragment {
        @Override // com.sermatec.sunmate.ui.notifications.WarnFragment
        public List<Warn> b(i iVar) {
            return iVar.d();
        }
    }

    /* loaded from: classes.dex */
    public class a extends ArrayAdapter<Warn> {
        public a(Context context, int i, List list) {
            super(context, i, list);
        }

        @Override // android.widget.ArrayAdapter, android.widget.BaseAdapter, android.widget.SpinnerAdapter
        public View getDropDownView(int i, @Nullable View view, @NonNull ViewGroup viewGroup) {
            return getView(i, view, viewGroup);
        }

        @Override // android.widget.ArrayAdapter, android.widget.Adapter
        @NonNull
        public View getView(int i, @Nullable View view, @NonNull ViewGroup viewGroup) {
            if (view == null) {
                view = LayoutInflater.from(WarnFragment.this.requireContext()).inflate(R.layout.warn_item, viewGroup, false);
            }
            Warn item = getItem(i);
            if (item != null) {
                ((AppCompatTextView) view.findViewById(R.id.desc)).setText(item.getDesc());
                ((AppCompatTextView) view.findViewById(R.id.detail)).setText(WarnFragment.this.d(item));
            }
            return view;
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: f */
    public /* synthetic */ void g() {
        c();
        this.f325a.setRefreshing(false);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: h */
    public /* synthetic */ void i(Long l) {
        c();
    }

    public List<Warn> b(i iVar) {
        return iVar.f();
    }

    public void c() {
        this.f326b.clear();
        this.f326b.addAll(e());
    }

    public final String d(Warn warn) {
        return warn.getDeviceName() + " | " + warn.getSignalName();
    }

    public final List<Warn> e() {
        i iVar;
        ArrayList arrayList = new ArrayList();
        if (ConnectionManager.e().g() && (iVar = (i) ConnectionManager.e().d().pipeline().get(i.class)) != null) {
            arrayList.addAll(b(iVar));
        }
        return arrayList;
    }

    @Override // androidx.fragment.app.Fragment
    @Nullable
    public View onCreateView(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, @Nullable Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.listview, viewGroup, false);
        a aVar = new a(requireContext(), -1, e());
        this.f326b = aVar;
        ((ListView) inflate.findViewById(R.id.container)).setAdapter((ListAdapter) aVar);
        SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) inflate.findViewById(R.id.refresh);
        this.f325a = swipeRefreshLayout;
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() { // from class: d.h.a.e.c.b
            @Override // androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener
            public final void onRefresh() {
                WarnFragment.this.g();
            }
        });
        this.f327c = l.f(1L, TimeUnit.SECONDS).g(e.b.u.c.a.a()).i(new g() { // from class: d.h.a.e.c.a
            @Override // e.b.y.g
            public final void accept(Object obj) {
                WarnFragment.this.i((Long) obj);
            }
        });
        return inflate;
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroyView() {
        this.f327c.dispose();
        super.onDestroyView();
    }
}
