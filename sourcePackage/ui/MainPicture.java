package com.sermatec.sunmate.ui;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.LinearInterpolator;
import androidx.annotation.Nullable;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.databinding.Observable;
import androidx.navigation.Navigation;
import com.google.android.material.shadow.ShadowDrawableWrapper;
import com.sermatec.sunmate.R;
import com.sermatec.sunmate.localControl.ConnectionManager;
import com.sermatec.sunmate.ui.data.DataViewModel;
import java.util.Locale;

/* loaded from: classes.dex */
public class MainPicture extends View {
    public boolean A;
    public boolean B;
    public boolean C;
    public boolean D;
    public final Paint L;
    public final int M;
    public final int N;
    public final int O;
    public final int P;
    public final int Q;
    public final int R;
    public final int S;
    public final int T;
    public Shader U;
    public Shader V;
    public Shader W;

    /* renamed from: a  reason: collision with root package name */
    public final Drawable f203a;
    public Shader a0;

    /* renamed from: b  reason: collision with root package name */
    public final Drawable f204b;
    public Shader b0;

    /* renamed from: c  reason: collision with root package name */
    public final Drawable f205c;
    public Shader c0;

    /* renamed from: d  reason: collision with root package name */
    public final Drawable f206d;
    public Observable.OnPropertyChangedCallback d0;

    /* renamed from: e  reason: collision with root package name */
    public final Drawable f207e;
    public Observable.OnPropertyChangedCallback e0;

    /* renamed from: f  reason: collision with root package name */
    public final Drawable f208f;
    public DataViewModel f0;
    public final Drawable g;
    public int g0;
    public Drawable h;
    public int h0;
    public final Drawable i;
    public final Paint j;
    public final Path n;
    public final Paint o;
    public d o0;
    public final Paint p;
    public d p0;
    public double q;
    public e q0;
    public double r;
    public e r0;
    public double s;
    public e s0;
    public e t0;
    public final ValueAnimator u0;
    public boolean w;
    public boolean x;
    public boolean y;
    public boolean z;
    public double t;
    public double u;
    public double v = this.t + this.u;
    public String E = "0.00w";
    public String F = "0.00w";
    public String G = "0.00%";
    public String H = "0.00w";
    public String I = "0.00w";
    public String J = "0.00w";
    public String K = "";
    public PointF i0 = new PointF();
    public PointF j0 = new PointF();
    public PointF k0 = new PointF();
    public PointF l0 = new PointF();
    public PointF m0 = new PointF();
    public PointF n0 = new PointF();
    public final RectF k = new RectF();
    public final Rect m = new Rect();
    public final Rect l = new Rect();

    /* loaded from: classes.dex */
    public class a extends Observable.OnPropertyChangedCallback {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ DataViewModel f209a;

        public a(DataViewModel dataViewModel) {
            this.f209a = dataViewModel;
        }

        @Override // androidx.databinding.Observable.OnPropertyChangedCallback
        public void onPropertyChanged(Observable observable, int i) {
            MainPicture.this.w = this.f209a.l0.get();
            MainPicture.this.x = this.f209a.m0.get();
            MainPicture.this.y = this.f209a.n0.get();
            MainPicture.this.z = this.f209a.o0.get();
            MainPicture.this.A = this.f209a.p0.get();
            MainPicture.this.B = this.f209a.q0.get();
            if (MainPicture.this.f0.E0.get() == 0) {
                MainPicture.this.q = ShadowDrawableWrapper.COS_45;
                MainPicture.this.r = ShadowDrawableWrapper.COS_45;
                MainPicture.this.s = ShadowDrawableWrapper.COS_45;
                MainPicture.this.t = ShadowDrawableWrapper.COS_45;
                MainPicture.this.u = ShadowDrawableWrapper.COS_45;
                MainPicture.this.v = ShadowDrawableWrapper.COS_45;
                MainPicture.this.E = String.format(Locale.CHINA, "%.0fw", Double.valueOf((double) ShadowDrawableWrapper.COS_45));
                MainPicture.this.F = String.format(Locale.CHINA, "%.0fw", Double.valueOf((double) ShadowDrawableWrapper.COS_45));
                MainPicture.this.G = String.format(Locale.CHINA, "%s%%", "0");
                MainPicture.this.H = String.format(Locale.CHINA, "%sw", "0");
                MainPicture.this.I = String.format(Locale.CHINA, "%sw", "0");
                MainPicture.this.J = String.format(Locale.CHINA, "%.0fw", Double.valueOf((double) ShadowDrawableWrapper.COS_45));
            } else {
                MainPicture.this.q = this.f209a.f0.get();
                MainPicture.this.r = this.f209a.g0.get();
                MainPicture.this.s = this.f209a.i0.get();
                MainPicture.this.t = this.f209a.k0.get();
                MainPicture.this.u = this.f209a.j0.get();
                MainPicture.this.v = this.f209a.k0.get() + this.f209a.j0.get();
                MainPicture.this.E = String.format(Locale.CHINA, "%.0fw", Double.valueOf(Double.parseDouble(this.f209a.f308e.get()) + Double.parseDouble(this.f209a.f309f.get())));
                MainPicture.this.F = String.format(Locale.CHINA, "%sw", this.f209a.h0.get());
                MainPicture.this.G = String.format(Locale.CHINA, "%s%%", this.f209a.J.get());
                MainPicture.this.H = String.format(Locale.CHINA, "%sw", this.f209a.u.get());
                MainPicture.this.I = String.format(Locale.CHINA, "%sw", this.f209a.g.get());
                MainPicture.this.J = String.format(Locale.CHINA, "%.0fw", Double.valueOf(this.f209a.j0.get()));
            }
            MainPicture.this.K = this.f209a.D0.get();
            MainPicture.this.D = this.f209a.H0.get();
        }
    }

    /* loaded from: classes.dex */
    public class b extends Observable.OnPropertyChangedCallback {
        public b() {
        }

        @Override // androidx.databinding.Observable.OnPropertyChangedCallback
        public void onPropertyChanged(Observable observable, int i) {
            MainPicture.this.C = ConnectionManager.e().g();
        }
    }

    /* loaded from: classes.dex */
    public class c implements ValueAnimator.AnimatorUpdateListener {

        /* renamed from: a  reason: collision with root package name */
        public float f212a = 0.0f;

        /* renamed from: b  reason: collision with root package name */
        public long f213b = 0;

        public c() {
        }

        @Override // android.animation.ValueAnimator.AnimatorUpdateListener
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            if (MainPicture.this.g0 != 0 && MainPicture.this.h0 != 0) {
                float animatedFraction = valueAnimator.getAnimatedFraction();
                if (MainPicture.this.q != ShadowDrawableWrapper.COS_45) {
                    MainPicture mainPicture = MainPicture.this;
                    mainPicture.i0 = mainPicture.o0.f(animatedFraction);
                }
                if (MainPicture.this.r != ShadowDrawableWrapper.COS_45) {
                    if ((MainPicture.this.r < ShadowDrawableWrapper.COS_45 && !MainPicture.this.p0.f219e) || (MainPicture.this.r > ShadowDrawableWrapper.COS_45 && MainPicture.this.p0.f219e)) {
                        MainPicture.this.p0.g();
                    }
                    MainPicture mainPicture2 = MainPicture.this;
                    mainPicture2.j0 = mainPicture2.p0.f(animatedFraction);
                }
                if (MainPicture.this.t != ShadowDrawableWrapper.COS_45) {
                    if ((MainPicture.this.t < ShadowDrawableWrapper.COS_45 && !MainPicture.this.q0.f222c) || (MainPicture.this.t > ShadowDrawableWrapper.COS_45 && MainPicture.this.q0.f222c)) {
                        MainPicture.this.q0.e();
                    }
                    MainPicture mainPicture3 = MainPicture.this;
                    mainPicture3.k0 = mainPicture3.q0.d(animatedFraction);
                }
                if (MainPicture.this.v != ShadowDrawableWrapper.COS_45) {
                    if ((MainPicture.this.v < ShadowDrawableWrapper.COS_45 && !MainPicture.this.t0.f222c) || (MainPicture.this.v > ShadowDrawableWrapper.COS_45 && MainPicture.this.t0.f222c)) {
                        MainPicture.this.t0.e();
                    }
                    MainPicture mainPicture4 = MainPicture.this;
                    mainPicture4.n0 = mainPicture4.t0.d(animatedFraction);
                }
                if (MainPicture.this.u != ShadowDrawableWrapper.COS_45) {
                    MainPicture mainPicture5 = MainPicture.this;
                    mainPicture5.l0 = mainPicture5.r0.d(animatedFraction);
                }
                if (MainPicture.this.s != ShadowDrawableWrapper.COS_45) {
                    MainPicture mainPicture6 = MainPicture.this;
                    mainPicture6.m0 = mainPicture6.s0.d(animatedFraction);
                }
                if (animatedFraction < this.f212a) {
                    this.f213b++;
                }
                if (this.f213b % 2 != 0) {
                    animatedFraction = 1.0f - animatedFraction;
                }
                this.f212a = valueAnimator.getAnimatedFraction();
                int i = (int) (animatedFraction * 255.0f);
                MainPicture.this.f203a.setAlpha(MainPicture.this.w ? i : 255);
                DrawableCompat.setTint(MainPicture.this.f203a, MainPicture.this.w ? MainPicture.this.O : MainPicture.this.N);
                MainPicture.this.f205c.setAlpha(MainPicture.this.x ? i : 255);
                DrawableCompat.setTint(MainPicture.this.f205c, MainPicture.this.x ? MainPicture.this.P : MainPicture.this.N);
                MainPicture.this.f204b.setAlpha(MainPicture.this.z ? i : 255);
                DrawableCompat.setTint(MainPicture.this.f204b, MainPicture.this.z ? MainPicture.this.Q : MainPicture.this.N);
                MainPicture.this.f207e.setAlpha(MainPicture.this.A ? i : 255);
                DrawableCompat.setTint(MainPicture.this.f207e, MainPicture.this.A ? MainPicture.this.T : MainPicture.this.N);
                MainPicture.this.i.setAlpha(MainPicture.this.B ? i : 255);
                DrawableCompat.setTint(MainPicture.this.i, MainPicture.this.B ? MainPicture.this.R : MainPicture.this.N);
                Drawable drawable = MainPicture.this.f206d;
                if (!MainPicture.this.y) {
                    i = 255;
                }
                drawable.setAlpha(i);
                DrawableCompat.setTint(MainPicture.this.f206d, MainPicture.this.y ? MainPicture.this.S : MainPicture.this.N);
                MainPicture mainPicture7 = MainPicture.this;
                mainPicture7.h = mainPicture7.C ? MainPicture.this.f208f : MainPicture.this.g;
                MainPicture.this.invalidate();
            }
        }
    }

    /* loaded from: classes.dex */
    public static class d {

        /* renamed from: a  reason: collision with root package name */
        public PointF f215a;

        /* renamed from: b  reason: collision with root package name */
        public PointF f216b;

        /* renamed from: c  reason: collision with root package name */
        public final PointF f217c;

        /* renamed from: d  reason: collision with root package name */
        public final PointF f218d;

        /* renamed from: e  reason: collision with root package name */
        public boolean f219e;

        public d(PointF pointF, PointF pointF2, PointF pointF3, PointF pointF4) {
            this.f215a = pointF;
            this.f216b = pointF2;
            this.f217c = pointF3;
            this.f218d = pointF4;
        }

        public PointF f(float f2) {
            double d2 = 1.0f - f2;
            double pow = Math.pow(d2, 3.0d);
            double d3 = this.f215a.x;
            Double.isNaN(d3);
            double d4 = f2;
            Double.isNaN(d4);
            double d5 = this.f217c.x;
            Double.isNaN(d5);
            double pow2 = (pow * d3) + (Math.pow(d2, 2.0d) * 3.0d * d4 * d5);
            Double.isNaN(d2);
            Double.isNaN(d4);
            Double.isNaN(d4);
            double d6 = d2 * 3.0d * d4 * d4;
            double d7 = this.f218d.x;
            Double.isNaN(d7);
            double d8 = pow2 + (d7 * d6);
            float f3 = f2 * f2 * f2;
            double d9 = this.f216b.x * f3;
            Double.isNaN(d9);
            double d10 = d8 + d9;
            double pow3 = Math.pow(d2, 3.0d);
            double d11 = this.f215a.y;
            Double.isNaN(d11);
            Double.isNaN(d4);
            double d12 = this.f217c.y;
            Double.isNaN(d12);
            double pow4 = (pow3 * d11) + (Math.pow(d2, 2.0d) * 3.0d * d4 * d12);
            double d13 = this.f218d.y;
            Double.isNaN(d13);
            double d14 = f3 * this.f216b.y;
            Double.isNaN(d14);
            return new PointF((float) d10, (float) (pow4 + (d6 * d13) + d14));
        }

        public void g() {
            PointF pointF = this.f215a;
            this.f215a = this.f216b;
            this.f216b = pointF;
            this.f219e = !this.f219e;
        }
    }

    /* loaded from: classes.dex */
    public static class e {

        /* renamed from: a  reason: collision with root package name */
        public PointF f220a;

        /* renamed from: b  reason: collision with root package name */
        public PointF f221b;

        /* renamed from: c  reason: collision with root package name */
        public boolean f222c;

        public e(PointF pointF, PointF pointF2) {
            this.f220a = pointF;
            this.f221b = pointF2;
        }

        public PointF d(float f2) {
            PointF pointF;
            PointF pointF2 = this.f220a;
            float f3 = pointF2.x;
            double d2 = f3 + ((this.f221b.x - f3) * f2);
            float f4 = pointF2.y;
            return new PointF((float) d2, f4 + ((pointF.y - f4) * f2));
        }

        public void e() {
            PointF pointF = this.f220a;
            this.f220a = this.f221b;
            this.f221b = pointF;
            this.f222c = !this.f222c;
        }
    }

    @SuppressLint({"UseCompatLoadingForDrawables"})
    public MainPicture(Context context, @Nullable AttributeSet attributeSet) {
        super(context, attributeSet);
        Paint paint = new Paint(1);
        this.j = paint;
        paint.setColor(context.getResources().getColor(R.color.colorPrimary));
        paint.setStrokeWidth(o0(context, 1.0f));
        paint.setStyle(Paint.Style.STROKE);
        Paint paint2 = new Paint(1);
        this.L = paint2;
        paint2.setTextSize(r0(context, 12.0f));
        paint2.setColor(context.getResources().getColor(R.color.textColor));
        int color = context.getResources().getColor(R.color.colorPrimary);
        this.M = color;
        this.N = context.getResources().getColor(R.color.colorBorder);
        Paint paint3 = new Paint(1);
        this.o = paint3;
        paint3.setColor(color);
        paint3.setStyle(Paint.Style.FILL);
        paint3.setShadowLayer(o0(context, 2.0f), 0.0f, 0.0f, color);
        Paint paint4 = new Paint();
        this.p = paint4;
        paint4.setColor(context.getResources().getColor(R.color.window_background));
        paint4.setStyle(Paint.Style.FILL_AND_STROKE);
        paint4.setStrokeWidth(o0(context, 1.0f));
        Drawable mutate = context.getResources().getDrawable(R.drawable.pv_vector).mutate();
        this.f203a = mutate;
        int color2 = context.getResources().getColor(R.color.color5);
        this.O = color2;
        DrawableCompat.setTint(mutate, color2);
        Drawable mutate2 = context.getResources().getDrawable(R.drawable.grid_vector).mutate();
        this.f204b = mutate2;
        int color3 = context.getResources().getColor(R.color.color7);
        this.Q = color3;
        DrawableCompat.setTint(mutate2, color3);
        Drawable mutate3 = context.getResources().getDrawable(R.drawable.ic_battery).mutate();
        this.f205c = mutate3;
        int color4 = context.getResources().getColor(R.color.color1);
        this.P = color4;
        DrawableCompat.setTint(mutate3, color4);
        Drawable mutate4 = context.getResources().getDrawable(R.drawable.load_vector).mutate();
        this.f206d = mutate4;
        int color5 = context.getResources().getColor(R.color.color3);
        this.S = color5;
        DrawableCompat.setTint(mutate4, color5);
        Drawable mutate5 = context.getResources().getDrawable(R.drawable.ic_remote_load).mutate();
        this.f207e = mutate5;
        int color6 = context.getResources().getColor(R.color.color4);
        this.T = color6;
        DrawableCompat.setTint(mutate5, color6);
        int b2 = d.h.a.e.e.d.b(context);
        if (b2 == -1 || b2 == 1) {
            Drawable drawable = context.getResources().getDrawable(R.drawable.inverter_offline);
            this.g = drawable;
            this.h = drawable;
            this.f208f = context.getResources().getDrawable(R.drawable.inverter_online);
        } else {
            Drawable drawable2 = context.getResources().getDrawable(R.drawable.dark_inverter_offline);
            this.g = drawable2;
            this.h = drawable2;
            this.f208f = context.getResources().getDrawable(R.drawable.dark_inverter_online);
        }
        Drawable mutate6 = context.getResources().getDrawable(R.drawable.meter_vector).mutate();
        this.i = mutate6;
        int color7 = context.getResources().getColor(R.color.color7);
        this.R = color7;
        DrawableCompat.setTint(mutate6, color7);
        this.n = new Path();
        ValueAnimator duration = ValueAnimator.ofInt(0, 1).setDuration(2000L);
        this.u0 = duration;
        duration.setInterpolator(new LinearInterpolator());
        duration.setRepeatMode(1);
        duration.setRepeatCount(-1);
        duration.addUpdateListener(new c());
    }

    public static int o0(Context context, float f2) {
        return (int) ((f2 * context.getResources().getDisplayMetrics().density) + 0.5f);
    }

    public static float r0(Context context, float f2) {
        return TypedValue.applyDimension(2, f2, context.getResources().getDisplayMetrics());
    }

    public void n0(DataViewModel dataViewModel) {
        this.f0 = dataViewModel;
        a aVar = new a(dataViewModel);
        this.d0 = aVar;
        dataViewModel.Z0.addOnPropertyChangedCallback(aVar);
        dataViewModel.Z0.notifyChange();
        this.C = ConnectionManager.e().g();
        this.e0 = new b();
        ConnectionManager.e().addOnPropertyChangedCallback(this.e0);
    }

    @Override // android.view.View
    public void onAttachedToWindow() {
        this.u0.start();
        super.onAttachedToWindow();
    }

    @Override // android.view.View
    public void onDetachedFromWindow() {
        this.u0.cancel();
        super.onDetachedFromWindow();
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        this.o.setShader(this.c0);
        canvas.drawRect(0.0f, 0.0f, this.g0, this.h0, this.o);
        this.o.setShader(null);
        Context context = getContext();
        int o0 = o0(context, 48.0f);
        int o02 = o0(context, 8.0f);
        int o03 = o0(context, 4.0f);
        int i = o02 * 2;
        int i2 = o02 * 4;
        int i3 = o0 * 2;
        int o04 = o0(context, 32.0f);
        int o05 = o0(context, 32.0f);
        String str = this.K;
        float f2 = o0;
        canvas.drawText(str, (this.g0 / 2.0f) - (this.L.measureText(str) / 2.0f), f2, this.L);
        float f3 = o02;
        float f4 = o0 + o02;
        q0(f3, f3, f4, f4, o02);
        if (this.w) {
            this.j.setShadowLayer(f3, 0.0f, 0.0f, this.O);
            this.j.setColor(this.O);
        } else {
            this.j.setShadowLayer(f3, 0.0f, 0.0f, this.N);
            this.j.setColor(this.N);
        }
        float f5 = f2 / 2.0f;
        canvas.drawCircle(this.k.centerX(), this.k.centerY(), f5, this.j);
        this.j.setShader(this.w ? this.U : null);
        this.f203a.setBounds(this.m);
        this.f203a.draw(canvas);
        float f6 = i;
        float f7 = f5 + f6;
        float f8 = o0 + i2;
        canvas.drawText(this.E, f7, f8, this.L);
        this.n.moveTo(this.o0.f215a.x, this.o0.f215a.y);
        this.n.cubicTo(this.o0.f217c.x, this.o0.f217c.y, this.o0.f218d.x, this.o0.f218d.y, this.o0.f216b.x, this.o0.f216b.y);
        canvas.drawPath(this.n, this.j);
        if (this.q != ShadowDrawableWrapper.COS_45 && this.w) {
            this.o.setColor(this.O);
            PointF pointF = this.i0;
            canvas.drawCircle(pointF.x, pointF.y, o03, this.o);
        }
        this.j.setShader(null);
        int i4 = this.g0;
        q0((i4 - o0) - o02, f3, i4 - o02, f4, o02);
        if (this.z) {
            this.j.setShadowLayer(f3, 0.0f, 0.0f, this.Q);
            this.j.setColor(this.Q);
        } else {
            this.j.setShadowLayer(f3, 0.0f, 0.0f, this.N);
            this.j.setColor(this.N);
        }
        canvas.drawCircle(this.k.centerX(), this.k.centerY(), f5, this.j);
        this.j.setShader(this.z ? this.W : null);
        this.f204b.setBounds(this.m);
        this.f204b.draw(canvas);
        String str2 = this.I;
        canvas.drawText(str2, (this.g0 - o0) - this.L.measureText(str2), f8, this.L);
        this.n.reset();
        this.n.moveTo(this.q0.f220a.x, this.q0.f220a.y);
        this.n.lineTo(this.q0.f221b.x, this.q0.f221b.y);
        canvas.drawPath(this.n, this.j);
        if (this.t != ShadowDrawableWrapper.COS_45 && this.z) {
            this.o.setColor(this.Q);
            PointF pointF2 = this.k0;
            canvas.drawCircle(pointF2.x, pointF2.y, o03, this.o);
        }
        this.j.setShader(null);
        int i5 = this.h0;
        q0(f3, (i5 - o0) - o02, f4, i5 - o02, o02);
        if (this.x) {
            this.j.setShadowLayer(f3, 0.0f, 0.0f, this.P);
            this.j.setColor(this.P);
        } else {
            this.j.setShadowLayer(f3, 0.0f, 0.0f, this.N);
            this.j.setColor(this.N);
        }
        canvas.drawCircle(this.k.centerX(), this.k.centerY(), f5, this.j);
        this.j.setShader(this.x ? this.V : null);
        this.f205c.setBounds(this.m);
        this.f205c.draw(canvas);
        canvas.drawText(this.F, f7, (this.h0 - o0) - i2, this.L);
        if (this.D) {
            canvas.drawText(this.G, o0 + i, this.h0 - f5, this.L);
        }
        this.n.reset();
        this.n.moveTo(this.p0.f215a.x, this.p0.f215a.y);
        this.n.cubicTo(this.p0.f217c.x, this.p0.f217c.y, this.p0.f218d.x, this.p0.f218d.y, this.p0.f216b.x, this.p0.f216b.y);
        canvas.drawPath(this.n, this.j);
        if (this.r != ShadowDrawableWrapper.COS_45 && this.x) {
            this.o.setColor(this.P);
            PointF pointF3 = this.j0;
            canvas.drawCircle(pointF3.x, pointF3.y, o03, this.o);
        }
        this.j.setShader(null);
        int i6 = this.g0;
        int i7 = this.h0;
        q0((i6 - o0) - o02, (i7 - o0) - o02, i6 - o02, i7 - o02, o02);
        if (this.A) {
            this.j.setShadowLayer(f3, 0.0f, 0.0f, this.T);
            this.j.setColor(this.T);
        } else {
            this.j.setShadowLayer(f3, 0.0f, 0.0f, this.N);
            this.j.setColor(this.N);
        }
        canvas.drawCircle(this.k.centerX(), this.k.centerY(), f5, this.j);
        this.j.setShader(this.A ? this.b0 : null);
        this.f207e.setBounds(this.m);
        this.f207e.draw(canvas);
        canvas.drawText(this.J, ((this.g0 - f5) - f6) - this.L.measureText(this.F), (this.h0 - o0) - i2, this.L);
        this.n.reset();
        this.n.moveTo(this.r0.f220a.x, this.r0.f220a.y);
        this.n.lineTo(this.r0.f221b.x, this.r0.f221b.y);
        canvas.drawPath(this.n, this.j);
        if (this.u != ShadowDrawableWrapper.COS_45 && this.A) {
            this.o.setColor(this.T);
            PointF pointF4 = this.l0;
            canvas.drawCircle(pointF4.x, pointF4.y, o03, this.o);
        }
        this.j.setShader(null);
        int i8 = this.g0;
        int i9 = this.h0;
        q0((i8 - i3) / 2.0f, (i9 - i3) / 2.0f, (i8 + i3) / 2.0f, (i9 + i3) / 2.0f, o02);
        if (this.C) {
            this.j.setShadowLayer(f3, 0.0f, 0.0f, this.M);
            this.j.setColor(this.M);
        } else {
            this.j.setShadowLayer(f3, 0.0f, 0.0f, this.N);
            this.j.setColor(this.N);
        }
        this.h.setBounds(this.l);
        this.h.draw(canvas);
        if (this.z || this.A) {
            this.j.setShadowLayer(f3, 0.0f, 0.0f, this.M);
            this.j.setColor(this.M);
        } else {
            this.j.setShadowLayer(f3, 0.0f, 0.0f, this.N);
            this.j.setColor(this.N);
        }
        this.n.reset();
        this.n.moveTo(this.t0.f220a.x, this.t0.f220a.y);
        this.n.lineTo(this.t0.f221b.x, this.t0.f221b.y);
        canvas.drawPath(this.n, this.j);
        if (this.v != ShadowDrawableWrapper.COS_45 && (this.z || this.A)) {
            this.o.setColor(this.M);
            PointF pointF5 = this.n0;
            canvas.drawCircle(pointF5.x, pointF5.y, o03, this.o);
        }
        int i10 = this.g0;
        int i11 = this.h0;
        q0((i10 - o0) / 2.0f, (i11 - o0) - o02, (i10 + o0) / 2.0f, i11 - o02, o02);
        if (this.y) {
            this.j.setShadowLayer(f3, 0.0f, 0.0f, this.S);
            this.j.setColor(this.S);
        } else {
            this.j.setShadowLayer(f3, 0.0f, 0.0f, this.N);
            this.j.setColor(this.N);
        }
        canvas.drawCircle(this.k.centerX(), this.k.centerY(), f5, this.j);
        this.j.setShader(this.y ? this.a0 : null);
        this.f206d.setBounds(this.m);
        this.f206d.draw(canvas);
        canvas.drawText(this.H, (this.g0 / 2.0f) + f3, (this.h0 - o0) - i2, this.L);
        this.n.reset();
        this.n.moveTo(this.s0.f220a.x, this.s0.f220a.y);
        this.n.lineTo(this.s0.f221b.x, this.s0.f221b.y);
        canvas.drawPath(this.n, this.j);
        if (this.s != ShadowDrawableWrapper.COS_45 && this.y) {
            this.o.setColor(this.S);
            PointF pointF6 = this.m0;
            canvas.drawCircle(pointF6.x, pointF6.y, o03, this.o);
        }
        this.j.setShader(null);
        int i12 = this.g0;
        float f9 = o04 / 2.0f;
        q0(((i12 - f5) - f9) - f3, o0 + o05, ((i12 - f5) + f9) - f3, o0 + o04 + o05, o02);
        if (this.B) {
            this.j.setShadowLayer(f3, 0.0f, 0.0f, this.R);
            this.j.setColor(this.R);
        } else {
            this.j.setShadowLayer(f3, 0.0f, 0.0f, this.N);
            this.j.setColor(this.N);
        }
        canvas.drawCircle(this.k.centerX(), this.k.centerY(), f9, this.p);
        canvas.drawCircle(this.k.centerX(), this.k.centerY(), f9, this.j);
        this.i.setBounds(this.m);
        this.i.draw(canvas);
    }

    @Override // android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        this.g0 = i;
        this.h0 = i2;
        p0();
    }

    @Override // android.view.View
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 1) {
            if (this.f203a.getBounds().contains((int) motionEvent.getX(), (int) motionEvent.getY())) {
                Navigation.findNavController(this).navigate(R.id.data_to_pv_data);
            } else if (this.f205c.getBounds().contains((int) motionEvent.getX(), (int) motionEvent.getY())) {
                Navigation.findNavController(this).navigate(R.id.data_to_bat_data);
            } else if (this.f204b.getBounds().contains((int) motionEvent.getX(), (int) motionEvent.getY())) {
                Navigation.findNavController(this).navigate(R.id.data_to_grid_data);
            } else if (this.f206d.getBounds().contains((int) motionEvent.getX(), (int) motionEvent.getY())) {
                Navigation.findNavController(this).navigate(R.id.data_to_load_data);
            }
        }
        return true;
    }

    public final void p0() {
        Context context = getContext();
        int o0 = o0(context, 48.0f);
        int o02 = o0(context, 8.0f);
        int i = o0 * 2;
        float f2 = o0 / 2.0f;
        float f3 = o02;
        float f4 = f2 + f3;
        float f5 = o0 + o02;
        float f6 = o02 * 4;
        this.o0 = new d(new PointF(f4, f5), new PointF((this.g0 - i) / 2.0f, (this.h0 / 2.0f) - f6), new PointF(f4, (this.h0 / 2.0f) - f6), new PointF(f4, (this.h0 / 2.0f) - f6));
        this.p0 = new d(new PointF(f4, (this.h0 - o0) - o02), new PointF((this.g0 - i) / 2.0f, (this.h0 / 2.0f) + f6), new PointF(f4, (this.h0 / 2.0f) + f6), new PointF(f4, (this.h0 / 2.0f) + f6));
        this.q0 = new e(new PointF((this.g0 - f2) - f3, this.h0 / 2.0f), new PointF((this.g0 - f2) - f3, f5));
        this.r0 = new e(new PointF((this.g0 - f2) - f3, this.h0 / 2.0f), new PointF((this.g0 - f2) - f3, (this.h0 - o0) - o02));
        this.s0 = new e(new PointF(this.g0 / 2.0f, ((this.h0 + i) - (f3 / 2.0f)) / 2.0f), new PointF(this.g0 / 2.0f, (this.h0 - o0) - o02));
        this.t0 = new e(new PointF((this.g0 + i) / 2.0f, this.h0 / 2.0f), new PointF((this.g0 - f2) - f3, this.h0 / 2.0f));
        this.U = new LinearGradient(this.o0.f215a.x, this.o0.f215a.y, this.o0.f216b.x, this.o0.f216b.y, this.O, this.M, Shader.TileMode.CLAMP);
        this.V = new LinearGradient(this.p0.f215a.x, this.p0.f215a.y, this.p0.f216b.x, this.p0.f216b.y, this.P, this.M, Shader.TileMode.CLAMP);
        this.a0 = new LinearGradient(this.s0.f220a.x, this.s0.f220a.y, this.s0.f221b.x, this.s0.f221b.y, this.M, this.S, Shader.TileMode.CLAMP);
        this.b0 = new LinearGradient(this.r0.f220a.x, this.r0.f220a.y, this.r0.f221b.x, this.r0.f221b.y, this.M, this.T, Shader.TileMode.CLAMP);
        this.W = new LinearGradient(this.q0.f220a.x, this.q0.f220a.y, this.q0.f221b.x, this.q0.f221b.y, this.M, this.Q, Shader.TileMode.CLAMP);
        int i2 = this.g0;
        int i3 = this.h0;
        this.c0 = new RadialGradient(i2 / 2.0f, i3 / 2.0f, Math.min(i2 / 2.0f, i3 / 2.0f), new int[]{context.getResources().getColor(R.color.background), context.getResources().getColor(R.color.window_background)}, new float[]{0.4f, 1.0f}, Shader.TileMode.CLAMP);
    }

    public final void q0(float f2, float f3, float f4, float f5, int i) {
        this.k.set(f2, f3, f4, f5);
        Rect rect = this.m;
        RectF rectF = this.k;
        float f6 = i;
        rect.set((int) (rectF.left + f6), (int) (rectF.top + f6), (int) (rectF.right - f6), (int) (rectF.bottom - f6));
        this.l.set((int) f2, (int) f3, (int) f4, (int) f5);
    }

    public void s0() {
        ConnectionManager.e().removeOnPropertyChangedCallback(this.e0);
        DataViewModel dataViewModel = this.f0;
        if (dataViewModel != null) {
            dataViewModel.Z0.removeOnPropertyChangedCallback(this.d0);
        }
    }
}
