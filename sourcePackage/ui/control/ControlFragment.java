package com.sermatec.sunmate.ui.control;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListPopupWindow;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.ArrayRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.alibaba.fastjson.JSONArray;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.sermatec.sunmate.MainActivity;
import com.sermatec.sunmate.R;
import com.sermatec.sunmate.entity.TimeType;
import com.sermatec.sunmate.entity.WorkParamMode;
import com.sermatec.sunmate.localControl.ConnectionManager;
import com.sermatec.sunmate.ui.control.ControlFragment;
import com.sermatec.sunmate.ui.control.TimePickerDialogWrap;
import d.h.a.e.a.q2;
import d.h.a.e.e.b;
import d.h.a.e.e.c;
import e.b.m;
import io.netty.channel.ChannelFuture;
import io.netty.handler.codec.http.multipart.HttpPostBodyUtil;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.concurrent.TimeUnit;

/* loaded from: classes.dex */
public class ControlFragment extends Fragment {
    public Context B;
    public volatile ListPopupWindow D;
    public ArrayAdapter<String> E;
    public e.b.v.b I;
    public e.b.v.b J;
    public e.b.v.b K;
    public e.b.l<Long> L;
    public Drawable M;
    public Drawable N;
    public InputMethodManager O;
    public boolean P;

    /* renamed from: a  reason: collision with root package name */
    public TimeTypeAdapter f223a;
    @BindView
    public TextView add_price_time;

    /* renamed from: b  reason: collision with root package name */
    public TimePickerDialogWrap f224b;
    @BindView
    public TextView btn_work_mode2;
    @BindView
    public TextView btn_work_mode_sure;

    /* renamed from: c  reason: collision with root package name */
    public KProgressHUD f225c;
    @BindView
    public TextView changWifiPwdOk;

    /* renamed from: d  reason: collision with root package name */
    public int f226d;

    /* renamed from: e  reason: collision with root package name */
    public v f227e;
    @BindView
    public EditText ensure_new_wifi_password;
    @BindView
    public EditText etAh;
    @BindView
    public EditText etRouterPwd;
    @BindView
    public TextView etServerIp;
    @BindView
    public EditText etServerPort;

    /* renamed from: f  reason: collision with root package name */
    public String f228f;
    public d.a.a.k.b<String> g;
    public t h;
    public d.a.a.k.b<String> i;
    @BindView
    public TextView influx;
    public d.a.a.k.b<String> j;
    public d.a.a.k.b<String> k;
    public d.a.a.k.b<String> l;
    public d.a.a.k.b<String> m;
    @BindView
    public RadioButton mRadioOpen;
    @BindView
    public View meterMonitor_parent;
    @BindView
    public TextView meter_check;
    @BindView
    public EditText mode_grid_power;
    @BindView
    public EditText mode_price_flat;
    @BindView
    public EditText mode_price_peak;
    @BindView
    public EditText mode_price_top;
    @BindView
    public EditText mode_price_valley;
    @BindView
    public TextView mode_type;
    public d.a.a.k.b<String> n;
    @BindView
    public EditText new_wifi_password;
    public d.a.a.k.b<String> o;
    @BindView
    public View offGridSoc_parent;
    @BindView
    public EditText off_grid_soc_limit;
    @BindView
    public RadioGroup onOff;
    @BindView
    public TextView onOffOk;
    @BindView
    public EditText on_grid_soc_limit;
    public d.a.a.k.b<String> p;
    public d.a.a.k.b<String> q;
    public t r;
    @BindView
    public RadioButton radio_close;
    @BindView
    public TextView resetOk;
    @BindView
    public TextView routerOk;
    @BindView
    public TextView routerssid;
    public t s;
    @BindView
    public TextView serverOk;
    public t t;
    @BindView
    public View threePhase_parent;
    @BindView
    public TextView three_phrase_out;
    @BindView
    public RecyclerView time_type_container;
    @BindView
    public View time_type_container_parent;
    @BindView
    public TextView tvDianBiao;
    @BindView
    public TextView tvDianChi;
    @BindView
    public TextView tvDianChiType;
    @BindView
    public TextView tvDianWang;
    @BindView
    public TextView tvRouterMode;
    @BindView
    public View tv_ah_parent;
    @BindView
    public View tv_dianchi_protocol_parent;
    public t u;
    @BindView
    public TextView updateDspOk;
    @BindView
    public TextView updatePucOk;
    public t v;
    public t w;
    @BindView
    public TextView workModeOk;
    public t x;
    public t y;
    public Dialog z;
    public Handler A = new Handler();
    public List<String> C = new ArrayList();
    public q2 F = new q2();
    public BroadcastReceiver G = new j();
    public BroadcastReceiver H = new k();

    /* loaded from: classes.dex */
    public class a implements View.OnTouchListener {
        public a() {
        }

        @Override // android.view.View.OnTouchListener
        public boolean onTouch(View view, MotionEvent motionEvent) {
            boolean z = motionEvent.getX() > ((float) (ControlFragment.this.routerssid.getWidth() - ControlFragment.this.routerssid.getCompoundPaddingEnd()));
            if (z && motionEvent.getAction() == 1) {
                ControlFragment controlFragment = ControlFragment.this;
                controlFragment.routerssid.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, controlFragment.M, (Drawable) null);
                ControlFragment.this.h1();
            }
            return z;
        }
    }

    /* loaded from: classes.dex */
    public class b extends v {
        public b(TextView textView, int i, Context context) {
            super(textView, i, context);
        }

        @Override // java.lang.Runnable
        public void run() {
            ControlFragment controlFragment = ControlFragment.this;
            d.h.a.d.g<Integer, e.b.l<Integer>> m = controlFragment.F.m(controlFragment.f228f);
            int intValue = m.a().intValue();
            if (intValue != 0) {
                i(intValue);
            } else {
                g(m.b());
            }
        }
    }

    /* loaded from: classes.dex */
    public class c extends v {
        public c(TextView textView, int i, Context context) {
            super(textView, i, context);
        }

        @Override // java.lang.Runnable
        public void run() {
            ControlFragment controlFragment = ControlFragment.this;
            d.h.a.d.g<Integer, e.b.l<Integer>> l = controlFragment.F.l(controlFragment.f228f);
            int intValue = l.a().intValue();
            if (intValue != 0) {
                i(intValue);
            } else {
                g(l.b());
            }
        }
    }

    /* loaded from: classes.dex */
    public class d implements c.AbstractC0014c {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ Context f232a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ Intent f233b;

        public d(Context context, Intent intent) {
            this.f232a = context;
            this.f233b = intent;
        }

        /* JADX INFO: Access modifiers changed from: private */
        /* renamed from: c */
        public /* synthetic */ void d(Context context, Intent intent, e.b.m mVar) {
            File file;
            boolean z;
            boolean z2 = false;
            try {
                file = new File(context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), "upgrade.bin");
                if (!file.exists() || (z = file.delete())) {
                    z = file.createNewFile();
                }
            } catch (Exception unused) {
            }
            if (z) {
                InputStream openInputStream = ControlFragment.this.requireContext().getContentResolver().openInputStream(intent.getData());
                if (openInputStream != null) {
                    FileOutputStream fileOutputStream = new FileOutputStream(file);
                    while (true) {
                        int read = openInputStream.read();
                        if (read == -1) {
                            break;
                        }
                        fileOutputStream.write(read);
                    }
                    openInputStream.close();
                    fileOutputStream.close();
                    ControlFragment.this.f228f = file.getAbsolutePath();
                }
                mVar.onNext(Boolean.valueOf(z2));
                mVar.onComplete();
            }
            z2 = z;
            mVar.onNext(Boolean.valueOf(z2));
            mVar.onComplete();
        }

        /* JADX INFO: Access modifiers changed from: private */
        /* renamed from: e */
        public /* synthetic */ void f(Boolean bool) {
            if (bool.booleanValue()) {
                ControlFragment.this.f227e.run();
            } else {
                ControlFragment.this.f227e.i(R.string.upgrade_fail);
            }
        }

        @Override // d.h.a.e.e.c.AbstractC0014c
        public void a(View view) {
            final Context context = this.f232a;
            final Intent intent = this.f233b;
            e.b.l.b(new e.b.n() { // from class: d.h.a.e.a.a
                @Override // e.b.n
                public final void a(m mVar) {
                    ControlFragment.d.this.d(context, intent, mVar);
                }
            }).m(e.b.e0.a.b()).g(e.b.u.c.a.a()).i(new e.b.y.g() { // from class: d.h.a.e.a.b
                @Override // e.b.y.g
                public final void accept(Object obj) {
                    ControlFragment.d.this.f((Boolean) obj);
                }
            });
        }

        @Override // d.h.a.e.e.c.AbstractC0014c
        public void b(View view) {
            ControlFragment.this.f227e.h();
        }
    }

    /* loaded from: classes.dex */
    public class e implements c.AbstractC0014c {
        public e() {
        }

        @Override // d.h.a.e.e.c.AbstractC0014c
        public void a(View view) {
            if (ConnectionManager.e().g()) {
                ControlFragment.this.j1(null);
            }
        }

        @Override // d.h.a.e.e.c.AbstractC0014c
        public void b(View view) {
        }
    }

    /* loaded from: classes.dex */
    public class f implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ Future f236a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ boolean f237b;

        /* renamed from: c  reason: collision with root package name */
        public final /* synthetic */ Handler f238c;

        /* loaded from: classes.dex */
        public class a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            public int f240a = 10;

            /* renamed from: b  reason: collision with root package name */
            public final /* synthetic */ AlertDialog f241b;

            public a(AlertDialog alertDialog) {
                this.f241b = alertDialog;
            }

            @Override // java.lang.Runnable
            public void run() {
                int i = this.f240a;
                if (i > 0) {
                    this.f241b.setMessage(ControlFragment.this.getString(R.string.device_reboot, Integer.valueOf(i)));
                    this.f240a--;
                    f.this.f238c.postDelayed(this, 1000L);
                    return;
                }
                this.f241b.dismiss();
            }
        }

        public f(Future future, boolean z, Handler handler) {
            this.f236a = future;
            this.f237b = z;
            this.f238c = handler;
        }

        @Override // java.lang.Runnable
        public void run() {
            if (!this.f236a.isSuccess()) {
                ControlFragment controlFragment = ControlFragment.this;
                Toast.makeText(controlFragment.B, controlFragment.getString(R.string.command_failed), 0).show();
            } else if (this.f237b) {
                AlertDialog create = new AlertDialog.Builder(ControlFragment.this.B).setCancelable(false).setTitle(R.string.tip).setMessage(ControlFragment.this.getString(R.string.device_reboot, 10)).create();
                a aVar = new a(create);
                create.show();
                aVar.run();
            } else {
                ControlFragment controlFragment2 = ControlFragment.this;
                Toast.makeText(controlFragment2.B, controlFragment2.getString(R.string.command_success), 0).show();
            }
        }
    }

    /* loaded from: classes.dex */
    public class g implements c.AbstractC0014c {
        public g() {
        }

        @Override // d.h.a.e.e.c.AbstractC0014c
        public void a(View view) {
            if (ConnectionManager.e().c(false)) {
                ControlFragment.this.z.show();
                ((TextView) ControlFragment.this.z.findViewById(R.id.tipMsg)).setText(R.string.deviceConnecting);
                ControlFragment.this.z.setCancelable(false);
            }
        }

        @Override // d.h.a.e.e.c.AbstractC0014c
        public void b(View view) {
        }
    }

    /* loaded from: classes.dex */
    public class h extends t {
        public h(TextView textView, List list, int i) {
            super(textView, list, i);
        }

        @Override // com.sermatec.sunmate.ui.control.ControlFragment.t, d.a.a.i.d
        public void a(int i, int i2, int i3, View view) {
            super.a(i, i2, i3, view);
            c();
        }

        @Override // com.sermatec.sunmate.ui.control.ControlFragment.t
        public void b(int i) {
            super.b(i);
            c();
        }

        public final void c() {
            int i = this.f263d;
            if (i == 1) {
                ControlFragment.this.tv_dianchi_protocol_parent.setVisibility(0);
                ControlFragment.this.tv_ah_parent.setVisibility(8);
            } else if (i == 2) {
                ControlFragment.this.tv_dianchi_protocol_parent.setVisibility(8);
                ControlFragment.this.tv_ah_parent.setVisibility(0);
            }
            ControlFragment.this.Z0(this.f263d);
        }
    }

    /* loaded from: classes.dex */
    public class i implements d.a.a.i.d {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ List f245a;

        public i(List list) {
            this.f245a = list;
        }

        @Override // d.a.a.i.d
        public void a(int i, int i2, int i3, View view) {
            ControlFragment.this.etServerIp.setText((CharSequence) this.f245a.get(i));
        }
    }

    /* loaded from: classes.dex */
    public class j extends BroadcastReceiver {
        public j() {
        }

        @Override // android.content.BroadcastReceiver
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (TextUtils.equals(action, "conn_ok")) {
                ConnectionManager.e().p();
                ControlFragment.this.d1();
                ControlFragment.this.A();
                ControlFragment.this.a1(R.string.deviceConnectedSuccess);
            } else if (TextUtils.equals(action, "conn_no")) {
                Toast.makeText(context, (int) R.string.deviceDisconnected, 1).show();
            } else if (TextUtils.equals(action, "conn_no_condition")) {
                ControlFragment.this.a1(R.string.connect_fail);
            } else if (TextUtils.equals(action, "conn_fail")) {
                ControlFragment.this.a1(R.string.connect_fault);
            }
        }
    }

    /* loaded from: classes.dex */
    public class k extends BroadcastReceiver {
        public k() {
        }

        @Override // android.content.BroadcastReceiver
        public void onReceive(Context context, Intent intent) {
            if (intent.getBooleanExtra("resultsUpdated", false)) {
                ControlFragment.this.b1((WifiManager) context.getApplicationContext().getSystemService("wifi"));
            }
        }
    }

    /* loaded from: classes.dex */
    public class l implements u {

        /* renamed from: a  reason: collision with root package name */
        public int f249a = 3;

        public l() {
        }

        @Override // com.sermatec.sunmate.ui.control.ControlFragment.u
        public void a(boolean z) {
            if (!z) {
                int i = this.f249a - 1;
                this.f249a = i;
                if (i > 0) {
                    ControlFragment.this.j1(this);
                }
            }
        }
    }

    /* loaded from: classes.dex */
    public class m implements Runnable {
        public m() {
        }

        @Override // java.lang.Runnable
        public void run() {
            ControlFragment.this.z.dismiss();
        }
    }

    /* loaded from: classes.dex */
    public class n extends RecyclerView.AdapterDataObserver {
        public n() {
        }

        @Override // androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
        public void onChanged() {
            if (ControlFragment.this.f223a.q().size() > 0) {
                ControlFragment.this.time_type_container_parent.setVisibility(0);
            } else {
                ControlFragment.this.time_type_container_parent.setVisibility(8);
            }
        }
    }

    /* loaded from: classes.dex */
    public class o implements TimePickerDialogWrap.d {
        public o() {
        }

        @Override // com.sermatec.sunmate.ui.control.TimePickerDialogWrap.d
        public void a(int i, int i2, int i3, int i4, int i5) {
            TimeType timeType = new TimeType((i2 * 60) + i3, (i4 * 60) + i5, i + 1);
            int V = ControlFragment.this.f223a.V(timeType.getStartTime(), timeType.getEndTime());
            if (V != 0) {
                ControlFragment.this.j(R.string.tip, V);
            } else {
                ControlFragment.this.f223a.f(timeType);
            }
        }
    }

    /* loaded from: classes.dex */
    public class p implements BaseQuickAdapter.f {

        /* loaded from: classes.dex */
        public class a implements TimePickerDialogWrap.d {

            /* renamed from: a  reason: collision with root package name */
            public final /* synthetic */ int f255a;

            /* renamed from: b  reason: collision with root package name */
            public final /* synthetic */ TimeType f256b;

            public a(int i, TimeType timeType) {
                this.f255a = i;
                this.f256b = timeType;
            }

            @Override // com.sermatec.sunmate.ui.control.TimePickerDialogWrap.d
            public void a(int i, int i2, int i3, int i4, int i5) {
                ControlFragment.this.f223a.O(this.f255a);
                TimeType timeType = new TimeType((i2 * 60) + i3, (i4 * 60) + i5, i + 1);
                int V = ControlFragment.this.f223a.V(timeType.getStartTime(), timeType.getEndTime());
                if (V != 0) {
                    ControlFragment.this.j(R.string.tip, V);
                    ControlFragment.this.f223a.f(this.f256b);
                    return;
                }
                ControlFragment.this.f223a.f(timeType);
            }
        }

        public p() {
        }

        @Override // com.chad.library.adapter.base.BaseQuickAdapter.f
        public void a(BaseQuickAdapter baseQuickAdapter, View view, int i) {
            TimeType timeType = (TimeType) baseQuickAdapter.getItem(i);
            if (timeType != null) {
                new TimePickerDialogWrap(new a(i, timeType), timeType.getElectricityTypeValue() - 1, timeType.getStartTime() / 60, timeType.getStartTime() % 60, timeType.getEndTime() / 60, timeType.getEndTime() % 60).show(ControlFragment.this.getChildFragmentManager(), "timePicker");
            }
        }
    }

    /* loaded from: classes.dex */
    public class q implements AdapterView.OnItemClickListener {
        public q() {
        }

        @Override // android.widget.AdapterView.OnItemClickListener
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            ControlFragment controlFragment = ControlFragment.this;
            controlFragment.routerssid.setText((CharSequence) controlFragment.C.get(i));
            ControlFragment.this.D.dismiss();
        }
    }

    /* loaded from: classes.dex */
    public class r implements PopupWindow.OnDismissListener {
        public r() {
        }

        @Override // android.widget.PopupWindow.OnDismissListener
        public void onDismiss() {
            ControlFragment controlFragment = ControlFragment.this;
            controlFragment.routerssid.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, controlFragment.N, (Drawable) null);
        }
    }

    /* loaded from: classes.dex */
    public interface s {
        ChannelFuture a();
    }

    /* loaded from: classes.dex */
    public interface u {
        void a(boolean z);
    }

    /* loaded from: classes.dex */
    public static abstract class v implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        public TextView f264a;

        /* renamed from: b  reason: collision with root package name */
        public int f265b;

        /* renamed from: c  reason: collision with root package name */
        public Context f266c;

        public v(TextView textView, int i, Context context) {
            this.f264a = textView;
            this.f265b = i;
            this.f266c = context;
        }

        /* JADX INFO: Access modifiers changed from: private */
        /* renamed from: b */
        public /* synthetic */ void c(Integer num) {
            this.f264a.setText(this.f266c.getString(R.string.upgrading, num));
        }

        /* JADX INFO: Access modifiers changed from: private */
        /* renamed from: d */
        public /* synthetic */ void e(Throwable th) {
            i(R.string.upgrade_fail);
        }

        public void f() {
            this.f264a.setClickable(false);
            this.f264a.setBackgroundColor(this.f266c.getResources().getColor(R.color.colorBorder));
        }

        public void g(e.b.l<Integer> lVar) {
            lVar.g(e.b.u.c.a.a()).j(new e.b.y.g() { // from class: d.h.a.e.a.w
                @Override // e.b.y.g
                public final void accept(Object obj) {
                    ControlFragment.v.this.c((Integer) obj);
                }
            }, new e.b.y.g() { // from class: d.h.a.e.a.x
                @Override // e.b.y.g
                public final void accept(Object obj) {
                    ControlFragment.v.this.e((Throwable) obj);
                }
            }, new e.b.y.a() { // from class: d.h.a.e.a.p2
                @Override // e.b.y.a
                public final void run() {
                    ControlFragment.v.this.j();
                }
            });
        }

        public void h() {
            Drawable drawable = this.f266c.getResources().getDrawable(R.drawable.button_bg);
            this.f264a.setText(R.string.modeUpgradeSectionTitle);
            this.f264a.setBackgroundDrawable(drawable);
            this.f264a.setClickable(true);
        }

        public void i(int i) {
            Drawable drawable = this.f266c.getResources().getDrawable(R.drawable.button_bg);
            this.f264a.setText(R.string.modeUpgradeSectionTitle);
            this.f264a.setBackgroundDrawable(drawable);
            this.f264a.setClickable(true);
            d.h.a.e.e.c.c(this.f266c, this.f265b, i).show();
        }

        public void j() {
            Drawable drawable = this.f266c.getResources().getDrawable(R.drawable.button_bg);
            this.f264a.setText(R.string.modeUpgradeSectionTitle);
            this.f264a.setBackgroundDrawable(drawable);
            this.f264a.setClickable(true);
            d.h.a.e.e.c.c(this.f266c, this.f265b, R.string.upgrade_success).show();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: B0 */
    public /* synthetic */ void C0(View view) {
        if (!ConnectionManager.e().g()) {
            p();
        } else {
            g1(t());
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: D0 */
    public /* synthetic */ void E0(View view) {
        this.q.u();
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: F */
    public /* synthetic */ void G() {
        Toast.makeText(this.B, getString(R.string.command_success), 0).show();
        q2 q2Var = this.F;
        q2Var.f863c = null;
        q2Var.f861a = null;
        q2Var.f862b = null;
        this.P = false;
        if (!j1(null)) {
            this.f225c.i();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: F0 */
    public /* synthetic */ void G0(View view) {
        this.i.u();
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: H */
    public /* synthetic */ void I() {
        Toast.makeText(this.B, getString(R.string.command_failed), 0).show();
        this.P = false;
        this.f225c.i();
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: H0 */
    public /* synthetic */ void I0(View view) {
        this.j.u();
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: J */
    public /* synthetic */ void K(final List list, Future future) {
        if (!future.isSuccess()) {
            this.A.post(new Runnable() { // from class: d.h.a.e.a.c0
                @Override // java.lang.Runnable
                public final void run() {
                    ControlFragment.this.I();
                }
            });
        } else if (list.size() > 1) {
            list.remove(0);
            this.A.postDelayed(new Runnable() { // from class: d.h.a.e.a.d
                @Override // java.lang.Runnable
                public final void run() {
                    ControlFragment.this.E(list);
                }
            }, 1000L);
        } else {
            this.A.postDelayed(new Runnable() { // from class: d.h.a.e.a.e
                @Override // java.lang.Runnable
                public final void run() {
                    ControlFragment.this.G();
                }
            }, 5000L);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: J0 */
    public /* synthetic */ void K0(View view) {
        this.k.u();
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: L */
    public /* synthetic */ ChannelFuture M(Object obj) {
        return this.F.o(null, u(obj), null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: L0 */
    public /* synthetic */ void M0(View view) {
        this.l.u();
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: N */
    public /* synthetic */ ChannelFuture O(Object obj) {
        return this.F.n(null, null, null, null, null, u(obj), null, null, null, null, null, null, null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: N0 */
    public /* synthetic */ void O0(boolean z) {
        C();
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: P */
    public /* synthetic */ ChannelFuture Q(Object obj, JSONArray jSONArray, String str, String str2, String str3, String str4) {
        return this.F.n(u(obj), null, null, jSONArray.toJSONString(), null, null, null, null, null, str, str2, str3, str4);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: P0 */
    public /* synthetic */ void Q0(boolean z, Future future) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new f(future, z, handler));
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: R */
    public /* synthetic */ ChannelFuture S(String str, String str2, String str3) {
        return this.F.n(null, str, str2, null, str3, null, null, null, null, null, null, null, null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: R0 */
    public /* synthetic */ void S0(u uVar, Long l2) {
        this.J.dispose();
        this.f225c.i();
        if (uVar != null) {
            uVar.a(false);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: T */
    public /* synthetic */ ChannelFuture U(Object obj, String str) {
        return this.F.o(u(obj), null, str);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: T0 */
    public /* synthetic */ void U0(e.b.v.b bVar, u uVar, Boolean bool) {
        bVar.dispose();
        this.J.dispose();
        this.onOff.check(bool.booleanValue() ? R.id.radio_open : R.id.radio_close);
        this.f225c.i();
        if (uVar != null) {
            uVar.a(true);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: V */
    public /* synthetic */ ChannelFuture W(Object obj, Object obj2, String str, Object obj3, Object obj4) {
        return this.F.f(u(obj), u(obj2), str, u(obj3), u(obj4));
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: V0 */
    public /* synthetic */ void W0(u uVar, Long l2) {
        this.f225c.i();
        if (uVar != null) {
            uVar.a(false);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: X */
    public /* synthetic */ void Y(View view) {
        this.g.u();
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: X0 */
    public /* synthetic */ void Y0(u uVar, WorkParamMode workParamMode) {
        this.I.dispose();
        this.F.f861a = workParamMode;
        this.h.b(workParamMode.getStyle());
        this.w.b(workParamMode.getRefluxs());
        this.mode_grid_power.setText(workParamMode.getCon());
        this.on_grid_soc_limit.setText(workParamMode.getSoc());
        this.mode_price_top.setText(workParamMode.getPrice1());
        this.mode_price_flat.setText(workParamMode.getPrice3());
        this.mode_price_peak.setText(workParamMode.getPrice2());
        this.mode_price_valley.setText(workParamMode.getPrice4());
        this.s.b(workParamMode.getElecCode());
        this.t.b(workParamMode.getBatteryProtocol());
        this.u.b(workParamMode.getMeterProtocol());
        this.v.b(workParamMode.getDcBatteryType());
        this.etAh.setText(workParamMode.getAh());
        this.f223a.q().clear();
        this.f223a.g(workParamMode.getTimeType());
        k1(uVar);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: Z */
    public /* synthetic */ void a0(View view) {
        this.o.u();
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: b0 */
    public /* synthetic */ void c0(View view) {
        this.p.u();
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: d0 */
    public /* synthetic */ void e0(View view) {
        this.n.u();
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: f0 */
    public /* synthetic */ void g0(View view) {
        this.f224b.show(getChildFragmentManager(), "timePicker");
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: h0 */
    public /* synthetic */ void i0(View view) {
        this.m.u();
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: j0 */
    public /* synthetic */ void k0(View view) {
        if (!ConnectionManager.e().g()) {
            p();
        } else {
            c1(this.F.e(this.mRadioOpen.isChecked() ? "1" : "0"), false);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: l0 */
    public /* synthetic */ void m0(View view) {
        if (!ConnectionManager.e().g()) {
            p();
        } else {
            g1(w());
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: n0 */
    public /* synthetic */ void o0(View view) {
        if (!ConnectionManager.e().g()) {
            p();
            return;
        }
        String obj = this.new_wifi_password.getText().toString();
        String obj2 = this.ensure_new_wifi_password.getText().toString();
        if (obj.length() < 8 || obj.length() >= 16) {
            j(R.string.tip, R.string.wifiTip);
        } else if (obj.equals(obj2)) {
            c1(this.F.b(obj), true);
        } else {
            j(R.string.tip, R.string.password_validate_fail);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: p0 */
    public /* synthetic */ void q0(View view) {
        if (!ConnectionManager.e().g()) {
            p();
            return;
        }
        Object tag = this.tvRouterMode.getTag();
        String obj = this.etRouterPwd.getText().toString();
        String charSequence = this.routerssid.getText().toString();
        if (o(new Object[]{tag, obj, charSequence})) {
            c1(this.F.i(obj, charSequence, String.valueOf(tag)), true);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: r0 */
    public /* synthetic */ void s0(View view) {
        if (!ConnectionManager.e().g()) {
            p();
            return;
        }
        String charSequence = this.etServerIp.getText().toString();
        String obj = this.etServerPort.getText().toString();
        if (o(new Object[]{charSequence, obj})) {
            c1(this.F.k(charSequence, obj), true);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: t0 */
    public /* synthetic */ void u0(View view) {
        if (!ConnectionManager.e().g()) {
            p();
            return;
        }
        b bVar = new b(this.updatePucOk, R.string.upgradePUC, requireContext());
        this.f227e = bVar;
        bVar.f();
        l1();
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: v0 */
    public /* synthetic */ void w0(View view) {
        if (!ConnectionManager.e().g()) {
            p();
            return;
        }
        c cVar = new c(this.updateDspOk, R.string.upgradeDSP, requireContext());
        this.f227e = cVar;
        cVar.f();
        l1();
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: x0 */
    public /* synthetic */ void y0(View view) {
        if (!ConnectionManager.e().g()) {
            p();
        } else {
            c1(this.F.h(), true);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: z0 */
    public /* synthetic */ void A0(View view) {
        if (!ConnectionManager.e().g()) {
            p();
            return;
        }
        Integer num = (Integer) this.mode_type.getTag();
        int i2 = 0;
        if (num != null && num.intValue() == 4) {
            i2 = this.f223a.U();
        }
        if (i2 == 0) {
            g1(v());
        } else {
            j(R.string.tip, i2);
        }
    }

    public void A() {
        i1(new u() { // from class: d.h.a.e.a.z
            @Override // com.sermatec.sunmate.ui.control.ControlFragment.u
            public final void a(boolean z) {
                ControlFragment.this.O0(z);
            }
        });
    }

    public void B() {
        this.f225c = KProgressHUD.h(this.B);
        this.N = this.B.getResources().getDrawable(R.drawable.ic_down);
        this.M = this.B.getResources().getDrawable(R.drawable.ic_up);
        this.routerssid.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, this.N, (Drawable) null);
        this.z = d.h.a.e.e.c.c(this.B, R.string.conn_device, R.string.deviceConnecting);
        z();
        this.time_type_container.setLayoutManager(new LinearLayoutManager(this.B));
        TimeTypeAdapter timeTypeAdapter = new TimeTypeAdapter(new ArrayList());
        this.f223a = timeTypeAdapter;
        timeTypeAdapter.registerAdapterDataObserver(new n());
        this.time_type_container.setAdapter(this.f223a);
        this.f224b = new TimePickerDialogWrap(new o());
        this.f223a.R(new p());
    }

    public void C() {
        if (ConnectionManager.e().g()) {
            this.f225c.l();
            if (!j1(new l())) {
                this.f225c.i();
            }
        }
    }

    public void Z0(int i2) {
    }

    public final void a1(@StringRes int i2) {
        TextView textView;
        Dialog dialog = this.z;
        if (dialog != null && (textView = (TextView) dialog.findViewById(R.id.tipMsg)) != null) {
            textView.setText(i2);
            this.z.setCancelable(true);
            this.A.postDelayed(new m(), 1000L);
        }
    }

    public final void b1(WifiManager wifiManager) {
        List<ScanResult> scanResults = wifiManager.getScanResults();
        if (scanResults != null) {
            this.C.clear();
            LinkedHashSet linkedHashSet = new LinkedHashSet();
            for (int i2 = 0; i2 < scanResults.size(); i2++) {
                String str = scanResults.get(i2).SSID;
                if (str != null && !str.isEmpty()) {
                    linkedHashSet.add(str);
                }
            }
            this.C.addAll(linkedHashSet);
            if (this.E != null) {
                this.E.notifyDataSetChanged();
            }
        }
    }

    public void c1(ChannelFuture channelFuture, final boolean z) {
        if (channelFuture == null) {
            Toast.makeText(this.B, getString(R.string.command_failed), 0).show();
        } else {
            channelFuture.addListener(new GenericFutureListener() { // from class: d.h.a.e.a.m
                @Override // io.netty.util.concurrent.GenericFutureListener
                public final void operationComplete(Future future) {
                    ControlFragment.this.Q0(z, future);
                }
            });
        }
    }

    public void d1() {
        if (ConnectionManager.e().g()) {
            this.f226d = ConnectionManager.e().f().getVersion().c();
        }
        if (this.f226d >= 259) {
            this.meterMonitor_parent.setVisibility(0);
            this.threePhase_parent.setVisibility(0);
            this.offGridSoc_parent.setVisibility(0);
        }
    }

    public void e1() {
        d.h.a.e.e.c.a(this.B, R.string.param_not_prepared, new e()).show();
    }

    public final void f1() {
        WifiManager wifiManager = (WifiManager) requireContext().getApplicationContext().getSystemService("wifi");
        b1(wifiManager);
        String str = "-------------------" + wifiManager.startScan();
    }

    public void g1(@NonNull List<s> list) {
        q2 q2Var = this.F;
        if (q2Var.f861a == null || q2Var.f862b == null) {
            e1();
        } else if (this.P) {
            Toast.makeText(this.B, (int) R.string.exec_command_rapidly, 0).show();
        } else {
            this.P = true;
            this.f225c.l();
            D(list);
        }
    }

    public final void h1() {
        this.D = new ListPopupWindow(this.B);
        try {
            Field declaredField = this.D.getClass().getDeclaredField("mPopup");
            declaredField.setAccessible(true);
            PopupWindow popupWindow = (PopupWindow) declaredField.get(this.D);
            if (Build.VERSION.SDK_INT >= 23) {
                popupWindow.setExitTransition(null);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        this.E = new ArrayAdapter<>(this.B, (int) R.layout.popupview, this.C);
        this.D.setAdapter(this.E);
        this.D.setAnchorView(this.routerssid);
        this.D.setModal(false);
        if (this.E.getCount() > 4) {
            this.D.setHeight(500);
        }
        this.D.setOnItemClickListener(new q());
        this.D.setOnDismissListener(new r());
        if (!(!this.O.isActive() || ((MainActivity) this.B).getCurrentFocus() == null || ((MainActivity) this.B).getCurrentFocus().getWindowToken() == null)) {
            this.O.hideSoftInputFromWindow(((MainActivity) this.B).getCurrentFocus().getWindowToken(), 2);
        }
        this.D.show();
    }

    public void i1(final u uVar) {
        if (ConnectionManager.e().g()) {
            this.f225c.l();
            final e.b.v.b i2 = e.b.l.p(3L, TimeUnit.SECONDS).g(e.b.u.c.a.a()).i(new e.b.y.g() { // from class: d.h.a.e.a.o0
                @Override // e.b.y.g
                public final void accept(Object obj) {
                    ControlFragment.this.S0(uVar, (Long) obj);
                }
            });
            this.J = d.h.a.d.i.f608b.g(e.b.u.c.a.a()).i(new e.b.y.g() { // from class: d.h.a.e.a.h0
                @Override // e.b.y.g
                public final void accept(Object obj) {
                    ControlFragment.this.U0(i2, uVar, (Boolean) obj);
                }
            });
            this.F.g("0C");
        }
    }

    public void j(@StringRes int i2, @StringRes int i3) {
        d.h.a.e.e.c.c(requireContext(), i2, i3).show();
    }

    @SuppressLint({"CheckResult"})
    public boolean j1(final u uVar) {
        if (!ConnectionManager.e().g()) {
            return false;
        }
        this.f225c.l();
        e.b.l<Long> g2 = e.b.l.p(3L, TimeUnit.SECONDS).g(e.b.u.c.a.a());
        this.L = g2;
        this.K = g2.i(new e.b.y.g() { // from class: d.h.a.e.a.f0
            @Override // e.b.y.g
            public final void accept(Object obj) {
                ControlFragment.this.W0(uVar, (Long) obj);
            }
        });
        this.I = d.h.a.d.i.f607a.g(e.b.u.c.a.a()).i(new e.b.y.g() { // from class: d.h.a.e.a.f
            @Override // e.b.y.g
            public final void accept(Object obj) {
                ControlFragment.this.Y0(uVar, (WorkParamMode) obj);
            }
        });
        this.F.g("95");
        return true;
    }

    public d.a.a.k.b<String> k(List<String> list, @StringRes int i2, t tVar) {
        return q(list, i2, tVar);
    }

    public void k1(u uVar) {
        q2 q2Var = this.F;
        if (q2Var.f861a != null || q2Var.f862b != null) {
            this.K.dispose();
            if (uVar != null) {
                uVar.a(true);
            }
            this.f225c.i();
        }
    }

    public d.h.a.d.g<d.a.a.k.b<String>, t> l(@ArrayRes int i2, @StringRes int i3, TextView textView) {
        return n(new ArrayList(Arrays.asList(getResources().getStringArray(i2))), i3, textView, 1);
    }

    public final void l1() {
        m1();
    }

    public d.h.a.d.g<d.a.a.k.b<String>, t> m(@ArrayRes int i2, @StringRes int i3, TextView textView, int i4) {
        return n(new ArrayList(Arrays.asList(getResources().getStringArray(i2))), i3, textView, i4);
    }

    public final void m1() {
        Intent intent = new Intent("android.intent.action.GET_CONTENT");
        intent.setType("*/*");
        intent.addCategory("android.intent.category.OPENABLE");
        try {
            startActivityForResult(Intent.createChooser(intent, getString(R.string.select_upgrade_file)), 2);
        } catch (ActivityNotFoundException unused) {
            this.f227e.run();
        }
    }

    public final d.h.a.d.g<d.a.a.k.b<String>, t> n(List<String> list, @StringRes int i2, TextView textView, int i3) {
        t tVar = new t(textView, list, i3);
        return new d.h.a.d.g<>(q(list, i2, tVar), tVar);
    }

    public boolean o(Object[] objArr) {
        for (int i2 = 0; i2 < objArr.length; i2++) {
            if (objArr[i2] instanceof String) {
                String str = (String) objArr[i2];
                if (str == null || str.isEmpty()) {
                    j(R.string.tip, R.string.param_loss);
                    return false;
                }
            } else if (objArr[i2] == null) {
                j(R.string.tip, R.string.param_loss);
                return false;
            }
        }
        return true;
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, @Nullable Intent intent) {
        Uri data;
        if (i2 != 2) {
            return;
        }
        if (i3 != -1 || intent == null || (data = intent.getData()) == null) {
            this.f227e.h();
            return;
        }
        Context requireContext = requireContext();
        String scheme = data.getScheme();
        String str = null;
        if (HttpPostBodyUtil.FILE.equalsIgnoreCase(scheme)) {
            str = data.getLastPathSegment();
        } else if ("content".equalsIgnoreCase(scheme)) {
            Cursor query = requireContext.getContentResolver().query(data, new String[]{"_display_name"}, null, null, null);
            if (!(query == null || query.getCount() == 0)) {
                int columnIndexOrThrow = query.getColumnIndexOrThrow("_display_name");
                query.moveToFirst();
                str = query.getString(columnIndexOrThrow);
            }
            if (query != null) {
                query.close();
            }
        }
        if (str != null) {
            d.h.a.e.e.c.b(requireContext(), requireContext.getString(R.string.upgrade_file_ensure, str, requireContext.getString(this.f227e.f265b)), new d(requireContext, intent)).show();
        } else {
            this.f227e.h();
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onAttach(Context context) {
        super.onAttach(context);
        this.B = context;
    }

    @Override // androidx.fragment.app.Fragment
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
        ConnectionManager.e().p();
        x();
        if (Build.VERSION.SDK_INT < 23 || ContextCompat.checkSelfPermission(requireContext(), "android.permission.ACCESS_COARSE_LOCATION") == 0) {
            f1();
            return;
        }
        if (ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(), "android.permission.ACCESS_COARSE_LOCATION")) {
            Toast.makeText(requireContext(), (int) R.string.askForLocation, 0).show();
        }
        requestPermissions(new String[]{"android.permission.ACCESS_COARSE_LOCATION", "android.permission.ACCESS_FINE_LOCATION", "android.permission.CHANGE_WIFI_STATE", "android.permission.ACCESS_WIFI_STATE"}, 3);
    }

    @Override // androidx.fragment.app.Fragment
    @Nullable
    public View onCreateView(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, @Nullable Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.fragment_control, viewGroup, false);
        ButterKnife.a(this, inflate);
        B();
        y();
        d1();
        A();
        return inflate;
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        getActivity().unregisterReceiver(this.G);
        getActivity().unregisterReceiver(this.H);
        ConnectionManager.e().o();
        if (this.D != null) {
            this.D.dismiss();
        }
        super.onDestroy();
    }

    @Override // androidx.fragment.app.Fragment
    public void onDetach() {
        this.B = null;
        super.onDetach();
    }

    @Override // androidx.fragment.app.Fragment
    public void onRequestPermissionsResult(int i2, @NonNull String[] strArr, @NonNull int[] iArr) {
        super.onRequestPermissionsResult(i2, strArr, iArr);
        if (i2 == 1) {
            if (iArr.length <= 0 || iArr[0] != 0) {
                this.f227e.run();
            } else {
                m1();
            }
        } else if (i2 == 3 && iArr.length > 0 && iArr[0] == 0) {
            f1();
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onStart() {
        super.onStart();
        ConnectionManager.e().p();
    }

    public void p() {
        d.h.a.e.e.c.a(requireContext(), R.string.conn_device, new g()).show();
    }

    public final d.a.a.k.b<String> q(List<String> list, @StringRes int i2, d.a.a.i.d dVar) {
        d.a.a.k.b<String> a2 = new d.a.a.g.a(this.B, dVar).m(getString(i2)).i(getResources().getColor(R.color.colorDarker)).e(getResources().getColor(R.color.colorDarker)).l(getResources().getColor(R.color.colorDarker)).k(getResources().getColor(R.color.colorBorder)).d(getResources().getColor(R.color.background)).j(getResources().getColor(R.color.colorDarker)).f(22).b(false).g(false, false, false).h(true).c(false).a();
        a2.z(list);
        return a2;
    }

    /* renamed from: r */
    public void D(@NonNull final List<s> list) {
        if (list.size() > 0) {
            ChannelFuture a2 = list.get(0).a();
            if (a2 == null) {
                Toast.makeText(this.B, getString(R.string.command_failed), 0).show();
                this.P = false;
                this.f225c.i();
                return;
            }
            a2.addListener(new GenericFutureListener() { // from class: d.h.a.e.a.h
                @Override // io.netty.util.concurrent.GenericFutureListener
                public final void operationComplete(Future future) {
                    ControlFragment.this.K(list, future);
                }
            });
            return;
        }
        this.P = false;
        this.f225c.i();
    }

    public final SparseIntArray s() {
        SparseIntArray sparseIntArray = new SparseIntArray();
        sparseIntArray.put(0, 1);
        sparseIntArray.put(1, 2);
        sparseIntArray.put(2, 3);
        sparseIntArray.put(3, 9);
        sparseIntArray.put(4, 12);
        sparseIntArray.put(5, 13);
        sparseIntArray.put(6, 14);
        sparseIntArray.put(7, 22);
        sparseIntArray.put(8, 23);
        sparseIntArray.put(9, 25);
        sparseIntArray.put(10, 26);
        sparseIntArray.put(11, 27);
        return sparseIntArray;
    }

    public List<s> t() {
        ArrayList arrayList = new ArrayList();
        if (ConnectionManager.e().f().getVersion().c() >= 259) {
            final Object tag = this.three_phrase_out.getTag();
            arrayList.add(new s() { // from class: d.h.a.e.a.l0
                @Override // com.sermatec.sunmate.ui.control.ControlFragment.s
                public final ChannelFuture a() {
                    return ControlFragment.this.M(tag);
                }
            });
        }
        final Object tag2 = this.influx.getTag();
        arrayList.add(new s() { // from class: d.h.a.e.a.k0
            @Override // com.sermatec.sunmate.ui.control.ControlFragment.s
            public final ChannelFuture a() {
                return ControlFragment.this.O(tag2);
            }
        });
        return arrayList;
    }

    public String u(Object obj) {
        if (obj instanceof String) {
            String trim = ((String) obj).trim();
            if (trim.isEmpty()) {
                return null;
            }
            return trim;
        } else if (obj == null) {
            return null;
        } else {
            return String.valueOf(obj);
        }
    }

    public List<s> v() {
        ArrayList arrayList = new ArrayList();
        final JSONArray jSONArray = new JSONArray();
        List<TimeType> q2 = this.f223a.q();
        for (int i2 = 0; i2 < q2.size(); i2++) {
            jSONArray.add(q2.get(i2).toJSONObject());
        }
        final Object tag = this.mode_type.getTag();
        final String obj = this.mode_price_top.getText().toString();
        final String obj2 = this.mode_price_flat.getText().toString();
        final String obj3 = this.mode_price_peak.getText().toString();
        final String obj4 = this.mode_price_valley.getText().toString();
        arrayList.add(new s() { // from class: d.h.a.e.a.j0
            @Override // com.sermatec.sunmate.ui.control.ControlFragment.s
            public final ChannelFuture a() {
                return ControlFragment.this.Q(tag, jSONArray, obj, obj3, obj2, obj4);
            }
        });
        return arrayList;
    }

    public List<s> w() {
        ArrayList arrayList = new ArrayList();
        final String obj = this.mode_grid_power.getText().toString();
        final String obj2 = this.on_grid_soc_limit.getText().toString();
        arrayList.add(new s() { // from class: d.h.a.e.a.g0
            @Override // com.sermatec.sunmate.ui.control.ControlFragment.s
            public final ChannelFuture a() {
                return ControlFragment.this.S(r2, obj, obj2);
            }
        });
        if (ConnectionManager.e().f().getVersion().c() >= 259) {
            final Object tag = this.meter_check.getTag();
            final String u2 = u(this.off_grid_soc_limit.getText().toString());
            arrayList.add(new s() { // from class: d.h.a.e.a.m0
                @Override // com.sermatec.sunmate.ui.control.ControlFragment.s
                public final ChannelFuture a() {
                    return ControlFragment.this.U(tag, u2);
                }
            });
        }
        final Object tag2 = this.tvDianWang.getTag();
        final Object tag3 = this.tvDianChiType.getTag();
        final String obj3 = this.etAh.getText().toString();
        final Object tag4 = this.tvDianChi.getTag();
        final Object tag5 = this.tvDianBiao.getTag();
        arrayList.add(new s() { // from class: d.h.a.e.a.c
            @Override // com.sermatec.sunmate.ui.control.ControlFragment.s
            public final ChannelFuture a() {
                return ControlFragment.this.W(tag2, tag3, obj3, tag4, tag5);
            }
        });
        return arrayList;
    }

    public final void x() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("conn_ok");
        intentFilter.addAction("conn_no");
        intentFilter.addAction("conn_fail");
        intentFilter.addAction("conn_no_condition");
        requireActivity().registerReceiver(this.G, intentFilter);
        IntentFilter intentFilter2 = new IntentFilter();
        intentFilter2.addAction("android.net.wifi.SCAN_RESULTS");
        requireActivity().registerReceiver(this.H, intentFilter2);
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public void y() {
        this.O = (InputMethodManager) this.B.getSystemService("input_method");
        this.routerssid.setOnTouchListener(new a());
        d.h.a.e.e.b.b(this.etServerIp, new b.a() { // from class: d.h.a.e.a.a0
            @Override // d.h.a.e.e.b.a
            public final void a(View view) {
                ControlFragment.this.E0(view);
            }
        });
        d.h.a.e.e.b.b(this.tvRouterMode, new b.a() { // from class: d.h.a.e.a.r
            @Override // d.h.a.e.e.b.a
            public final void a(View view) {
                ControlFragment.this.G0(view);
            }
        });
        d.h.a.e.e.b.b(this.tvDianWang, new b.a() { // from class: d.h.a.e.a.y
            @Override // d.h.a.e.e.b.a
            public final void a(View view) {
                ControlFragment.this.I0(view);
            }
        });
        d.h.a.e.e.b.b(this.tvDianChi, new b.a() { // from class: d.h.a.e.a.n0
            @Override // d.h.a.e.e.b.a
            public final void a(View view) {
                ControlFragment.this.K0(view);
            }
        });
        d.h.a.e.e.b.b(this.tvDianBiao, new b.a() { // from class: d.h.a.e.a.i0
            @Override // d.h.a.e.e.b.a
            public final void a(View view) {
                ControlFragment.this.M0(view);
            }
        });
        d.h.a.e.e.b.b(this.mode_type, new b.a() { // from class: d.h.a.e.a.u
            @Override // d.h.a.e.e.b.a
            public final void a(View view) {
                ControlFragment.this.Y(view);
            }
        });
        d.h.a.e.e.b.b(this.influx, new b.a() { // from class: d.h.a.e.a.v
            @Override // d.h.a.e.e.b.a
            public final void a(View view) {
                ControlFragment.this.a0(view);
            }
        });
        d.h.a.e.e.b.b(this.meter_check, new b.a() { // from class: d.h.a.e.a.o
            @Override // d.h.a.e.e.b.a
            public final void a(View view) {
                ControlFragment.this.c0(view);
            }
        });
        d.h.a.e.e.b.b(this.three_phrase_out, new b.a() { // from class: d.h.a.e.a.j
            @Override // d.h.a.e.e.b.a
            public final void a(View view) {
                ControlFragment.this.e0(view);
            }
        });
        d.h.a.e.e.b.b(this.add_price_time, new b.a() { // from class: d.h.a.e.a.d0
            @Override // d.h.a.e.e.b.a
            public final void a(View view) {
                ControlFragment.this.g0(view);
            }
        });
        d.h.a.e.e.b.b(this.tvDianChiType, new b.a() { // from class: d.h.a.e.a.t
            @Override // d.h.a.e.e.b.a
            public final void a(View view) {
                ControlFragment.this.i0(view);
            }
        });
        d.h.a.e.e.b.b(this.onOffOk, new b.a() { // from class: d.h.a.e.a.p
            @Override // d.h.a.e.e.b.a
            public final void a(View view) {
                ControlFragment.this.k0(view);
            }
        });
        d.h.a.e.e.b.b(this.workModeOk, new b.a() { // from class: d.h.a.e.a.k
            @Override // d.h.a.e.e.b.a
            public final void a(View view) {
                ControlFragment.this.m0(view);
            }
        });
        d.h.a.e.e.b.b(this.changWifiPwdOk, new b.a() { // from class: d.h.a.e.a.e0
            @Override // d.h.a.e.e.b.a
            public final void a(View view) {
                ControlFragment.this.o0(view);
            }
        });
        d.h.a.e.e.b.b(this.routerOk, new b.a() { // from class: d.h.a.e.a.q
            @Override // d.h.a.e.e.b.a
            public final void a(View view) {
                ControlFragment.this.q0(view);
            }
        });
        d.h.a.e.e.b.b(this.serverOk, new b.a() { // from class: d.h.a.e.a.s
            @Override // d.h.a.e.e.b.a
            public final void a(View view) {
                ControlFragment.this.s0(view);
            }
        });
        d.h.a.e.e.b.b(this.updatePucOk, new b.a() { // from class: d.h.a.e.a.n
            @Override // d.h.a.e.e.b.a
            public final void a(View view) {
                ControlFragment.this.u0(view);
            }
        });
        d.h.a.e.e.b.b(this.updateDspOk, new b.a() { // from class: d.h.a.e.a.l
            @Override // d.h.a.e.e.b.a
            public final void a(View view) {
                ControlFragment.this.w0(view);
            }
        });
        d.h.a.e.e.b.b(this.resetOk, new b.a() { // from class: d.h.a.e.a.g
            @Override // d.h.a.e.e.b.a
            public final void a(View view) {
                ControlFragment.this.y0(view);
            }
        });
        d.h.a.e.e.b.b(this.btn_work_mode_sure, new b.a() { // from class: d.h.a.e.a.b0
            @Override // d.h.a.e.e.b.a
            public final void a(View view) {
                ControlFragment.this.A0(view);
            }
        });
        d.h.a.e.e.b.b(this.btn_work_mode2, new b.a() { // from class: d.h.a.e.a.i
            @Override // d.h.a.e.e.b.a
            public final void a(View view) {
                ControlFragment.this.C0(view);
            }
        });
    }

    public final void z() {
        d.h.a.d.g<d.a.a.k.b<String>, t> m2 = m(R.array.router_work_mode, R.string.modeRouterSettingSectionTitle, this.tvRouterMode, 0);
        this.i = m2.a();
        this.r = m2.b();
        d.h.a.d.g<d.a.a.k.b<String>, t> l2 = l(R.array.country, R.string.console_controlOrder_param_powerGridCode, this.tvDianWang);
        this.j = l2.a();
        this.s = l2.b();
        ArrayList arrayList = new ArrayList(Arrays.asList(getResources().getStringArray(R.array.protocol_dianchi)));
        t tVar = new t(this.tvDianChi, arrayList, s());
        this.t = tVar;
        this.k = k(arrayList, R.string.console_controlOrder_param_BatteryProtocol, tVar);
        d.h.a.d.g<d.a.a.k.b<String>, t> l3 = l(R.array.protocol_dianbiao, R.string.console_controlOrder_param_AmmeterProtocol, this.tvDianBiao);
        this.l = l3.a();
        this.u = l3.b();
        ArrayList arrayList2 = new ArrayList(Arrays.asList(getResources().getStringArray(R.array.type_dianchi)));
        h hVar = new h(this.tvDianChiType, arrayList2, 1);
        this.v = hVar;
        this.m = k(arrayList2, R.string.console_controlOrder_param_sideBattery, hVar);
        d.h.a.d.g<d.a.a.k.b<String>, t> l4 = l(R.array.work_model, R.string.mode_type, this.mode_type);
        this.g = l4.a();
        this.h = l4.b();
        d.h.a.d.g<d.a.a.k.b<String>, t> l5 = l(R.array.enable_option, R.string.three_phrase_out, this.three_phrase_out);
        this.n = l5.a();
        this.x = l5.b();
        d.h.a.d.g<d.a.a.k.b<String>, t> l6 = l(R.array.enable_option, R.string.meter_check, this.meter_check);
        this.p = l6.a();
        this.y = l6.b();
        d.h.a.d.g<d.a.a.k.b<String>, t> l7 = l(R.array.enable_option, R.string.influx, this.influx);
        this.o = l7.a();
        this.w = l7.b();
        ArrayList arrayList3 = new ArrayList(Arrays.asList(this.B.getResources().getStringArray(R.array.platform_ip)));
        this.q = q(arrayList3, R.string.ServiceSettingIPTitle, new i(arrayList3));
    }

    /* loaded from: classes.dex */
    public static class t implements d.a.a.i.d {

        /* renamed from: a  reason: collision with root package name */
        public TextView f260a;

        /* renamed from: b  reason: collision with root package name */
        public List<String> f261b;

        /* renamed from: c  reason: collision with root package name */
        public SparseIntArray f262c;

        /* renamed from: d  reason: collision with root package name */
        public int f263d;

        public t(TextView textView, List<String> list, int i) {
            this.f260a = textView;
            this.f261b = list;
            this.f262c = new SparseIntArray();
            for (int i2 = 0; i2 < list.size(); i2++) {
                this.f262c.put(i2, i + i2);
            }
        }

        @Override // d.a.a.i.d
        public void a(int i, int i2, int i3, View view) {
            if (i < this.f261b.size()) {
                int i4 = this.f262c.get(i);
                this.f263d = i4;
                this.f260a.setText(this.f261b.get(i));
                this.f260a.setTag(Integer.valueOf(i4));
            }
        }

        public void b(int i) {
            int indexOfValue = this.f262c.indexOfValue(i);
            if (indexOfValue >= 0) {
                int keyAt = this.f262c.keyAt(indexOfValue);
                this.f263d = i;
                this.f260a.setText(this.f261b.get(keyAt));
                this.f260a.setTag(Integer.valueOf(i));
                return;
            }
            this.f263d = 0;
            this.f260a.setTag(null);
            this.f260a.setText(R.string.pls_select);
        }

        public t(TextView textView, List<String> list, SparseIntArray sparseIntArray) {
            this.f260a = textView;
            this.f261b = list;
            this.f262c = sparseIntArray;
        }
    }
}
