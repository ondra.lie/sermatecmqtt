package com.sermatec.sunmate.ui.control;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import com.chad.library.adapter.base.BaseItemDraggableAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.sermatec.sunmate.R;
import com.sermatec.sunmate.entity.TimeRange;
import com.sermatec.sunmate.ui.control.TimeRangeAdapter;
import java.util.List;
import java.util.Locale;

/* loaded from: classes.dex */
public class TimeRangeAdapter extends BaseItemDraggableAdapter<TimeRange, BaseViewHolder> {
    public TimeRangeAdapter(List<TimeRange> list) {
        super(R.layout.adapter_time_range, list);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: h0 */
    public /* synthetic */ void i0(BaseViewHolder baseViewHolder, View view) {
        O(baseViewHolder.getAdapterPosition());
    }

    public int e0(int i, int i2) {
        if (i >= i2) {
            return R.string.selectTimeError;
        }
        if (q().size() >= 4) {
            return R.string.timeRangeMaxError;
        }
        for (TimeRange timeRange : q()) {
            if (i >= timeRange.getStartTime() && i < timeRange.getEndTime()) {
                return R.string.timeTypeCrossError;
            }
            if (i2 > timeRange.getStartTime() && i2 <= timeRange.getEndTime()) {
                return R.string.timeTypeCrossError;
            }
        }
        return 0;
    }

    /* renamed from: f0 */
    public void l(@NonNull final BaseViewHolder baseViewHolder, TimeRange timeRange) {
        if (timeRange != null) {
            ((TextView) baseViewHolder.d(R.id.start_time)).setText(g0(timeRange.getStartTime()));
            ((TextView) baseViewHolder.d(R.id.end_time)).setText(g0(timeRange.getEndTime()));
            baseViewHolder.d(R.id.delete).setOnClickListener(new View.OnClickListener() { // from class: d.h.a.e.a.m2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    TimeRangeAdapter.this.i0(baseViewHolder, view);
                }
            });
        }
    }

    public final String g0(int i) {
        return String.format(Locale.CHINA, "%02d:%02d", Integer.valueOf(i / 60), Integer.valueOf(i % 60));
    }
}
