package com.sermatec.sunmate.ui.control;

import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import com.alibaba.fastjson.JSONArray;
import com.sermatec.sunmate.R;
import com.sermatec.sunmate.entity.CdSetting;
import com.sermatec.sunmate.entity.EffectDate;
import com.sermatec.sunmate.entity.ServerAndRouterInfo;
import com.sermatec.sunmate.entity.TimeRange;
import com.sermatec.sunmate.entity.WorkParam2;
import com.sermatec.sunmate.localControl.ConnectionManager;
import com.sermatec.sunmate.ui.control.ControlFragment;
import com.sermatec.sunmate.ui.control.ControlFragment2;
import com.sermatec.sunmate.ui.control.TimePickerDialogWrap2;
import com.sermatec.sunmate.ui.control.TimePickerDialogWrap3;
import d.h.a.d.i;
import d.h.a.e.a.q2;
import d.h.a.e.e.b;
import e.b.l;
import io.netty.channel.ChannelFuture;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

/* loaded from: classes.dex */
public class ControlFragment2 extends ControlFragment {
    public TimePickerDialogWrap2 Q;
    public TimePickerDialogWrap2 R;
    public TimeRangeAdapter S;
    public TimeRangeAdapter T;
    public TimePickerDialogWrap3 U;
    public TimePickerDialogWrap3 V;
    public EffectDateAdapter W;
    public EffectDateAdapter X;
    public d.a.a.k.b<String> Y;
    public d.a.a.k.b<String> Z;
    public d.a.a.k.b<String> a0;
    @BindView
    public TextView acidBatEOD;
    @BindView
    public TextView acidBatEqVol;
    @BindView
    public TextView acidBatFloatVol;
    @BindView
    public View acidBat_parent;
    @BindView
    public TextView add_fc_day;
    @BindView
    public TextView add_fc_time_range;
    @BindView
    public TextView add_fd_day;
    @BindView
    public TextView add_fd_time_range;
    public d.a.a.k.b<String> b0;
    @BindView
    public TextView backUpOrder;
    @BindView
    public TextView backUpOutEnable;
    @BindView
    public TextView batActive;
    @BindView
    public TextView batVolDownLimit;
    @BindView
    public View batVolLimit_parent;
    @BindView
    public TextView batVolSettable;
    @BindView
    public TextView batVolUpLimit;
    @BindView
    public TextView bat_power_adjustable;
    @BindView
    public View bat_power_adjustable_parent;
    @BindView
    public TextView btn_cdsetting;
    @BindView
    public TextView btn_local_wifi_setting;
    @BindView
    public TextView btn_send_parallel_config;
    public d.a.a.k.b<String> c0;
    @BindView
    public TextView cPowerLimit;
    public d.a.a.k.b<String> d0;
    @BindView
    public TextView dPowerLimit;
    public d.a.a.k.b<String> e0;
    @BindView
    public TextView eSCCurUpLimit;
    public d.a.a.k.b<String> f0;
    @BindView
    public SwitchCompat fcOpen;
    @BindView
    public TextView fcSocLimit;
    @BindView
    public RecyclerView fc_day_container;
    @BindView
    public View fc_day_container_parent;
    @BindView
    public View fc_open_parent;
    @BindView
    public RecyclerView fc_time_range_container;
    @BindView
    public View fc_time_range_container_parent;
    @BindView
    public SwitchCompat fdOpen;
    @BindView
    public TextView fdSocLimit;
    @BindView
    public RecyclerView fd_day_container;
    @BindView
    public View fd_day_container_parent;
    @BindView
    public View fd_open_parent;
    @BindView
    public RecyclerView fd_time_range_container;
    @BindView
    public View fd_time_range_container_parent;
    public d.a.a.k.b<String> g0;
    @BindView
    public TextView getIpType;
    public d.a.a.k.b<String> h0;
    @BindView
    public View high_set_parent_500;
    public d.a.a.k.b<String> i0;
    @BindView
    public TextView ip;
    @BindView
    public TextView ipGateway;
    @BindView
    public View ipGateway_container;
    @BindView
    public TextView ipMask;
    @BindView
    public View ipMask_container;
    @BindView
    public View ip_container;
    public d.a.a.k.b<String> j0;
    public ControlFragment.t k0;
    public ControlFragment.t l0;
    public ControlFragment.t m0;
    public ControlFragment.t n0;
    public ControlFragment.t o0;
    public ControlFragment.t p0;
    @BindView
    public TextView parallel_addr;
    @BindView
    public TextView parallel_flag;
    @BindView
    public TextView parallel_type;
    @BindView
    public View params_setting_parent_500;
    @BindView
    public TextView pvSourceChoice;
    public ControlFragment.t q0;
    public ControlFragment.t r0;
    public ControlFragment.t s0;
    @BindView
    public TextView shadowScan;
    @BindView
    public TextView sideWayEnable;
    @BindView
    public TextView socDiff;
    @BindView
    public View socDiff_parent;
    public ControlFragment.t t0;
    public ControlFragment.t u0;
    public e.b.v.b v0;
    @BindView
    public View view;
    @BindView
    public View vol_settable_parent;
    public e.b.v.b w0;
    @BindView
    public View work_mode_setting_500;
    public e.b.v.b x0;

    /* loaded from: classes.dex */
    public class a extends RecyclerView.AdapterDataObserver {
        public a() {
        }

        @Override // androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
        public void onChanged() {
            if (ControlFragment2.this.S.q().size() > 0) {
                ControlFragment2.this.fc_time_range_container_parent.setVisibility(0);
            } else {
                ControlFragment2.this.fc_time_range_container_parent.setVisibility(8);
            }
        }
    }

    /* loaded from: classes.dex */
    public class b extends RecyclerView.AdapterDataObserver {
        public b() {
        }

        @Override // androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
        public void onChanged() {
            if (ControlFragment2.this.T.q().size() > 0) {
                ControlFragment2.this.fd_time_range_container_parent.setVisibility(0);
            } else {
                ControlFragment2.this.fd_time_range_container_parent.setVisibility(8);
            }
        }
    }

    /* loaded from: classes.dex */
    public class c extends RecyclerView.AdapterDataObserver {
        public c() {
        }

        @Override // androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
        public void onChanged() {
            if (ControlFragment2.this.W.q().size() > 0) {
                ControlFragment2.this.fc_day_container_parent.setVisibility(0);
            } else {
                ControlFragment2.this.fc_day_container_parent.setVisibility(8);
            }
        }
    }

    /* loaded from: classes.dex */
    public class d extends RecyclerView.AdapterDataObserver {
        public d() {
        }

        @Override // androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
        public void onChanged() {
            if (ControlFragment2.this.X.q().size() > 0) {
                ControlFragment2.this.fd_day_container_parent.setVisibility(0);
            } else {
                ControlFragment2.this.fd_day_container_parent.setVisibility(8);
            }
        }
    }

    /* loaded from: classes.dex */
    public class e extends ControlFragment.t {
        public e(TextView textView, List list, int i) {
            super(textView, list, i);
        }

        @Override // com.sermatec.sunmate.ui.control.ControlFragment.t, d.a.a.i.d
        public void a(int i, int i2, int i3, View view) {
            super.a(i, i2, i3, view);
            c();
        }

        @Override // com.sermatec.sunmate.ui.control.ControlFragment.t
        public void b(int i) {
            super.b(i);
            c();
        }

        public final void c() {
            int i = this.f263d;
            if (i == 1) {
                ControlFragment2.this.batVolLimit_parent.setVisibility(0);
            } else if (i == 2) {
                ControlFragment2.this.batVolLimit_parent.setVisibility(8);
            }
        }
    }

    /* loaded from: classes.dex */
    public class f extends ControlFragment.t {
        public f(TextView textView, List list, int i) {
            super(textView, list, i);
        }

        @Override // com.sermatec.sunmate.ui.control.ControlFragment.t, d.a.a.i.d
        public void a(int i, int i2, int i3, View view) {
            super.a(i, i2, i3, view);
            int i4 = i == 0 ? 8 : 0;
            ControlFragment2.this.ip_container.setVisibility(i4);
            ControlFragment2.this.ipMask_container.setVisibility(i4);
            ControlFragment2.this.ipGateway_container.setVisibility(i4);
        }
    }

    /* loaded from: classes.dex */
    public class g extends ControlFragment.t {
        public g(TextView textView, List list, int i) {
            super(textView, list, i);
        }

        @Override // com.sermatec.sunmate.ui.control.ControlFragment.t, d.a.a.i.d
        public void a(int i, int i2, int i3, View view) {
            super.a(i, i2, i3, view);
            c();
        }

        @Override // com.sermatec.sunmate.ui.control.ControlFragment.t
        public void b(int i) {
            super.b(i);
            c();
        }

        public final void c() {
            if (this.f263d == 3) {
                ControlFragment2.this.socDiff_parent.setVisibility(0);
            } else {
                ControlFragment2.this.socDiff_parent.setVisibility(8);
            }
        }
    }

    /* loaded from: classes.dex */
    public class h extends ControlFragment.t {
        public h(TextView textView, List list, int i) {
            super(textView, list, i);
        }

        @Override // com.sermatec.sunmate.ui.control.ControlFragment.t, d.a.a.i.d
        public void a(int i, int i2, int i3, View view) {
            super.a(i, i2, i3, view);
            c();
        }

        @Override // com.sermatec.sunmate.ui.control.ControlFragment.t
        public void b(int i) {
            super.b(i);
            c();
        }

        public final void c() {
            if (this.f263d == 1) {
                ControlFragment2.this.bat_power_adjustable_parent.setVisibility(0);
            } else {
                ControlFragment2.this.bat_power_adjustable_parent.setVisibility(8);
            }
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: B0 */
    public /* synthetic */ void Y1(View view) {
        this.i0.u();
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: B1 */
    public /* synthetic */ ChannelFuture C1(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8) {
        return this.F.p(str, null, str2, null, null, null, null, null, null, null, str3, str4, str5, null, null, str6, str7, str8, null, null, null, null, null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: B2 */
    public /* synthetic */ void C2(Long l) {
        this.x0.dispose();
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: D1 */
    public /* synthetic */ ChannelFuture E1(Object obj, String str) {
        return this.F.o(u(obj), null, str);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: D2 */
    public /* synthetic */ void E2(ControlFragment.u uVar, CdSetting cdSetting) {
        this.x0.dispose();
        this.F.f863c = cdSetting;
        this.fcOpen.setChecked(cdSetting.getFcOpen() != 0);
        this.W.q().clear();
        HashSet hashSet = new HashSet();
        hashSet.add(1);
        hashSet.add(2);
        if (hashSet.contains(Integer.valueOf(cdSetting.getFcSunEnable()))) {
            this.W.f(new EffectDate(0, cdSetting.getFcSunEnable()));
        }
        if (hashSet.contains(Integer.valueOf(cdSetting.getFcSatEnable()))) {
            this.W.f(new EffectDate(1, cdSetting.getFcSatEnable()));
        }
        if (hashSet.contains(Integer.valueOf(cdSetting.getFcFriEnable()))) {
            this.W.f(new EffectDate(2, cdSetting.getFcFriEnable()));
        }
        if (hashSet.contains(Integer.valueOf(cdSetting.getFcThuEnable()))) {
            this.W.f(new EffectDate(3, cdSetting.getFcThuEnable()));
        }
        if (hashSet.contains(Integer.valueOf(cdSetting.getFcWenEnable()))) {
            this.W.f(new EffectDate(4, cdSetting.getFcWenEnable()));
        }
        if (hashSet.contains(Integer.valueOf(cdSetting.getFcTusEnable()))) {
            this.W.f(new EffectDate(5, cdSetting.getFcTusEnable()));
        }
        if (hashSet.contains(Integer.valueOf(cdSetting.getFcMonEnable()))) {
            this.W.f(new EffectDate(6, cdSetting.getFcMonEnable()));
        }
        this.fcSocLimit.setText(cdSetting.getFcSocLimit());
        this.fdOpen.setChecked(cdSetting.getFdOpen() != 0);
        this.X.q().clear();
        if (hashSet.contains(Integer.valueOf(cdSetting.getFdSunEnable()))) {
            this.X.f(new EffectDate(0, cdSetting.getFdSunEnable()));
        }
        if (hashSet.contains(Integer.valueOf(cdSetting.getFdSatEnable()))) {
            this.X.f(new EffectDate(1, cdSetting.getFdSatEnable()));
        }
        if (hashSet.contains(Integer.valueOf(cdSetting.getFdFriEnable()))) {
            this.X.f(new EffectDate(2, cdSetting.getFdFriEnable()));
        }
        if (hashSet.contains(Integer.valueOf(cdSetting.getFdThuEnable()))) {
            this.X.f(new EffectDate(3, cdSetting.getFdThuEnable()));
        }
        if (hashSet.contains(Integer.valueOf(cdSetting.getFdWenEnable()))) {
            this.X.f(new EffectDate(4, cdSetting.getFdWenEnable()));
        }
        if (hashSet.contains(Integer.valueOf(cdSetting.getFdTusEnable()))) {
            this.X.f(new EffectDate(5, cdSetting.getFdTusEnable()));
        }
        if (hashSet.contains(Integer.valueOf(cdSetting.getFdMonEnable()))) {
            this.X.f(new EffectDate(6, cdSetting.getFdMonEnable()));
        }
        this.fdSocLimit.setText(cdSetting.getFdSocLimit());
        this.S.q().clear();
        this.T.q().clear();
        this.S.g(cdSetting.getFcTimeRanges());
        this.T.g(cdSetting.getFdTimeRanges());
        k1(uVar);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: F0 */
    public /* synthetic */ void h2(View view) {
        this.Y.u();
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: F1 */
    public /* synthetic */ ChannelFuture G1(String str, String str2, String str3, String str4, String str5) {
        return this.F.f(str, str2, str3, str4, str5);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: F2 */
    public /* synthetic */ void G2(ControlFragment.u uVar, Long l) {
        this.v0.dispose();
        this.f225c.i();
        if (uVar != null) {
            uVar.a(false);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: H0 */
    public /* synthetic */ void i2(View view) {
        this.Z.u();
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: H2 */
    public /* synthetic */ void I2(e.b.v.b bVar, ControlFragment.u uVar, ServerAndRouterInfo serverAndRouterInfo) {
        bVar.dispose();
        this.v0.dispose();
        this.etRouterPwd.setText(serverAndRouterInfo.getPassword());
        this.routerssid.setText(serverAndRouterInfo.getSsid());
        this.r.b(serverAndRouterInfo.getStyle());
        this.etServerIp.setText(serverAndRouterInfo.getIp());
        this.etServerPort.setText(serverAndRouterInfo.getPort());
        this.f225c.i();
        if (uVar != null) {
            uVar.a(true);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: J0 */
    public /* synthetic */ void j2(View view) {
        this.a0.u();
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: L0 */
    public /* synthetic */ void k2(View view) {
        this.b0.u();
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: Q1 */
    public /* synthetic */ ChannelFuture R1(Object obj, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, Object obj2, String str9, String str10, String str11, String str12, String str13, String str14, String str15, String str16, JSONArray jSONArray, JSONArray jSONArray2) {
        return this.F.a(u(obj), str, str2, str3, str4, str5, str6, str7, str8, u(obj2), str9, str10, str11, str12, str13, str14, str15, str16, jSONArray.toJSONString(), jSONArray2.toJSONString(), u(Integer.valueOf(jSONArray.size())), u(Integer.valueOf(jSONArray2.size())));
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: S1 */
    public /* synthetic */ ChannelFuture T1(String str, String str2, String str3, String str4) {
        return this.F.p(null, null, null, null, null, null, null, null, str, str2, null, null, null, null, null, null, null, null, str3, null, null, null, str4);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: X */
    public /* synthetic */ void H1(View view) {
        this.c0.u();
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: Z */
    public /* synthetic */ void I1(View view) {
        this.d0.u();
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: Z1 */
    public /* synthetic */ ChannelFuture a2(String str, String str2, String str3) {
        return this.F.p(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, str, str2, str3, null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: b0 */
    public /* synthetic */ void J1(View view) {
        this.e0.u();
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: b2 */
    public /* synthetic */ void c2(View view) {
        if (!ConnectionManager.e().g()) {
            p();
            return;
        }
        final String str = null;
        final String obj = this.parallel_type.getTag() != null ? this.parallel_type.getTag().toString() : null;
        final String obj2 = this.parallel_flag.getTag() != null ? this.parallel_flag.getTag().toString() : null;
        if (this.parallel_addr.getTag() != null) {
            str = this.parallel_addr.getTag().toString();
        }
        ControlFragment.s t1Var = new ControlFragment.s() { // from class: d.h.a.e.a.t1
            @Override // com.sermatec.sunmate.ui.control.ControlFragment.s
            public final ChannelFuture a() {
                return ControlFragment2.this.a2(obj, obj2, str);
            }
        };
        ArrayList arrayList = new ArrayList();
        arrayList.add(t1Var);
        g1(arrayList);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: d0 */
    public /* synthetic */ void K1(View view) {
        this.f0.u();
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: d2 */
    public /* synthetic */ void e2(CompoundButton compoundButton, boolean z) {
        this.fc_open_parent.setVisibility(z ? 0 : 8);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: f0 */
    public /* synthetic */ void L1(View view) {
        this.j0.u();
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: f2 */
    public /* synthetic */ void g2(CompoundButton compoundButton, boolean z) {
        this.fd_open_parent.setVisibility(z ? 0 : 8);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: h0 */
    public /* synthetic */ void M1(View view) {
        this.Q.show(getChildFragmentManager(), "fcTimeRange");
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: j0 */
    public /* synthetic */ void N1(View view) {
        this.R.show(getChildFragmentManager(), "fdTimeRange");
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: l0 */
    public /* synthetic */ void O1(View view) {
        this.U.show(getChildFragmentManager(), "fcEffectDay");
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: l2 */
    public /* synthetic */ void m2(boolean z) {
        C();
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: n0 */
    public /* synthetic */ void P1(View view) {
        this.V.show(getChildFragmentManager(), "fdEffectDay");
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: n2 */
    public /* synthetic */ void o2(boolean z) {
        J2(new ControlFragment.u() { // from class: d.h.a.e.a.x1
            @Override // com.sermatec.sunmate.ui.control.ControlFragment.u
            public final void a(boolean z2) {
                ControlFragment2.this.m2(z2);
            }
        });
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: p2 */
    public /* synthetic */ void q2(int i, int i2, int i3, int i4) {
        TimeRange timeRange = new TimeRange((i * 60) + i2, (i3 * 60) + i4);
        int e0 = this.S.e0(timeRange.getStartTime(), timeRange.getEndTime());
        if (e0 != 0) {
            j(R.string.tip, e0);
        } else {
            this.S.f(timeRange);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: r1 */
    public /* synthetic */ ChannelFuture s1(String str, String str2, String str3, String str4, String str5) {
        return this.F.p(null, str, null, null, null, str2, null, str3, null, null, null, null, null, str4, str5, null, null, null, null, null, null, null, null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: r2 */
    public /* synthetic */ void s2(int i, int i2, int i3, int i4) {
        TimeRange timeRange = new TimeRange((i * 60) + i2, (i3 * 60) + i4);
        int e0 = this.T.e0(timeRange.getStartTime(), timeRange.getEndTime());
        if (e0 != 0) {
            j(R.string.tip, e0);
        } else {
            this.T.f(timeRange);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: t0 */
    public /* synthetic */ void U1(View view) {
        EffectDate next;
        if (!ConnectionManager.e().g()) {
            p();
            return;
        }
        final Integer valueOf = Integer.valueOf(this.fcOpen.isChecked() ? 1 : 0);
        int i = 0;
        int i2 = 0;
        Iterator<EffectDate> it = this.W.q().iterator();
        Integer num = 0;
        Integer num2 = 0;
        Integer num3 = 0;
        Integer num4 = 0;
        Integer num5 = 0;
        while (true) {
            Integer num6 = i2;
            while (it.hasNext()) {
                next = it.next();
                switch (next.getWeekday()) {
                    case 0:
                        i = Integer.valueOf(next.getEffectMode());
                    case 2:
                        num5 = Integer.valueOf(next.getEffectMode());
                    case 3:
                        num4 = Integer.valueOf(next.getEffectMode());
                    case 4:
                        num3 = Integer.valueOf(next.getEffectMode());
                    case 5:
                        num2 = Integer.valueOf(next.getEffectMode());
                    case 6:
                        num = Integer.valueOf(next.getEffectMode());
                }
            }
            final String u = u(this.fcSocLimit.getText().toString());
            final Integer valueOf2 = Integer.valueOf(this.fdOpen.isChecked() ? 1 : 0);
            Integer num7 = 0;
            Integer num8 = 0;
            Integer num9 = 0;
            Integer num10 = 0;
            Integer num11 = 0;
            Integer num12 = 0;
            Integer num13 = 0;
            for (EffectDate effectDate : this.X.q()) {
                switch (effectDate.getWeekday()) {
                    case 0:
                        num9 = Integer.valueOf(effectDate.getEffectMode());
                        break;
                    case 1:
                        num12 = Integer.valueOf(effectDate.getEffectMode());
                        break;
                    case 2:
                        num11 = Integer.valueOf(effectDate.getEffectMode());
                        break;
                    case 3:
                        num13 = Integer.valueOf(effectDate.getEffectMode());
                        break;
                    case 4:
                        num10 = Integer.valueOf(effectDate.getEffectMode());
                        break;
                    case 5:
                        num8 = Integer.valueOf(effectDate.getEffectMode());
                        break;
                    case 6:
                        num7 = Integer.valueOf(effectDate.getEffectMode());
                        break;
                }
            }
            final String u2 = u(this.fdSocLimit.getText().toString());
            List<TimeRange> q = this.S.q();
            Integer num14 = num10;
            List<TimeRange> q2 = this.T.q();
            Integer num15 = num8;
            JSONArray jSONArray = new JSONArray();
            Iterator<TimeRange> it2 = q.iterator();
            while (it2.hasNext()) {
                it2 = it2;
                jSONArray.add(it2.next().toJSONObject());
            }
            final JSONArray jSONArray2 = new JSONArray();
            for (TimeRange timeRange : q2) {
                jSONArray = jSONArray;
                jSONArray2.add(timeRange.toJSONObject());
            }
            final JSONArray jSONArray3 = jSONArray;
            final String u3 = u(i);
            final String u4 = u(num6);
            final String u5 = u(num5);
            final String u6 = u(num4);
            final String u7 = u(num3);
            final String u8 = u(num2);
            final String u9 = u(num);
            final String u10 = u(num9);
            final String u11 = u(num12);
            final String u12 = u(num11);
            final String u13 = u(num13);
            final String u14 = u(num14);
            final String u15 = u(num15);
            final String u16 = u(num7);
            ControlFragment.s u1Var = new ControlFragment.s() { // from class: d.h.a.e.a.u1
                @Override // com.sermatec.sunmate.ui.control.ControlFragment.s
                public final ChannelFuture a() {
                    return ControlFragment2.this.R1(valueOf, u3, u4, u5, u6, u7, u8, u9, u, valueOf2, u10, u11, u12, u13, u14, u15, u16, u2, jSONArray3, jSONArray2);
                }
            };
            String u17 = u(this.cPowerLimit.getText().toString());
            try {
                if (Double.parseDouble(u17) >= 4200.0d) {
                    u17 = "4200";
                }
            } catch (Exception unused) {
                u17 = "0";
            }
            final String str = u17;
            final String u18 = u(this.dPowerLimit.getText().toString());
            final String u19 = u(this.eSCCurUpLimit.getText().toString());
            final String u20 = u(this.bat_power_adjustable.getTag());
            ControlFragment.s k1Var = new ControlFragment.s() { // from class: d.h.a.e.a.k1
                @Override // com.sermatec.sunmate.ui.control.ControlFragment.s
                public final ChannelFuture a() {
                    return ControlFragment2.this.T1(str, u18, u19, u20);
                }
            };
            ArrayList arrayList = new ArrayList();
            arrayList.add(u1Var);
            arrayList.add(k1Var);
            g1(arrayList);
            return;
            i2 = Integer.valueOf(next.getEffectMode());
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: t1 */
    public /* synthetic */ ChannelFuture u1(Object obj) {
        return this.F.o(null, u(obj), null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: t2 */
    public /* synthetic */ void u2(int i, int i2) {
        EffectDate effectDate = new EffectDate(i, i2);
        int e0 = this.W.e0(effectDate);
        if (e0 != 0) {
            j(R.string.tip, e0);
        } else {
            this.W.f(effectDate);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: v0 */
    public /* synthetic */ void V1(View view) {
        if (!ConnectionManager.e().g()) {
            p();
            return;
        }
        Integer num = (Integer) this.getIpType.getTag();
        if (o(new Object[]{num})) {
            String str = "";
            String charSequence = num.intValue() == 1 ? str : this.ip.getText().toString();
            String charSequence2 = num.intValue() == 1 ? str : this.ipMask.getText().toString();
            if (num.intValue() != 1) {
                str = this.ipGateway.getText().toString();
            }
            if (num.intValue() == 1 || o(new Object[]{num, charSequence, charSequence2, str})) {
                c1(this.F.d(String.valueOf(num), charSequence, charSequence2, str), true);
            }
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: v1 */
    public /* synthetic */ ChannelFuture w1(Object obj) {
        return this.F.n(null, null, null, null, null, u(obj), null, null, null, null, null, null, null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: v2 */
    public /* synthetic */ void w2(int i, int i2) {
        EffectDate effectDate = new EffectDate(i, i2);
        int e0 = this.X.e0(effectDate);
        if (e0 != 0) {
            j(R.string.tip, e0);
        } else {
            this.X.f(effectDate);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: x0 */
    public /* synthetic */ void W1(View view) {
        this.g0.u();
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: x1 */
    public /* synthetic */ ChannelFuture y1(String str, String str2, String str3) {
        return this.F.p(null, null, null, str, str2, null, str3, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: x2 */
    public /* synthetic */ void y2(Long l) {
        this.w0.dispose();
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: z0 */
    public /* synthetic */ void X1(View view) {
        this.h0.u();
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: z1 */
    public /* synthetic */ ChannelFuture A1(String str, String str2) {
        return this.F.n(null, null, str, null, str2, null, null, null, null, null, null, null, null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: z2 */
    public /* synthetic */ void A2(ControlFragment.u uVar, WorkParam2 workParam2) {
        this.w0.dispose();
        this.F.f862b = workParam2;
        this.x.b(workParam2.getThreePhase());
        this.y.b(workParam2.getMeterMonitor());
        this.off_grid_soc_limit.setText(workParam2.getOffGridSoc());
        if (this.f226d >= 500) {
            this.socDiff.setText(workParam2.getSocDiff());
            this.k0.b(workParam2.getPvSourceChoice());
            this.l0.b(workParam2.getBackUpOrder());
            this.m0.b(workParam2.getSideWayEnable());
            this.n0.b(workParam2.getBackUpOutEnable());
            this.cPowerLimit.setText(workParam2.getcPowerLimit());
            this.dPowerLimit.setText(workParam2.getdPowerLimit());
            this.o0.b(workParam2.getBatVolSettable());
            this.batVolUpLimit.setText(workParam2.getBatVolUpLimit());
            this.batVolDownLimit.setText(workParam2.getBatVolDownLimit());
            this.p0.b(workParam2.getShadowScan());
            this.q0.b(workParam2.getBatActive());
            this.acidBatEqVol.setText(workParam2.getAcidBatEqVol());
            this.acidBatFloatVol.setText(workParam2.getAcidBatFloatVol());
            this.acidBatEOD.setText(workParam2.getAcidBatEOD());
            this.eSCCurUpLimit.setText(workParam2.geteSCCurUpLimit());
            this.r0.b(workParam2.getParallelType());
            this.s0.b(workParam2.getParallelFlag());
            this.t0.b(workParam2.getParallelAddr());
            this.u0.b(workParam2.getBatPowerAdjustable());
        }
        k1(uVar);
    }

    @Override // com.sermatec.sunmate.ui.control.ControlFragment
    public void A() {
        if (this.f226d >= 500) {
            i1(new ControlFragment.u() { // from class: d.h.a.e.a.q1
                @Override // com.sermatec.sunmate.ui.control.ControlFragment.u
                public final void a(boolean z) {
                    ControlFragment2.this.o2(z);
                }
            });
        } else {
            super.A();
        }
    }

    @Override // com.sermatec.sunmate.ui.control.ControlFragment
    public void B() {
        super.B();
        this.fc_time_range_container.setLayoutManager(new LinearLayoutManager(this.B));
        TimeRangeAdapter timeRangeAdapter = new TimeRangeAdapter(new ArrayList());
        this.S = timeRangeAdapter;
        timeRangeAdapter.registerAdapterDataObserver(new a());
        this.fc_time_range_container.setAdapter(this.S);
        this.Q = new TimePickerDialogWrap2(new TimePickerDialogWrap2.d() { // from class: d.h.a.e.a.z1
            @Override // com.sermatec.sunmate.ui.control.TimePickerDialogWrap2.d
            public final void a(int i, int i2, int i3, int i4) {
                ControlFragment2.this.q2(i, i2, i3, i4);
            }
        });
        this.fd_time_range_container.setLayoutManager(new LinearLayoutManager(this.B));
        TimeRangeAdapter timeRangeAdapter2 = new TimeRangeAdapter(new ArrayList());
        this.T = timeRangeAdapter2;
        timeRangeAdapter2.registerAdapterDataObserver(new b());
        this.fd_time_range_container.setAdapter(this.T);
        this.R = new TimePickerDialogWrap2(new TimePickerDialogWrap2.d() { // from class: d.h.a.e.a.m1
            @Override // com.sermatec.sunmate.ui.control.TimePickerDialogWrap2.d
            public final void a(int i, int i2, int i3, int i4) {
                ControlFragment2.this.s2(i, i2, i3, i4);
            }
        });
        this.fc_day_container.setLayoutManager(new LinearLayoutManager(this.B));
        EffectDateAdapter effectDateAdapter = new EffectDateAdapter(new ArrayList());
        this.W = effectDateAdapter;
        effectDateAdapter.registerAdapterDataObserver(new c());
        this.fc_day_container.setAdapter(this.W);
        this.U = new TimePickerDialogWrap3(new TimePickerDialogWrap3.c() { // from class: d.h.a.e.a.w0
            @Override // com.sermatec.sunmate.ui.control.TimePickerDialogWrap3.c
            public final void a(int i, int i2) {
                ControlFragment2.this.u2(i, i2);
            }
        });
        this.fd_day_container.setLayoutManager(new LinearLayoutManager(this.B));
        EffectDateAdapter effectDateAdapter2 = new EffectDateAdapter(new ArrayList());
        this.X = effectDateAdapter2;
        effectDateAdapter2.registerAdapterDataObserver(new d());
        this.fd_day_container.setAdapter(this.X);
        this.V = new TimePickerDialogWrap3(new TimePickerDialogWrap3.c() { // from class: d.h.a.e.a.b2
            @Override // com.sermatec.sunmate.ui.control.TimePickerDialogWrap3.c
            public final void a(int i, int i2) {
                ControlFragment2.this.w2(i, i2);
            }
        });
        d.h.a.d.g<d.a.a.k.b<String>, ControlFragment.t> m = m(R.array.pvSourceChoice, R.string.pvSourceChoice, this.pvSourceChoice, 0);
        this.Y = m.a();
        this.k0 = m.b();
        d.h.a.d.g<d.a.a.k.b<String>, ControlFragment.t> m2 = m(R.array.backUpOrder, R.string.backUpOrder, this.backUpOrder, 0);
        this.Z = m2.a();
        this.l0 = m2.b();
        d.h.a.d.g<d.a.a.k.b<String>, ControlFragment.t> l = l(R.array.enable_option, R.string.sideWayEnable, this.sideWayEnable);
        this.a0 = l.a();
        this.m0 = l.b();
        d.h.a.d.g<d.a.a.k.b<String>, ControlFragment.t> l2 = l(R.array.enable_option, R.string.backUpOutEnable, this.backUpOutEnable);
        this.b0 = l2.a();
        this.n0 = l2.b();
        ArrayList arrayList = new ArrayList(Arrays.asList(getResources().getStringArray(R.array.enable_option)));
        e eVar = new e(this.batVolSettable, arrayList, 1);
        this.o0 = eVar;
        this.c0 = k(arrayList, R.string.batVolSettable, eVar);
        d.h.a.d.g<d.a.a.k.b<String>, ControlFragment.t> l3 = l(R.array.enable_option, R.string.shadowScan, this.shadowScan);
        this.d0 = l3.a();
        this.p0 = l3.b();
        d.h.a.d.g<d.a.a.k.b<String>, ControlFragment.t> l4 = l(R.array.enable_option, R.string.batActive, this.batActive);
        this.e0 = l4.a();
        this.q0 = l4.b();
        ArrayList arrayList2 = new ArrayList(Arrays.asList(getResources().getStringArray(R.array.getIpType)));
        this.f0 = k(arrayList2, R.string.getIpType, new f(this.getIpType, arrayList2, 1));
        ArrayList arrayList3 = new ArrayList(Arrays.asList(getResources().getStringArray(R.array.work_model)));
        g gVar = new g(this.mode_type, arrayList3, 1);
        this.h = gVar;
        this.g = k(arrayList3, R.string.mode_type, gVar);
        ArrayList arrayList4 = new ArrayList(Arrays.asList(getResources().getStringArray(R.array.parallel_type)));
        ControlFragment.t tVar = new ControlFragment.t(this.parallel_type, arrayList4, 0);
        this.r0 = tVar;
        this.g0 = k(arrayList4, R.string.parallel_type, tVar);
        ArrayList arrayList5 = new ArrayList(Arrays.asList(getResources().getStringArray(R.array.parallel_flag)));
        ControlFragment.t tVar2 = new ControlFragment.t(this.parallel_flag, arrayList5, 0);
        this.s0 = tVar2;
        this.h0 = k(arrayList5, R.string.parallel_flag, tVar2);
        ArrayList arrayList6 = new ArrayList(Arrays.asList(getResources().getStringArray(R.array.parallel_addr)));
        ControlFragment.t tVar3 = new ControlFragment.t(this.parallel_addr, arrayList6, 1);
        this.t0 = tVar3;
        this.i0 = k(arrayList6, R.string.parallel_addr, tVar3);
        ArrayList arrayList7 = new ArrayList(Arrays.asList(getResources().getStringArray(R.array.enable_option)));
        h hVar = new h(this.bat_power_adjustable, arrayList7, 1);
        this.u0 = hVar;
        this.j0 = k(arrayList7, R.string.bat_power_adjustable, hVar);
    }

    public void J2(final ControlFragment.u uVar) {
        if (ConnectionManager.e().g()) {
            this.f225c.l();
            final e.b.v.b i = l.p(3L, TimeUnit.SECONDS).g(e.b.u.c.a.a()).i(new e.b.y.g() { // from class: d.h.a.e.a.a1
                @Override // e.b.y.g
                public final void accept(Object obj) {
                    ControlFragment2.this.G2(uVar, (Long) obj);
                }
            });
            this.v0 = i.f611e.g(e.b.u.c.a.a()).i(new e.b.y.g() { // from class: d.h.a.e.a.i1
                @Override // e.b.y.g
                public final void accept(Object obj) {
                    ControlFragment2.this.I2(i, uVar, (ServerAndRouterInfo) obj);
                }
            });
            this.F.g("B1");
        }
    }

    @Override // com.sermatec.sunmate.ui.control.ControlFragment
    public void Z0(int i) {
        if (i == 1) {
            this.acidBat_parent.setVisibility(8);
            this.vol_settable_parent.setVisibility(0);
        } else if (i == 2) {
            this.acidBat_parent.setVisibility(0);
            this.vol_settable_parent.setVisibility(8);
        }
    }

    @Override // com.sermatec.sunmate.ui.control.ControlFragment
    public void d1() {
        super.d1();
        if (this.f226d >= 500) {
            this.view.setVisibility(0);
            this.high_set_parent_500.setVisibility(0);
            this.params_setting_parent_500.setVisibility(0);
            this.work_mode_setting_500.setVisibility(0);
        }
    }

    @Override // com.sermatec.sunmate.ui.control.ControlFragment
    public void g1(@NonNull List<ControlFragment.s> list) {
        if (this.f226d >= 500) {
            q2 q2Var = this.F;
            if (q2Var.f862b == null || q2Var.f861a == null || q2Var.f863c == null) {
                e1();
            } else if (this.P) {
                Toast.makeText(this.B, (int) R.string.exec_command_rapidly, 0).show();
            } else {
                this.P = true;
                this.f225c.l();
                D(list);
            }
        } else {
            super.g1(list);
        }
    }

    @Override // com.sermatec.sunmate.ui.control.ControlFragment
    public boolean j1(final ControlFragment.u uVar) {
        boolean j1 = super.j1(uVar);
        if (j1) {
            if (this.f226d >= 259) {
                this.L = this.L.d(new e.b.y.g() { // from class: d.h.a.e.a.y0
                    @Override // e.b.y.g
                    public final void accept(Object obj) {
                        ControlFragment2.this.y2((Long) obj);
                    }
                });
                this.w0 = i.f609c.g(e.b.u.c.a.a()).i(new e.b.y.g() { // from class: d.h.a.e.a.x0
                    @Override // e.b.y.g
                    public final void accept(Object obj) {
                        ControlFragment2.this.A2(uVar, (WorkParam2) obj);
                    }
                });
            }
            if (this.f226d >= 500) {
                this.L = this.L.d(new e.b.y.g() { // from class: d.h.a.e.a.f1
                    @Override // e.b.y.g
                    public final void accept(Object obj) {
                        ControlFragment2.this.C2((Long) obj);
                    }
                });
                this.x0 = i.f610d.g(e.b.u.c.a.a()).i(new e.b.y.g() { // from class: d.h.a.e.a.b1
                    @Override // e.b.y.g
                    public final void accept(Object obj) {
                        ControlFragment2.this.E2(uVar, (CdSetting) obj);
                    }
                });
            }
        }
        return j1;
    }

    @Override // com.sermatec.sunmate.ui.control.ControlFragment
    public void k1(ControlFragment.u uVar) {
        if (this.f226d >= 500) {
            q2 q2Var = this.F;
            if (q2Var.f862b != null && q2Var.f861a != null && q2Var.f863c != null) {
                super.k1(uVar);
                return;
            }
            return;
        }
        super.k1(uVar);
    }

    @Override // com.sermatec.sunmate.ui.control.ControlFragment
    public List<ControlFragment.s> t() {
        ArrayList arrayList = new ArrayList();
        int c2 = ConnectionManager.e().f().getVersion().c();
        if (c2 >= 500) {
            final String u = u(this.three_phrase_out.getTag());
            final String u2 = u(this.backUpOrder.getTag());
            final String u3 = u(this.backUpOutEnable.getTag());
            final String u4 = u(this.shadowScan.getTag());
            final String u5 = u(this.batActive.getTag());
            arrayList.add(new ControlFragment.s() { // from class: d.h.a.e.a.d1
                @Override // com.sermatec.sunmate.ui.control.ControlFragment.s
                public final ChannelFuture a() {
                    return ControlFragment2.this.s1(u, u2, u3, u4, u5);
                }
            });
        } else if (c2 >= 259) {
            final Object tag = this.three_phrase_out.getTag();
            arrayList.add(new ControlFragment.s() { // from class: d.h.a.e.a.p1
                @Override // com.sermatec.sunmate.ui.control.ControlFragment.s
                public final ChannelFuture a() {
                    return ControlFragment2.this.u1(tag);
                }
            });
        }
        final Object tag2 = this.influx.getTag();
        arrayList.add(new ControlFragment.s() { // from class: d.h.a.e.a.w1
            @Override // com.sermatec.sunmate.ui.control.ControlFragment.s
            public final ChannelFuture a() {
                return ControlFragment2.this.w1(tag2);
            }
        });
        return arrayList;
    }

    @Override // com.sermatec.sunmate.ui.control.ControlFragment
    public List<ControlFragment.s> v() {
        List<ControlFragment.s> v = super.v();
        if (ConnectionManager.e().f().getVersion().c() >= 500) {
            final String u = u(this.socDiff.getText().toString());
            final String u2 = u(this.pvSourceChoice.getTag());
            final String u3 = u(this.sideWayEnable.getTag());
            v.add(new ControlFragment.s() { // from class: d.h.a.e.a.l1
                @Override // com.sermatec.sunmate.ui.control.ControlFragment.s
                public final ChannelFuture a() {
                    return ControlFragment2.this.y1(u, u2, u3);
                }
            });
        }
        return v;
    }

    @Override // com.sermatec.sunmate.ui.control.ControlFragment
    public List<ControlFragment.s> w() {
        ArrayList arrayList = new ArrayList();
        final String u = u(this.mode_grid_power.getText().toString());
        final String u2 = u(this.on_grid_soc_limit.getText().toString());
        arrayList.add(new ControlFragment.s() { // from class: d.h.a.e.a.a2
            @Override // com.sermatec.sunmate.ui.control.ControlFragment.s
            public final ChannelFuture a() {
                return ControlFragment2.this.A1(u, u2);
            }
        });
        final String u3 = u(this.tvDianChiType.getTag());
        int c2 = ConnectionManager.e().f().getVersion().c();
        if (c2 >= 500) {
            final String u4 = u(this.meter_check.getTag());
            final String u5 = u(this.off_grid_soc_limit.getText().toString());
            final String u6 = u(this.batVolSettable.getTag());
            final String u7 = u(this.batVolUpLimit.getText().toString());
            final String u8 = u(this.batVolDownLimit.getText().toString());
            final String u9 = u(this.acidBatEqVol.getText().toString());
            final String u10 = u(this.acidBatFloatVol.getText().toString());
            final String u11 = u(this.acidBatEOD.getText().toString());
            arrayList.add(new ControlFragment.s() { // from class: d.h.a.e.a.t0
                @Override // com.sermatec.sunmate.ui.control.ControlFragment.s
                public final ChannelFuture a() {
                    return ControlFragment2.this.C1(u4, u5, u6, u7, u8, u9, u10, u11);
                }
            });
        } else if (c2 >= 259) {
            final Object tag = this.meter_check.getTag();
            final String u12 = u(this.off_grid_soc_limit.getText().toString());
            arrayList.add(new ControlFragment.s() { // from class: d.h.a.e.a.d2
                @Override // com.sermatec.sunmate.ui.control.ControlFragment.s
                public final ChannelFuture a() {
                    return ControlFragment2.this.E1(tag, u12);
                }
            });
        }
        final String u13 = u(this.tvDianWang.getTag());
        final String u14 = u(this.etAh.getText().toString());
        final String u15 = u(this.tvDianChi.getTag());
        final String u16 = u(this.tvDianBiao.getTag());
        arrayList.add(new ControlFragment.s() { // from class: d.h.a.e.a.e1
            @Override // com.sermatec.sunmate.ui.control.ControlFragment.s
            public final ChannelFuture a() {
                return ControlFragment2.this.G1(u13, u3, u14, u15, u16);
            }
        });
        return arrayList;
    }

    @Override // com.sermatec.sunmate.ui.control.ControlFragment
    public void y() {
        super.y();
        this.fcOpen.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: d.h.a.e.a.s0
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                ControlFragment2.this.e2(compoundButton, z);
            }
        });
        this.fdOpen.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: d.h.a.e.a.r0
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                ControlFragment2.this.g2(compoundButton, z);
            }
        });
        d.h.a.e.e.b.b(this.pvSourceChoice, new b.a() { // from class: d.h.a.e.a.h1
            @Override // d.h.a.e.e.b.a
            public final void a(View view) {
                ControlFragment2.this.h2(view);
            }
        });
        d.h.a.e.e.b.b(this.backUpOrder, new b.a() { // from class: d.h.a.e.a.p0
            @Override // d.h.a.e.e.b.a
            public final void a(View view) {
                ControlFragment2.this.i2(view);
            }
        });
        d.h.a.e.e.b.b(this.sideWayEnable, new b.a() { // from class: d.h.a.e.a.n1
            @Override // d.h.a.e.e.b.a
            public final void a(View view) {
                ControlFragment2.this.j2(view);
            }
        });
        d.h.a.e.e.b.b(this.backUpOutEnable, new b.a() { // from class: d.h.a.e.a.c2
            @Override // d.h.a.e.e.b.a
            public final void a(View view) {
                ControlFragment2.this.k2(view);
            }
        });
        d.h.a.e.e.b.b(this.batVolSettable, new b.a() { // from class: d.h.a.e.a.y1
            @Override // d.h.a.e.e.b.a
            public final void a(View view) {
                ControlFragment2.this.H1(view);
            }
        });
        d.h.a.e.e.b.b(this.shadowScan, new b.a() { // from class: d.h.a.e.a.g2
            @Override // d.h.a.e.e.b.a
            public final void a(View view) {
                ControlFragment2.this.I1(view);
            }
        });
        d.h.a.e.e.b.b(this.batActive, new b.a() { // from class: d.h.a.e.a.v1
            @Override // d.h.a.e.e.b.a
            public final void a(View view) {
                ControlFragment2.this.J1(view);
            }
        });
        d.h.a.e.e.b.b(this.getIpType, new b.a() { // from class: d.h.a.e.a.q0
            @Override // d.h.a.e.e.b.a
            public final void a(View view) {
                ControlFragment2.this.K1(view);
            }
        });
        d.h.a.e.e.b.b(this.bat_power_adjustable, new b.a() { // from class: d.h.a.e.a.s1
            @Override // d.h.a.e.e.b.a
            public final void a(View view) {
                ControlFragment2.this.L1(view);
            }
        });
        d.h.a.e.e.b.b(this.add_fc_time_range, new b.a() { // from class: d.h.a.e.a.c1
            @Override // d.h.a.e.e.b.a
            public final void a(View view) {
                ControlFragment2.this.M1(view);
            }
        });
        d.h.a.e.e.b.b(this.add_fd_time_range, new b.a() { // from class: d.h.a.e.a.r1
            @Override // d.h.a.e.e.b.a
            public final void a(View view) {
                ControlFragment2.this.N1(view);
            }
        });
        d.h.a.e.e.b.b(this.add_fc_day, new b.a() { // from class: d.h.a.e.a.z0
            @Override // d.h.a.e.e.b.a
            public final void a(View view) {
                ControlFragment2.this.O1(view);
            }
        });
        d.h.a.e.e.b.b(this.add_fd_day, new b.a() { // from class: d.h.a.e.a.g1
            @Override // d.h.a.e.e.b.a
            public final void a(View view) {
                ControlFragment2.this.P1(view);
            }
        });
        d.h.a.e.e.b.b(this.btn_cdsetting, new b.a() { // from class: d.h.a.e.a.v0
            @Override // d.h.a.e.e.b.a
            public final void a(View view) {
                ControlFragment2.this.U1(view);
            }
        });
        d.h.a.e.e.b.b(this.btn_local_wifi_setting, new b.a() { // from class: d.h.a.e.a.e2
            @Override // d.h.a.e.e.b.a
            public final void a(View view) {
                ControlFragment2.this.V1(view);
            }
        });
        d.h.a.e.e.b.b(this.parallel_type, new b.a() { // from class: d.h.a.e.a.j1
            @Override // d.h.a.e.e.b.a
            public final void a(View view) {
                ControlFragment2.this.W1(view);
            }
        });
        d.h.a.e.e.b.b(this.parallel_flag, new b.a() { // from class: d.h.a.e.a.o1
            @Override // d.h.a.e.e.b.a
            public final void a(View view) {
                ControlFragment2.this.X1(view);
            }
        });
        d.h.a.e.e.b.b(this.parallel_addr, new b.a() { // from class: d.h.a.e.a.f2
            @Override // d.h.a.e.e.b.a
            public final void a(View view) {
                ControlFragment2.this.Y1(view);
            }
        });
        d.h.a.e.e.b.b(this.btn_send_parallel_config, new b.a() { // from class: d.h.a.e.a.u0
            @Override // d.h.a.e.e.b.a
            public final void a(View view) {
                ControlFragment2.this.c2(view);
            }
        });
    }
}
