package com.sermatec.sunmate.ui.control;

import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import c.b.a;
import com.sermatec.sunmate.R;

/* loaded from: classes.dex */
public class ControlFragment_ViewBinding implements Unbinder {

    /* renamed from: b  reason: collision with root package name */
    public ControlFragment f276b;

    @UiThread
    public ControlFragment_ViewBinding(ControlFragment controlFragment, View view) {
        this.f276b = controlFragment;
        controlFragment.new_wifi_password = (EditText) a.c(view, R.id.new_wifi_password, "field 'new_wifi_password'", EditText.class);
        controlFragment.ensure_new_wifi_password = (EditText) a.c(view, R.id.ensure_new_wifi_password, "field 'ensure_new_wifi_password'", EditText.class);
        controlFragment.changWifiPwdOk = (TextView) a.c(view, R.id.btn_changwifipad_sure, "field 'changWifiPwdOk'", TextView.class);
        controlFragment.mRadioOpen = (RadioButton) a.c(view, R.id.radio_open, "field 'mRadioOpen'", RadioButton.class);
        controlFragment.radio_close = (RadioButton) a.c(view, R.id.radio_close, "field 'radio_close'", RadioButton.class);
        controlFragment.onOffOk = (TextView) a.c(view, R.id.btn_open_sure, "field 'onOffOk'", TextView.class);
        controlFragment.onOff = (RadioGroup) a.c(view, R.id.onOff, "field 'onOff'", RadioGroup.class);
        controlFragment.meterMonitor_parent = a.b(view, R.id.meterMonitor_parent, "field 'meterMonitor_parent'");
        controlFragment.offGridSoc_parent = a.b(view, R.id.offGridSoc_parent, "field 'offGridSoc_parent'");
        controlFragment.tvDianWang = (TextView) a.c(view, R.id.tv_grid_code, "field 'tvDianWang'", TextView.class);
        controlFragment.tvDianChi = (TextView) a.c(view, R.id.tv_dianchi_protocol, "field 'tvDianChi'", TextView.class);
        controlFragment.tv_dianchi_protocol_parent = a.b(view, R.id.tv_dianchi_protocol_parent, "field 'tv_dianchi_protocol_parent'");
        controlFragment.tv_ah_parent = a.b(view, R.id.tv_ah_parent, "field 'tv_ah_parent'");
        controlFragment.tvDianBiao = (TextView) a.c(view, R.id.tv_dianbiao_protocol, "field 'tvDianBiao'", TextView.class);
        controlFragment.tvDianChiType = (TextView) a.c(view, R.id.tv_dianchi_type, "field 'tvDianChiType'", TextView.class);
        controlFragment.etAh = (EditText) a.c(view, R.id.tv_ah, "field 'etAh'", EditText.class);
        controlFragment.workModeOk = (TextView) a.c(view, R.id.btn_workmode_sure, "field 'workModeOk'", TextView.class);
        controlFragment.mode_grid_power = (EditText) a.c(view, R.id.mode_grid_power, "field 'mode_grid_power'", EditText.class);
        controlFragment.on_grid_soc_limit = (EditText) a.c(view, R.id.on_grid_soc_limit, "field 'on_grid_soc_limit'", EditText.class);
        controlFragment.off_grid_soc_limit = (EditText) a.c(view, R.id.offGridSoc, "field 'off_grid_soc_limit'", EditText.class);
        controlFragment.meter_check = (TextView) a.c(view, R.id.meterMonitor, "field 'meter_check'", TextView.class);
        controlFragment.routerOk = (TextView) a.c(view, R.id.btn_router_sure, "field 'routerOk'", TextView.class);
        controlFragment.etRouterPwd = (EditText) a.c(view, R.id.edit_router_pwd, "field 'etRouterPwd'", EditText.class);
        controlFragment.routerssid = (TextView) a.c(view, R.id.routerssid, "field 'routerssid'", TextView.class);
        controlFragment.tvRouterMode = (TextView) a.c(view, R.id.tv_workmode, "field 'tvRouterMode'", TextView.class);
        controlFragment.etServerIp = (TextView) a.c(view, R.id.edit_server_ip, "field 'etServerIp'", TextView.class);
        controlFragment.etServerPort = (EditText) a.c(view, R.id.edit_server_port, "field 'etServerPort'", EditText.class);
        controlFragment.serverOk = (TextView) a.c(view, R.id.btn_server_sure, "field 'serverOk'", TextView.class);
        controlFragment.updatePucOk = (TextView) a.c(view, R.id.btn_update_puc, "field 'updatePucOk'", TextView.class);
        controlFragment.updateDspOk = (TextView) a.c(view, R.id.btn_update_dsp, "field 'updateDspOk'", TextView.class);
        controlFragment.resetOk = (TextView) a.c(view, R.id.btn_reset_sure, "field 'resetOk'", TextView.class);
        controlFragment.mode_type = (TextView) a.c(view, R.id.mode_type, "field 'mode_type'", TextView.class);
        controlFragment.mode_price_top = (EditText) a.c(view, R.id.mode_price_top, "field 'mode_price_top'", EditText.class);
        controlFragment.mode_price_flat = (EditText) a.c(view, R.id.mode_price_flat, "field 'mode_price_flat'", EditText.class);
        controlFragment.mode_price_peak = (EditText) a.c(view, R.id.mode_price_peak, "field 'mode_price_peak'", EditText.class);
        controlFragment.mode_price_valley = (EditText) a.c(view, R.id.mode_price_valley, "field 'mode_price_valley'", EditText.class);
        controlFragment.add_price_time = (TextView) a.c(view, R.id.add_price_time, "field 'add_price_time'", TextView.class);
        controlFragment.btn_work_mode_sure = (TextView) a.c(view, R.id.btn_work_mode_sure, "field 'btn_work_mode_sure'", TextView.class);
        controlFragment.time_type_container = (RecyclerView) a.c(view, R.id.time_type_container, "field 'time_type_container'", RecyclerView.class);
        controlFragment.time_type_container_parent = a.b(view, R.id.time_type_container_parent, "field 'time_type_container_parent'");
        controlFragment.threePhase_parent = a.b(view, R.id.threePhase_parent, "field 'threePhase_parent'");
        controlFragment.three_phrase_out = (TextView) a.c(view, R.id.threePhase, "field 'three_phrase_out'", TextView.class);
        controlFragment.influx = (TextView) a.c(view, R.id.influx, "field 'influx'", TextView.class);
        controlFragment.btn_work_mode2 = (TextView) a.c(view, R.id.btn_work_mode2, "field 'btn_work_mode2'", TextView.class);
    }
}
