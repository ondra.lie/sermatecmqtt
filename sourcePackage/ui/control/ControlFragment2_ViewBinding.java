package com.sermatec.sunmate.ui.control;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.SwitchCompat;
import androidx.recyclerview.widget.RecyclerView;
import c.b.a;
import com.sermatec.sunmate.R;

/* loaded from: classes.dex */
public class ControlFragment2_ViewBinding extends ControlFragment_ViewBinding {

    /* renamed from: c  reason: collision with root package name */
    public ControlFragment2 f275c;

    @UiThread
    public ControlFragment2_ViewBinding(ControlFragment2 controlFragment2, View view) {
        super(controlFragment2, view);
        this.f275c = controlFragment2;
        controlFragment2.work_mode_setting_500 = a.b(view, R.id.work_mode_setting_500, "field 'work_mode_setting_500'");
        controlFragment2.socDiff_parent = a.b(view, R.id.socDiff_parent, "field 'socDiff_parent'");
        controlFragment2.sideWayEnable = (TextView) a.c(view, R.id.sideWayEnable, "field 'sideWayEnable'", TextView.class);
        controlFragment2.socDiff = (TextView) a.c(view, R.id.socDiff, "field 'socDiff'", TextView.class);
        controlFragment2.pvSourceChoice = (TextView) a.c(view, R.id.pvSourceChoice, "field 'pvSourceChoice'", TextView.class);
        controlFragment2.params_setting_parent_500 = a.b(view, R.id.params_setting_parent_500, "field 'params_setting_parent_500'");
        controlFragment2.vol_settable_parent = a.b(view, R.id.vol_settable_parent, "field 'vol_settable_parent'");
        controlFragment2.acidBat_parent = a.b(view, R.id.acidBat_parent, "field 'acidBat_parent'");
        controlFragment2.acidBatEqVol = (TextView) a.c(view, R.id.acidBatEqVol, "field 'acidBatEqVol'", TextView.class);
        controlFragment2.acidBatFloatVol = (TextView) a.c(view, R.id.acidBatFloatVol, "field 'acidBatFloatVol'", TextView.class);
        controlFragment2.acidBatEOD = (TextView) a.c(view, R.id.acidBatEOD, "field 'acidBatEOD'", TextView.class);
        controlFragment2.batVolSettable = (TextView) a.c(view, R.id.batVolSettable, "field 'batVolSettable'", TextView.class);
        controlFragment2.batVolLimit_parent = a.b(view, R.id.batVolLimit_parent, "field 'batVolLimit_parent'");
        controlFragment2.batVolUpLimit = (TextView) a.c(view, R.id.batVolUpLimit, "field 'batVolUpLimit'", TextView.class);
        controlFragment2.batVolDownLimit = (TextView) a.c(view, R.id.batVolDownLimit, "field 'batVolDownLimit'", TextView.class);
        controlFragment2.high_set_parent_500 = a.b(view, R.id.high_set_parent_500, "field 'high_set_parent_500'");
        controlFragment2.shadowScan = (TextView) a.c(view, R.id.shadowScan, "field 'shadowScan'", TextView.class);
        controlFragment2.batActive = (TextView) a.c(view, R.id.batActive, "field 'batActive'", TextView.class);
        controlFragment2.backUpOrder = (TextView) a.c(view, R.id.backUpOrder, "field 'backUpOrder'", TextView.class);
        controlFragment2.backUpOutEnable = (TextView) a.c(view, R.id.backUpOutEnable, "field 'backUpOutEnable'", TextView.class);
        controlFragment2.bat_power_adjustable = (TextView) a.c(view, R.id.bat_power_adjustable, "field 'bat_power_adjustable'", TextView.class);
        controlFragment2.bat_power_adjustable_parent = a.b(view, R.id.bat_power_adjustable_parent, "field 'bat_power_adjustable_parent'");
        controlFragment2.eSCCurUpLimit = (TextView) a.c(view, R.id.eSCCurUpLimit, "field 'eSCCurUpLimit'", TextView.class);
        controlFragment2.cPowerLimit = (TextView) a.c(view, R.id.cPowerLimit, "field 'cPowerLimit'", TextView.class);
        controlFragment2.dPowerLimit = (TextView) a.c(view, R.id.dPowerLimit, "field 'dPowerLimit'", TextView.class);
        controlFragment2.fc_open_parent = a.b(view, R.id.fc_open_parent, "field 'fc_open_parent'");
        controlFragment2.fd_open_parent = a.b(view, R.id.fd_open_parent, "field 'fd_open_parent'");
        controlFragment2.fcOpen = (SwitchCompat) a.c(view, R.id.fcOpen, "field 'fcOpen'", SwitchCompat.class);
        controlFragment2.fcSocLimit = (TextView) a.c(view, R.id.fcSocLimit, "field 'fcSocLimit'", TextView.class);
        controlFragment2.fc_time_range_container = (RecyclerView) a.c(view, R.id.fc_time_range_container, "field 'fc_time_range_container'", RecyclerView.class);
        controlFragment2.fc_time_range_container_parent = a.b(view, R.id.fc_time_range_container_parent, "field 'fc_time_range_container_parent'");
        controlFragment2.add_fc_time_range = (TextView) a.c(view, R.id.add_fc_time_range, "field 'add_fc_time_range'", TextView.class);
        controlFragment2.fdOpen = (SwitchCompat) a.c(view, R.id.fdOpen, "field 'fdOpen'", SwitchCompat.class);
        controlFragment2.fdSocLimit = (TextView) a.c(view, R.id.fdSocLimit, "field 'fdSocLimit'", TextView.class);
        controlFragment2.fd_time_range_container = (RecyclerView) a.c(view, R.id.fd_time_range_container, "field 'fd_time_range_container'", RecyclerView.class);
        controlFragment2.fd_time_range_container_parent = a.b(view, R.id.fd_time_range_container_parent, "field 'fd_time_range_container_parent'");
        controlFragment2.add_fd_time_range = (TextView) a.c(view, R.id.add_fd_time_range, "field 'add_fd_time_range'", TextView.class);
        controlFragment2.add_fc_day = (TextView) a.c(view, R.id.add_fc_day, "field 'add_fc_day'", TextView.class);
        controlFragment2.add_fd_day = (TextView) a.c(view, R.id.add_fd_day, "field 'add_fd_day'", TextView.class);
        controlFragment2.fc_day_container_parent = a.b(view, R.id.fc_day_container_parent, "field 'fc_day_container_parent'");
        controlFragment2.fd_day_container_parent = a.b(view, R.id.fd_day_container_parent, "field 'fd_day_container_parent'");
        controlFragment2.fc_day_container = (RecyclerView) a.c(view, R.id.fc_day_container, "field 'fc_day_container'", RecyclerView.class);
        controlFragment2.fd_day_container = (RecyclerView) a.c(view, R.id.fd_day_container, "field 'fd_day_container'", RecyclerView.class);
        controlFragment2.getIpType = (TextView) a.c(view, R.id.getIpType, "field 'getIpType'", TextView.class);
        controlFragment2.ip = (TextView) a.c(view, R.id.ip, "field 'ip'", TextView.class);
        controlFragment2.ipMask = (TextView) a.c(view, R.id.ipMask, "field 'ipMask'", TextView.class);
        controlFragment2.ipGateway = (TextView) a.c(view, R.id.ipGateway, "field 'ipGateway'", TextView.class);
        controlFragment2.ip_container = a.b(view, R.id.ip_container, "field 'ip_container'");
        controlFragment2.ipMask_container = a.b(view, R.id.ipMask_container, "field 'ipMask_container'");
        controlFragment2.ipGateway_container = a.b(view, R.id.ipGateway_container, "field 'ipGateway_container'");
        controlFragment2.btn_cdsetting = (TextView) a.c(view, R.id.btn_cdsetting, "field 'btn_cdsetting'", TextView.class);
        controlFragment2.btn_local_wifi_setting = (TextView) a.c(view, R.id.btn_local_wifi_setting, "field 'btn_local_wifi_setting'", TextView.class);
        controlFragment2.parallel_type = (TextView) a.c(view, R.id.parallel_type, "field 'parallel_type'", TextView.class);
        controlFragment2.parallel_flag = (TextView) a.c(view, R.id.parallel_flag, "field 'parallel_flag'", TextView.class);
        controlFragment2.parallel_addr = (TextView) a.c(view, R.id.parallel_addr, "field 'parallel_addr'", TextView.class);
        controlFragment2.btn_send_parallel_config = (TextView) a.c(view, R.id.btn_send_parallel_config, "field 'btn_send_parallel_config'", TextView.class);
        controlFragment2.view = a.b(view, R.id.version_500, "field 'view'");
    }
}
