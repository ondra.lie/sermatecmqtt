package com.sermatec.sunmate.ui.control;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import com.chad.library.adapter.base.BaseItemDraggableAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.sermatec.sunmate.R;
import com.sermatec.sunmate.entity.EffectDate;
import com.sermatec.sunmate.ui.control.EffectDateAdapter;
import java.util.HashSet;
import java.util.List;

/* loaded from: classes.dex */
public class EffectDateAdapter extends BaseItemDraggableAdapter<EffectDate, BaseViewHolder> {
    public EffectDateAdapter(List<EffectDate> list) {
        super(R.layout.adapter_effect_date, list);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: g0 */
    public /* synthetic */ void h0(BaseViewHolder baseViewHolder, View view) {
        O(baseViewHolder.getAdapterPosition());
    }

    public int e0(EffectDate effectDate) {
        if (!new HashSet(q()).add(effectDate)) {
            return R.string.date_exists;
        }
        return 0;
    }

    /* renamed from: f0 */
    public void l(@NonNull final BaseViewHolder baseViewHolder, EffectDate effectDate) {
        if (effectDate != null) {
            String[] stringArray = this.v.getResources().getStringArray(R.array.effect_mode);
            ((TextView) baseViewHolder.d(R.id.weekday)).setText(this.v.getResources().getStringArray(R.array.weekdays)[effectDate.getWeekday()]);
            ((TextView) baseViewHolder.d(R.id.effectTime)).setText(stringArray[effectDate.getEffectMode() - 1]);
            baseViewHolder.d(R.id.delete).setOnClickListener(new View.OnClickListener() { // from class: d.h.a.e.a.h2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    EffectDateAdapter.this.h0(baseViewHolder, view);
                }
            });
        }
    }
}
