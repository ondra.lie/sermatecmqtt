package com.sermatec.sunmate.ui.control;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.NumberPicker;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import com.sermatec.sunmate.R;
import com.sermatec.sunmate.ui.control.TimePickerDialogWrap;
import java.util.Locale;

/* loaded from: classes.dex */
public class TimePickerDialogWrap extends DialogFragment {

    /* renamed from: a  reason: collision with root package name */
    public d f277a;

    /* renamed from: b  reason: collision with root package name */
    public NumberPicker f278b;

    /* renamed from: c  reason: collision with root package name */
    public NumberPicker f279c;

    /* renamed from: d  reason: collision with root package name */
    public NumberPicker f280d;

    /* renamed from: e  reason: collision with root package name */
    public NumberPicker f281e;

    /* renamed from: f  reason: collision with root package name */
    public NumberPicker f282f;
    public int g;
    public int h;
    public int i;
    public int j;
    public int k;

    /* loaded from: classes.dex */
    public class a implements NumberPicker.Formatter {
        public a() {
        }

        @Override // android.widget.NumberPicker.Formatter
        public String format(int i) {
            return String.format(Locale.CHINA, "%02d", Integer.valueOf(i));
        }
    }

    /* loaded from: classes.dex */
    public class b implements View.OnClickListener {
        public b() {
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            TimePickerDialogWrap timePickerDialogWrap = TimePickerDialogWrap.this;
            timePickerDialogWrap.g = timePickerDialogWrap.f278b.getValue();
            TimePickerDialogWrap timePickerDialogWrap2 = TimePickerDialogWrap.this;
            timePickerDialogWrap2.h = timePickerDialogWrap2.f279c.getValue();
            TimePickerDialogWrap timePickerDialogWrap3 = TimePickerDialogWrap.this;
            timePickerDialogWrap3.i = timePickerDialogWrap3.f280d.getValue();
            TimePickerDialogWrap timePickerDialogWrap4 = TimePickerDialogWrap.this;
            timePickerDialogWrap4.j = timePickerDialogWrap4.f281e.getValue();
            TimePickerDialogWrap timePickerDialogWrap5 = TimePickerDialogWrap.this;
            timePickerDialogWrap5.k = timePickerDialogWrap5.f282f.getValue();
            TimePickerDialogWrap.this.f277a.a(TimePickerDialogWrap.this.g, TimePickerDialogWrap.this.h, TimePickerDialogWrap.this.i, TimePickerDialogWrap.this.j, TimePickerDialogWrap.this.k);
            TimePickerDialogWrap.this.dismiss();
        }
    }

    /* loaded from: classes.dex */
    public class c implements View.OnClickListener {
        public c() {
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            TimePickerDialogWrap.this.dismiss();
        }
    }

    /* loaded from: classes.dex */
    public interface d {
        void a(int i, int i2, int i3, int i4, int i5);
    }

    public TimePickerDialogWrap(@NonNull d dVar) {
        this.f277a = dVar;
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: q */
    public /* synthetic */ void r(NumberPicker numberPicker, int i, int i2) {
        if (i2 == 24) {
            this.f280d.setMaxValue(0);
        } else {
            this.f280d.setMaxValue(59);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: s */
    public /* synthetic */ void t(NumberPicker numberPicker, int i, int i2) {
        if (i2 == 24) {
            this.f282f.setMaxValue(0);
        } else {
            this.f282f.setMaxValue(59);
        }
    }

    @Override // androidx.fragment.app.DialogFragment
    @NonNull
    public Dialog onCreateDialog(@Nullable Bundle bundle) {
        AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
        View inflate = requireActivity().getLayoutInflater().inflate(R.layout.time_select, (ViewGroup) null);
        NumberPicker numberPicker = (NumberPicker) inflate.findViewById(R.id.timeType);
        this.f278b = numberPicker;
        numberPicker.setWrapSelectorWheel(false);
        this.f278b.setMinValue(0);
        this.f278b.setMaxValue(3);
        this.f278b.setDisplayedValues(requireContext().getResources().getStringArray(R.array.time_type));
        this.f278b.setValue(this.g);
        NumberPicker numberPicker2 = (NumberPicker) inflate.findViewById(R.id.startHour);
        this.f279c = numberPicker2;
        numberPicker2.setWrapSelectorWheel(false);
        this.f279c.setMinValue(0);
        this.f279c.setMaxValue(24);
        this.f279c.setValue(this.h);
        NumberPicker numberPicker3 = (NumberPicker) inflate.findViewById(R.id.endHour);
        this.f281e = numberPicker3;
        numberPicker3.setWrapSelectorWheel(false);
        this.f281e.setMinValue(0);
        this.f281e.setMaxValue(24);
        this.f281e.setValue(this.j);
        a aVar = new a();
        NumberPicker numberPicker4 = (NumberPicker) inflate.findViewById(R.id.startMin);
        this.f280d = numberPicker4;
        numberPicker4.setWrapSelectorWheel(false);
        this.f280d.setMinValue(0);
        this.f280d.setMaxValue(59);
        this.f280d.setValue(this.i);
        this.f280d.setFormatter(aVar);
        NumberPicker numberPicker5 = (NumberPicker) inflate.findViewById(R.id.endMin);
        this.f282f = numberPicker5;
        numberPicker5.setWrapSelectorWheel(false);
        this.f282f.setMinValue(0);
        this.f282f.setMaxValue(59);
        this.f282f.setValue(this.k);
        this.f282f.setFormatter(aVar);
        this.f279c.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() { // from class: d.h.a.e.a.j2
            @Override // android.widget.NumberPicker.OnValueChangeListener
            public final void onValueChange(NumberPicker numberPicker6, int i, int i2) {
                TimePickerDialogWrap.this.r(numberPicker6, i, i2);
            }
        });
        this.f281e.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() { // from class: d.h.a.e.a.i2
            @Override // android.widget.NumberPicker.OnValueChangeListener
            public final void onValueChange(NumberPicker numberPicker6, int i, int i2) {
                TimePickerDialogWrap.this.t(numberPicker6, i, i2);
            }
        });
        TextView textView = (TextView) inflate.findViewById(R.id.positiveBtn);
        textView.setClickable(true);
        textView.setOnClickListener(new b());
        TextView textView2 = (TextView) inflate.findViewById(R.id.negativeBtn);
        textView2.setClickable(true);
        textView2.setOnClickListener(new c());
        AlertDialog create = builder.setView(inflate).create();
        create.getWindow().setBackgroundDrawableResource(17170445);
        create.setCanceledOnTouchOutside(false);
        return create;
    }

    public TimePickerDialogWrap(d dVar, int i, int i2, int i3, int i4, int i5) {
        this.f277a = dVar;
        this.g = i;
        this.h = i2;
        this.i = i3;
        this.j = i4;
        this.k = i5;
    }
}
