package com.sermatec.sunmate.ui.control;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.NumberPicker;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import com.sermatec.sunmate.R;
import com.sermatec.sunmate.ui.control.TimePickerDialogWrap2;
import java.util.Locale;

/* loaded from: classes.dex */
public class TimePickerDialogWrap2 extends DialogFragment {

    /* renamed from: a  reason: collision with root package name */
    public d f286a;

    /* renamed from: b  reason: collision with root package name */
    public NumberPicker f287b;

    /* renamed from: c  reason: collision with root package name */
    public NumberPicker f288c;

    /* renamed from: d  reason: collision with root package name */
    public NumberPicker f289d;

    /* renamed from: e  reason: collision with root package name */
    public NumberPicker f290e;

    /* renamed from: f  reason: collision with root package name */
    public int f291f;
    public int g;
    public int h;
    public int i;

    /* loaded from: classes.dex */
    public class a implements NumberPicker.Formatter {
        public a() {
        }

        @Override // android.widget.NumberPicker.Formatter
        public String format(int i) {
            return String.format(Locale.CHINA, "%02d", Integer.valueOf(i));
        }
    }

    /* loaded from: classes.dex */
    public class b implements View.OnClickListener {
        public b() {
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            TimePickerDialogWrap2 timePickerDialogWrap2 = TimePickerDialogWrap2.this;
            timePickerDialogWrap2.f291f = timePickerDialogWrap2.f287b.getValue();
            TimePickerDialogWrap2 timePickerDialogWrap22 = TimePickerDialogWrap2.this;
            timePickerDialogWrap22.g = timePickerDialogWrap22.f288c.getValue();
            TimePickerDialogWrap2 timePickerDialogWrap23 = TimePickerDialogWrap2.this;
            timePickerDialogWrap23.h = timePickerDialogWrap23.f289d.getValue();
            TimePickerDialogWrap2 timePickerDialogWrap24 = TimePickerDialogWrap2.this;
            timePickerDialogWrap24.i = timePickerDialogWrap24.f290e.getValue();
            TimePickerDialogWrap2.this.f286a.a(TimePickerDialogWrap2.this.f291f, TimePickerDialogWrap2.this.g, TimePickerDialogWrap2.this.h, TimePickerDialogWrap2.this.i);
            TimePickerDialogWrap2.this.dismiss();
        }
    }

    /* loaded from: classes.dex */
    public class c implements View.OnClickListener {
        public c() {
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            TimePickerDialogWrap2.this.f291f = 0;
            TimePickerDialogWrap2.this.g = 0;
            TimePickerDialogWrap2.this.h = 0;
            TimePickerDialogWrap2.this.i = 0;
            TimePickerDialogWrap2.this.dismiss();
        }
    }

    /* loaded from: classes.dex */
    public interface d {
        void a(int i, int i2, int i3, int i4);
    }

    public TimePickerDialogWrap2(@NonNull d dVar) {
        this.f286a = dVar;
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: n */
    public /* synthetic */ void o(NumberPicker numberPicker, int i, int i2) {
        if (i2 == 24) {
            this.f288c.setMaxValue(0);
        } else {
            this.f288c.setMaxValue(59);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: p */
    public /* synthetic */ void q(NumberPicker numberPicker, int i, int i2) {
        if (i2 == 24) {
            this.f290e.setMaxValue(0);
        } else {
            this.f290e.setMaxValue(59);
        }
    }

    @Override // androidx.fragment.app.DialogFragment
    @NonNull
    public Dialog onCreateDialog(@Nullable Bundle bundle) {
        AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
        View inflate = requireActivity().getLayoutInflater().inflate(R.layout.time_select2, (ViewGroup) null);
        NumberPicker numberPicker = (NumberPicker) inflate.findViewById(R.id.startHour);
        this.f287b = numberPicker;
        numberPicker.setWrapSelectorWheel(false);
        this.f287b.setMinValue(0);
        this.f287b.setMaxValue(24);
        NumberPicker numberPicker2 = (NumberPicker) inflate.findViewById(R.id.endHour);
        this.f289d = numberPicker2;
        numberPicker2.setWrapSelectorWheel(false);
        this.f289d.setMinValue(0);
        this.f289d.setMaxValue(24);
        a aVar = new a();
        NumberPicker numberPicker3 = (NumberPicker) inflate.findViewById(R.id.startMin);
        this.f288c = numberPicker3;
        numberPicker3.setWrapSelectorWheel(false);
        this.f288c.setMinValue(0);
        this.f288c.setMaxValue(59);
        this.f288c.setFormatter(aVar);
        NumberPicker numberPicker4 = (NumberPicker) inflate.findViewById(R.id.endMin);
        this.f290e = numberPicker4;
        numberPicker4.setWrapSelectorWheel(false);
        this.f290e.setMinValue(0);
        this.f290e.setMaxValue(59);
        this.f290e.setFormatter(aVar);
        this.f287b.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() { // from class: d.h.a.e.a.k2
            @Override // android.widget.NumberPicker.OnValueChangeListener
            public final void onValueChange(NumberPicker numberPicker5, int i, int i2) {
                TimePickerDialogWrap2.this.o(numberPicker5, i, i2);
            }
        });
        this.f289d.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() { // from class: d.h.a.e.a.l2
            @Override // android.widget.NumberPicker.OnValueChangeListener
            public final void onValueChange(NumberPicker numberPicker5, int i, int i2) {
                TimePickerDialogWrap2.this.q(numberPicker5, i, i2);
            }
        });
        TextView textView = (TextView) inflate.findViewById(R.id.positiveBtn);
        textView.setClickable(true);
        textView.setOnClickListener(new b());
        TextView textView2 = (TextView) inflate.findViewById(R.id.negativeBtn);
        textView2.setClickable(true);
        textView2.setOnClickListener(new c());
        AlertDialog create = builder.setView(inflate).create();
        create.getWindow().setBackgroundDrawableResource(17170445);
        create.setCanceledOnTouchOutside(false);
        return create;
    }
}
