package com.sermatec.sunmate.ui.control;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.NumberPicker;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import com.sermatec.sunmate.R;

/* loaded from: classes.dex */
public class TimePickerDialogWrap3 extends DialogFragment {

    /* renamed from: a  reason: collision with root package name */
    public c f295a;

    /* renamed from: b  reason: collision with root package name */
    public NumberPicker f296b;

    /* renamed from: c  reason: collision with root package name */
    public NumberPicker f297c;

    /* loaded from: classes.dex */
    public class a implements View.OnClickListener {
        public a() {
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            TimePickerDialogWrap3.this.f295a.a(TimePickerDialogWrap3.this.f296b.getValue(), TimePickerDialogWrap3.this.f297c.getValue());
            TimePickerDialogWrap3.this.dismiss();
        }
    }

    /* loaded from: classes.dex */
    public class b implements View.OnClickListener {
        public b() {
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            TimePickerDialogWrap3.this.dismiss();
        }
    }

    /* loaded from: classes.dex */
    public interface c {
        void a(int i, int i2);
    }

    public TimePickerDialogWrap3(@NonNull c cVar) {
        this.f295a = cVar;
    }

    @Override // androidx.fragment.app.DialogFragment
    @NonNull
    public Dialog onCreateDialog(@Nullable Bundle bundle) {
        AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
        View inflate = requireActivity().getLayoutInflater().inflate(R.layout.time_select3, (ViewGroup) null);
        NumberPicker numberPicker = (NumberPicker) inflate.findViewById(R.id.date);
        this.f296b = numberPicker;
        numberPicker.setWrapSelectorWheel(false);
        this.f296b.setMinValue(0);
        this.f296b.setMaxValue(6);
        NumberPicker numberPicker2 = (NumberPicker) inflate.findViewById(R.id.effectTime);
        this.f297c = numberPicker2;
        numberPicker2.setWrapSelectorWheel(false);
        this.f297c.setMinValue(1);
        this.f297c.setMaxValue(2);
        String[] stringArray = requireContext().getResources().getStringArray(R.array.weekdays);
        String[] stringArray2 = requireContext().getResources().getStringArray(R.array.effect_mode);
        this.f296b.setDisplayedValues(stringArray);
        this.f297c.setDisplayedValues(stringArray2);
        TextView textView = (TextView) inflate.findViewById(R.id.positiveBtn);
        textView.setClickable(true);
        textView.setOnClickListener(new a());
        TextView textView2 = (TextView) inflate.findViewById(R.id.negativeBtn);
        textView2.setClickable(true);
        textView2.setOnClickListener(new b());
        AlertDialog create = builder.setView(inflate).create();
        create.getWindow().setBackgroundDrawableResource(17170445);
        create.setCanceledOnTouchOutside(false);
        return create;
    }
}
