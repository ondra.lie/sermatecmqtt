package com.sermatec.sunmate.ui.control;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.sermatec.sunmate.R;
import com.sermatec.sunmate.entity.TimeType;
import com.sermatec.sunmate.ui.control.TimeTypeAdapter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

/* loaded from: classes.dex */
public class TimeTypeAdapter extends BaseQuickAdapter<TimeType, BaseViewHolder> {
    public TimeTypeAdapter(List<TimeType> list) {
        super(R.layout.adapter_time_type, list);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: Y */
    public /* synthetic */ void Z(BaseViewHolder baseViewHolder, View view) {
        O(baseViewHolder.getAdapterPosition());
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: a0 */
    public /* synthetic */ void b0(BaseViewHolder baseViewHolder, View view) {
        if (A() != null) {
            A().a(this, view, baseViewHolder.getAdapterPosition());
        }
    }

    public int U() {
        ArrayList arrayList = new ArrayList(q());
        Collections.sort(arrayList);
        if (arrayList.size() <= 0) {
            return R.string.timeTypeLackError;
        }
        TimeType timeType = (TimeType) arrayList.get(0);
        if (timeType.getStartTime() != 0) {
            return R.string.timeTypeLackError;
        }
        for (int i = 1; i < arrayList.size(); i++) {
            if (timeType.getEndTime() != ((TimeType) arrayList.get(i)).getStartTime()) {
                return R.string.timeTypeLackError;
            }
            timeType = (TimeType) arrayList.get(i);
        }
        if (timeType.getEndTime() != 1440) {
            return R.string.timeTypeLackError;
        }
        return 0;
    }

    public int V(int i, int i2) {
        if (i >= i2) {
            return R.string.selectTimeError;
        }
        if (q().size() >= 16) {
            return R.string.timeTypeMaxError;
        }
        for (TimeType timeType : q()) {
            if (i < timeType.getEndTime() && i2 > timeType.getStartTime()) {
                return R.string.timeTypeCrossError;
            }
        }
        return 0;
    }

    /* renamed from: W */
    public void l(@NonNull final BaseViewHolder baseViewHolder, TimeType timeType) {
        if (timeType != null) {
            String[] stringArray = this.v.getResources().getStringArray(R.array.time_type);
            ((TextView) baseViewHolder.d(R.id.start_time)).setText(X(timeType.getStartTime()));
            ((TextView) baseViewHolder.d(R.id.end_time)).setText(X(timeType.getEndTime()));
            ((TextView) baseViewHolder.d(R.id.time_type)).setText(stringArray[timeType.getElectricityTypeValue() - 1]);
            baseViewHolder.d(R.id.delete).setOnClickListener(new View.OnClickListener() { // from class: d.h.a.e.a.o2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    TimeTypeAdapter.this.Z(baseViewHolder, view);
                }
            });
            View d2 = baseViewHolder.d(R.id.swipe_station);
            d2.setClickable(true);
            d2.setOnClickListener(new View.OnClickListener() { // from class: d.h.a.e.a.n2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    TimeTypeAdapter.this.b0(baseViewHolder, view);
                }
            });
        }
    }

    public final String X(int i) {
        return String.format(Locale.CHINA, "%02d:%02d", Integer.valueOf(i / 60), Integer.valueOf(i % 60));
    }
}
