package com.sermatec.sunmate.ui.system;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import com.sermatec.sunmate.MainActivity;
import com.sermatec.sunmate.R;
import d.h.a.e.e.d;
import java.util.Locale;

/* loaded from: classes.dex */
public class LanguageFragment extends Fragment {

    /* loaded from: classes.dex */
    public class a implements View.OnClickListener {
        public a() {
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            if (!"".equals(d.a(LanguageFragment.this.requireContext()))) {
                d.c(LanguageFragment.this.requireContext(), "");
                LanguageFragment.this.c();
            }
        }
    }

    /* loaded from: classes.dex */
    public class b implements View.OnClickListener {
        public b() {
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            if (!"zh_CN".equals(d.a(LanguageFragment.this.requireContext()))) {
                d.c(LanguageFragment.this.requireContext(), "zh_CN");
                LanguageFragment.this.c();
            }
        }
    }

    /* loaded from: classes.dex */
    public class c implements View.OnClickListener {
        public c() {
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            if (!"en".equals(d.a(LanguageFragment.this.requireContext()))) {
                d.c(LanguageFragment.this.requireContext(), "en");
                LanguageFragment.this.c();
            }
        }
    }

    public static void b(@NonNull Context context) {
        Locale locale;
        Configuration configuration = context.getResources().getConfiguration();
        String a2 = d.a(context);
        if ("zh_CN".equals(a2)) {
            locale = Locale.CHINA;
        } else if ("en".equals(a2)) {
            locale = Locale.ENGLISH;
        } else {
            locale = Locale.getDefault();
        }
        if (!locale.equals(configuration.locale)) {
            configuration.locale = locale;
            context.getResources().updateConfiguration(configuration, context.getResources().getDisplayMetrics());
        }
    }

    public final void c() {
        startActivity(new Intent(requireContext(), MainActivity.class));
        requireActivity().finish();
    }

    @Override // androidx.fragment.app.Fragment
    @Nullable
    public View onCreateView(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, @Nullable Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.language_switch, viewGroup, false);
        AppCompatTextView appCompatTextView = (AppCompatTextView) inflate.findViewById(R.id.language_ch);
        AppCompatTextView appCompatTextView2 = (AppCompatTextView) inflate.findViewById(R.id.language_en);
        AppCompatTextView appCompatTextView3 = (AppCompatTextView) inflate.findViewById(R.id.language_follow);
        String a2 = d.a(requireContext());
        if (a2.equals("")) {
            appCompatTextView3.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, getResources().getDrawable(R.drawable.ic_select), (Drawable) null);
        } else if (a2.equals("zh_CN")) {
            appCompatTextView.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, getResources().getDrawable(R.drawable.ic_select), (Drawable) null);
        } else if (a2.equals("en")) {
            appCompatTextView2.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, getResources().getDrawable(R.drawable.ic_select), (Drawable) null);
        }
        appCompatTextView3.setOnClickListener(new a());
        appCompatTextView.setOnClickListener(new b());
        appCompatTextView2.setOnClickListener(new c());
        return inflate;
    }
}
