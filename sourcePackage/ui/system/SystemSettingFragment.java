package com.sermatec.sunmate.ui.system;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import com.sermatec.sunmate.R;
import com.sermatec.sunmate.localControl.ConnectionManager;
import d.h.a.e.e.c;

/* loaded from: classes.dex */
public class SystemSettingFragment extends Fragment implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    public Drawable f349a;

    /* renamed from: b  reason: collision with root package name */
    public Dialog f350b;

    /* renamed from: c  reason: collision with root package name */
    public TextView f351c;

    /* renamed from: d  reason: collision with root package name */
    public Handler f352d = new Handler();

    /* renamed from: e  reason: collision with root package name */
    public BroadcastReceiver f353e = new a();

    /* loaded from: classes.dex */
    public class a extends BroadcastReceiver {
        public a() {
        }

        @Override // android.content.BroadcastReceiver
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("conn_ok")) {
                SystemSettingFragment.this.d(R.string.deviceConnectedSuccess);
                SystemSettingFragment.this.e(true, context);
            } else if (intent.getAction().equals("conn_no")) {
                SystemSettingFragment.this.e(false, context);
            } else if (intent.getAction().equals("conn_no_condition")) {
                SystemSettingFragment.this.d(R.string.connect_fail);
                SystemSettingFragment.this.e(false, context);
            } else if (intent.getAction().equals("conn_fail")) {
                SystemSettingFragment.this.d(R.string.connect_fault);
                SystemSettingFragment.this.e(false, context);
            }
        }
    }

    /* loaded from: classes.dex */
    public class b implements Runnable {
        public b() {
        }

        @Override // java.lang.Runnable
        public void run() {
            SystemSettingFragment.this.f350b.dismiss();
        }
    }

    public final void d(@StringRes int i) {
        TextView textView;
        Dialog dialog = this.f350b;
        if (dialog != null && (textView = (TextView) dialog.findViewById(R.id.tipMsg)) != null) {
            textView.setText(i);
            this.f350b.setCancelable(true);
            this.f352d.postDelayed(new b(), 1000L);
        }
    }

    public final void e(boolean z, Context context) {
        Drawable drawable = this.f349a;
        if (drawable != null && Build.VERSION.SDK_INT >= 21) {
            drawable.setTint(context.getResources().getColor(z ? R.color.colorPrimary : 17170454));
        }
        TextView textView = this.f351c;
        if (textView != null) {
            textView.setText(z ? R.string.connected : R.string.connect_title);
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.f350b = c.c(context, R.string.conn_device, R.string.deviceConnecting);
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        NavController findNavController = Navigation.findNavController(requireActivity(), R.id.left_nav_host_fragment);
        switch (view.getId()) {
            case R.id.about_us /* 2131296274 */:
                findNavController.navigate(R.id.sys_to_about_us);
                return;
            case R.id.connect /* 2131296431 */:
                if (ConnectionManager.e().c(false)) {
                    this.f350b.show();
                    ((TextView) this.f350b.findViewById(R.id.tipMsg)).setText(R.string.deviceConnecting);
                    this.f350b.setCancelable(false);
                    return;
                }
                return;
            case R.id.language_change /* 2131296613 */:
                findNavController.navigate(R.id.sys_to_language_change);
                return;
            case R.id.theme_setting /* 2131296900 */:
                findNavController.navigate(R.id.sys_to_theme_setting);
                return;
            default:
                return;
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("conn_ok");
        intentFilter.addAction("conn_no");
        intentFilter.addAction("conn_fail");
        intentFilter.addAction("conn_no_condition");
        requireActivity().registerReceiver(this.f353e, intentFilter);
    }

    @Override // androidx.fragment.app.Fragment
    @Nullable
    public View onCreateView(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, @Nullable Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.sys_setting, viewGroup, false);
        inflate.findViewById(R.id.language_change).setOnClickListener(this);
        inflate.findViewById(R.id.theme_setting).setOnClickListener(this);
        inflate.findViewById(R.id.about_us).setOnClickListener(this);
        TextView textView = (TextView) inflate.findViewById(R.id.connect);
        this.f351c = textView;
        textView.setOnClickListener(this);
        this.f349a = ((AppCompatImageView) inflate.findViewById(R.id.connectImg)).getDrawable();
        e(ConnectionManager.e().g(), requireContext());
        return inflate;
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        requireActivity().unregisterReceiver(this.f353e);
        super.onDestroy();
    }
}
