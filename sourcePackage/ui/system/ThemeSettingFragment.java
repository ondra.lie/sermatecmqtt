package com.sermatec.sunmate.ui.system;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import com.sermatec.sunmate.R;
import d.h.a.e.e.d;

/* loaded from: classes.dex */
public class ThemeSettingFragment extends Fragment implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    public AppCompatTextView f356a;

    /* renamed from: b  reason: collision with root package name */
    public AppCompatTextView f357b;

    /* renamed from: c  reason: collision with root package name */
    public AppCompatTextView f358c;

    public final void a(Context context, int i) {
        AppCompatDelegate.setDefaultNightMode(i);
        d.d(context, i);
        b(i);
    }

    public final void b(int i) {
        if (i == 1) {
            this.f357b.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, getResources().getDrawable(R.drawable.ic_select), (Drawable) null);
            this.f358c.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
            this.f356a.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
        } else if (i != 2) {
            this.f356a.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, getResources().getDrawable(R.drawable.ic_select), (Drawable) null);
            this.f357b.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
            this.f358c.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
        } else {
            this.f358c.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, getResources().getDrawable(R.drawable.ic_select), (Drawable) null);
            this.f357b.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
            this.f356a.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
        }
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.dark_mode) {
            a(view.getContext(), 2);
        } else if (id == R.id.follow_mode) {
            a(view.getContext(), -1);
        } else if (id == R.id.light_mode) {
            a(view.getContext(), 1);
        }
    }

    @Override // androidx.fragment.app.Fragment
    @Nullable
    public View onCreateView(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, @Nullable Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.theme_setting, viewGroup, false);
        AppCompatTextView appCompatTextView = (AppCompatTextView) inflate.findViewById(R.id.follow_mode);
        this.f356a = appCompatTextView;
        appCompatTextView.setOnClickListener(this);
        AppCompatTextView appCompatTextView2 = (AppCompatTextView) inflate.findViewById(R.id.light_mode);
        this.f357b = appCompatTextView2;
        appCompatTextView2.setOnClickListener(this);
        AppCompatTextView appCompatTextView3 = (AppCompatTextView) inflate.findViewById(R.id.dark_mode);
        this.f358c = appCompatTextView3;
        appCompatTextView3.setOnClickListener(this);
        b(d.b(requireContext()));
        return inflate;
    }
}
