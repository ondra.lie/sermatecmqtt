package com.sermatec.sunmate.ui.system;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import com.sermatec.sunmate.R;
import java.util.Calendar;

/* loaded from: classes.dex */
public class AboutUsFragment extends Fragment {
    @Override // androidx.fragment.app.Fragment
    @Nullable
    public View onCreateView(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, @Nullable Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.about_us, viewGroup, false);
        ((AppCompatTextView) inflate.findViewById(R.id.copyright)).setText(getString(R.string.copyright, Integer.valueOf(Calendar.getInstance().get(1))));
        ((AppCompatTextView) inflate.findViewById(R.id.version)).setText("Version 1.1.7");
        return inflate;
    }
}
