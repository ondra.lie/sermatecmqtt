package com.sermatec.sunmate.ui.home;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import com.sermatec.sunmate.R;
import com.sermatec.sunmate.databinding.FragmentHomeBinding;
import com.sermatec.sunmate.ui.MainPicture;
import com.sermatec.sunmate.ui.data.DataViewModel;
import com.youth.banner.Banner;
import com.youth.banner.adapter.BannerImageAdapter;
import com.youth.banner.holder.BannerImageHolder;
import com.youth.banner.indicator.CircleIndicator;
import java.util.Arrays;
import java.util.List;

/* loaded from: classes.dex */
public class HomeFragment extends Fragment {

    /* renamed from: a  reason: collision with root package name */
    public DataViewModel f314a;

    /* renamed from: b  reason: collision with root package name */
    public Banner f315b;

    /* renamed from: c  reason: collision with root package name */
    public Handler f316c = new Handler();

    /* renamed from: d  reason: collision with root package name */
    public MainPicture f317d;

    /* loaded from: classes.dex */
    public class a extends BannerImageAdapter<Integer> {
        public a(List list) {
            super(list);
        }

        /* renamed from: b */
        public void onBindView(BannerImageHolder bannerImageHolder, Integer num, int i, int i2) {
            bannerImageHolder.imageView.setImageResource(num.intValue());
        }
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(@NonNull LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        FragmentHomeBinding a2 = FragmentHomeBinding.a(layoutInflater, viewGroup, false);
        DataViewModel dataViewModel = (DataViewModel) ViewModelProviders.of(requireActivity()).get(DataViewModel.class);
        this.f314a = dataViewModel;
        a2.c(dataViewModel);
        View root = a2.getRoot();
        MainPicture mainPicture = (MainPicture) root.findViewById(R.id.mainPicture);
        this.f317d = mainPicture;
        mainPicture.n0(this.f314a);
        this.f315b = (Banner) root.findViewById(R.id.banner);
        this.f315b.addBannerLifecycleObserver(this).setAdapter(new a(Arrays.asList(Integer.valueOf((int) R.mipmap.banner1), Integer.valueOf((int) R.mipmap.banner2), Integer.valueOf((int) R.mipmap.banner3)))).setIndicator(new CircleIndicator(requireContext()));
        return root;
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroyView() {
        this.f317d.s0();
        super.onDestroyView();
    }
}
