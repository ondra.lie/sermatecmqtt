package com.sermatec.sunmate.ui.home;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

/* loaded from: classes.dex */
public class HomeViewModel extends ViewModel {

    /* renamed from: a  reason: collision with root package name */
    public MutableLiveData<String> f319a;

    public HomeViewModel() {
        MutableLiveData<String> mutableLiveData = new MutableLiveData<>();
        this.f319a = mutableLiveData;
        mutableLiveData.setValue("This is home fragment");
    }
}
