package com.sermatec.sunmate.ui.dashboard;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

/* loaded from: classes.dex */
public class DashboardViewModel extends ViewModel {

    /* renamed from: a  reason: collision with root package name */
    public MutableLiveData<String> f303a;

    public DashboardViewModel() {
        MutableLiveData<String> mutableLiveData = new MutableLiveData<>();
        this.f303a = mutableLiveData;
        mutableLiveData.setValue("This is dashboard fragment");
    }

    public LiveData<String> a() {
        return this.f303a;
    }
}
