package com.sermatec.sunmate.ui.dashboard;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import com.sermatec.sunmate.R;

/* loaded from: classes.dex */
public class DashboardFragment extends Fragment {

    /* renamed from: a  reason: collision with root package name */
    public DashboardViewModel f300a;

    /* loaded from: classes.dex */
    public class a implements Observer<String> {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ TextView f301a;

        public a(TextView textView) {
            this.f301a = textView;
        }

        /* renamed from: a */
        public void onChanged(@Nullable String str) {
            this.f301a.setText(str);
        }
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(@NonNull LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.f300a = (DashboardViewModel) ViewModelProviders.of(this).get(DashboardViewModel.class);
        View inflate = layoutInflater.inflate(R.layout.fragment_dashboard, viewGroup, false);
        this.f300a.a().observe(this, new a((TextView) inflate.findViewById(R.id.text_dashboard)));
        return inflate;
    }
}
