package com.sermatec.sunmate.ui.report;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.google.android.material.tabs.TabLayout;
import com.sermatec.sunmate.R;
import com.sermatec.sunmate.ui.data.DataViewModel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/* loaded from: classes.dex */
public class ReportFragment extends Fragment {

    /* renamed from: a  reason: collision with root package name */
    public DataViewModel f329a;

    /* loaded from: classes.dex */
    public static class ChartFragment extends Fragment implements View.OnClickListener {

        /* renamed from: a  reason: collision with root package name */
        public DataViewModel f330a;

        /* renamed from: b  reason: collision with root package name */
        public int f331b;
        @BindView
        public BarChart chart;
        @BindView
        public View data_view;
        @BindView
        public RecyclerView data_view_container;
        @BindView
        public TextView day;
        @BindView
        public TextView history;
        @BindView
        public TextView month;
        @BindView
        public AppCompatImageView switch_btn;
        @BindView
        public TextView year;

        /* loaded from: classes.dex */
        public class a extends d.d.a.a.f.c {
            public a() {
            }

            @Override // d.d.a.a.f.c
            public String d(float f2) {
                int i = (int) f2;
                return String.format(Locale.CHINA, "%02d:%02d", Integer.valueOf(i / 60), Integer.valueOf(i % 60));
            }
        }

        /* loaded from: classes.dex */
        public class b extends d.d.a.a.f.c {
            public b() {
            }

            @Override // d.d.a.a.f.c
            public String d(float f2) {
                return String.valueOf((int) f2);
            }
        }

        /* loaded from: classes.dex */
        public class c extends d.d.a.a.f.c {
            public c() {
            }

            @Override // d.d.a.a.f.c
            public String d(float f2) {
                return String.valueOf((int) f2);
            }
        }

        /* loaded from: classes.dex */
        public class d extends d.d.a.a.f.c {
            public d() {
            }

            @Override // d.d.a.a.f.c
            public String d(float f2) {
                return String.valueOf((int) f2);
            }
        }

        public ChartFragment() {
        }

        public final void a(List<d.d.a.a.e.c> list, int i) {
            c[] cVarArr = new c[list.size()];
            for (int i2 = 0; i2 < list.size(); i2++) {
                d.d.a.a.e.c cVar = list.get(i2);
                if (i != R.id.day) {
                    cVarArr[i2] = new c(String.valueOf(i2 + 1), String.valueOf((int) cVar.f()), String.valueOf(list.get(i2).c()));
                } else {
                    cVarArr[i2] = new c(String.valueOf(i2 + 1), String.format(Locale.CHINA, "%02d:%02d", Integer.valueOf(((int) cVar.f()) / 60), Integer.valueOf(((int) cVar.f()) % 60)), String.valueOf(list.get(i2).c()));
                }
            }
            b bVar = (b) this.data_view_container.getAdapter();
            if (bVar != null) {
                bVar.submitList(Arrays.asList(cVarArr));
            }
        }

        public final void b() {
            this.chart.setBackgroundColor(getResources().getColor(R.color.background));
            this.chart.getDescription().g(false);
            this.chart.setTouchEnabled(true);
            this.chart.setDragEnabled(true);
            this.chart.setScaleEnabled(true);
            this.chart.setPinchZoom(true);
            XAxis xAxis = this.chart.getXAxis();
            xAxis.H(false);
            xAxis.D(1.5f);
            int color = getResources().getColor(R.color.textColor);
            xAxis.h(color);
            YAxis axisLeft = this.chart.getAxisLeft();
            int i = this.f331b;
            if (i == 0 || i == 2) {
                axisLeft.F(0.0f);
            }
            axisLeft.G(false);
            axisLeft.H(true);
            axisLeft.h(color);
            xAxis.O(XAxis.XAxisPosition.BOTTOM);
            this.chart.getAxisRight().g(false);
            this.chart.f(500);
            this.chart.getLegend().h(color);
            this.chart.getLegend().G(Legend.LegendHorizontalAlignment.CENTER);
            this.chart.getLegend().H(Legend.LegendVerticalAlignment.TOP);
            this.chart.setNoDataText(getString(R.string.no_data));
        }

        public final void c(float[] fArr, float[] fArr2, int i, @StringRes int i2, @StringRes int i3, @StringRes int i4, boolean z) {
            if (fArr == null || fArr2 == null) {
                this.chart.setData(null);
            } else {
                ArrayList arrayList = new ArrayList();
                for (int i5 = 0; i5 < fArr.length; i5++) {
                    arrayList.add(new d.d.a.a.e.c(fArr[i5], fArr2[i5]));
                }
                a(arrayList, i);
                d.d.a.a.e.b bVar = new d.d.a.a.e.b(arrayList, getString(i2));
                bVar.S(getResources().getColor(R.color.colorPrimary));
                bVar.T(false);
                this.chart.setData(new d.d.a.a.e.a(bVar));
                d.h.a.e.d.a aVar = new d.h.a.e.d.a(requireContext(), i3, R.color.colorPrimary, i4, z);
                this.chart.setMarker(aVar);
                aVar.setChartView(this.chart);
            }
            this.chart.invalidate();
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            int i;
            int i2;
            float[] fArr;
            int i3;
            int i4;
            float[] fArr2;
            int i5;
            int i6;
            float[] fArr3;
            int i7;
            int i8;
            float[] fArr4;
            XAxis xAxis = this.chart.getXAxis();
            switch (view.getId()) {
                case R.id.day /* 2131296462 */:
                    xAxis.K(new a());
                    xAxis.E(1440.0f);
                    xAxis.F(0.0f);
                    float[] fArr5 = new float[48];
                    for (int i9 = 0; i9 < 48; i9++) {
                        fArr5[i9] = i9 * 30;
                    }
                    int i10 = this.f331b;
                    if (i10 == 0) {
                        fArr = this.f330a.r0.get();
                        i2 = R.string.pv_day_ennergy;
                        i = R.string.pv_day_ennergy_placeholder;
                    } else if (i10 == 1) {
                        fArr = this.f330a.v0.get();
                        i2 = R.string.grid_day_ennergy;
                        i = R.string.grid_day_ennergy_placeholder;
                    } else if (i10 != 2) {
                        fArr = null;
                        i2 = 0;
                        i = 0;
                    } else {
                        fArr = this.f330a.z0.get();
                        i2 = R.string.load_day_ennergy;
                        i = R.string.load_day_ennergy_placeholder;
                    }
                    c(fArr5, fArr, R.id.day, i2, i, R.string.day_title_placeholder, true);
                    return;
                case R.id.history /* 2131296581 */:
                    xAxis.K(new d());
                    float[] fArr6 = new float[5];
                    int i11 = (((Calendar.getInstance().get(1) - 2018) / 5) * 5) + 2018;
                    for (int i12 = 0; i12 < 5; i12++) {
                        fArr6[i12] = i11 + i12;
                    }
                    xAxis.F(i11);
                    xAxis.E(i11 + 5);
                    int i13 = this.f331b;
                    if (i13 == 0) {
                        fArr2 = this.f330a.u0.get();
                        i4 = R.string.pv_history_ennergy;
                        i3 = R.string.pv_history_ennergy_placeholder;
                    } else if (i13 == 1) {
                        fArr2 = this.f330a.y0.get();
                        i4 = R.string.grid_history_ennergy;
                        i3 = R.string.grid_history_ennergy_placeholder;
                    } else if (i13 != 2) {
                        fArr2 = null;
                        i4 = 0;
                        i3 = 0;
                    } else {
                        fArr2 = this.f330a.C0.get();
                        i4 = R.string.load_history_ennergy;
                        i3 = R.string.load_history_ennergy_placeholder;
                    }
                    c(fArr6, fArr2, R.id.history, i4, i3, R.string.history_title_placeholder, false);
                    return;
                case R.id.month /* 2131296681 */:
                    xAxis.K(new b());
                    xAxis.E(31.0f);
                    xAxis.F(0.0f);
                    float[] fArr7 = new float[31];
                    int i14 = 0;
                    while (i14 < 31) {
                        int i15 = i14 + 1;
                        fArr7[i14] = i15;
                        i14 = i15;
                    }
                    int i16 = this.f331b;
                    if (i16 == 0) {
                        fArr3 = this.f330a.s0.get();
                        i6 = R.string.pv_month_ennergy;
                        i5 = R.string.pv_month_ennergy_placeholder;
                    } else if (i16 == 1) {
                        fArr3 = this.f330a.w0.get();
                        i6 = R.string.grid_month_ennergy;
                        i5 = R.string.grid_month_ennergy_placeholder;
                    } else if (i16 != 2) {
                        fArr3 = null;
                        i6 = 0;
                        i5 = 0;
                    } else {
                        fArr3 = this.f330a.A0.get();
                        i6 = R.string.load_month_ennergy;
                        i5 = R.string.load_month_ennergy_placeholder;
                    }
                    c(fArr7, fArr3, R.id.month, i6, i5, R.string.month_title_placeholder, false);
                    return;
                case R.id.year /* 2131296961 */:
                    xAxis.K(new c());
                    xAxis.E(12.0f);
                    xAxis.F(0.0f);
                    float[] fArr8 = new float[12];
                    int i17 = 0;
                    while (i17 < 12) {
                        int i18 = i17 + 1;
                        fArr8[i17] = i18;
                        i17 = i18;
                    }
                    int i19 = this.f331b;
                    if (i19 == 0) {
                        fArr4 = this.f330a.t0.get();
                        i8 = R.string.pv_year_ennergy;
                        i7 = R.string.pv_year_ennergy_placeholder;
                    } else if (i19 == 1) {
                        fArr4 = this.f330a.x0.get();
                        i8 = R.string.grid_year_ennergy;
                        i7 = R.string.grid_year_ennergy_placeholder;
                    } else if (i19 != 2) {
                        fArr4 = null;
                        i8 = 0;
                        i7 = 0;
                    } else {
                        fArr4 = this.f330a.B0.get();
                        i8 = R.string.load_year_ennergy;
                        i7 = R.string.load_year_ennergy_placeholder;
                    }
                    c(fArr8, fArr4, R.id.year, i8, i7, R.string.year_title_placeholder, false);
                    return;
                default:
                    return;
            }
        }

        @Override // androidx.fragment.app.Fragment
        public void onCreate(@Nullable Bundle bundle) {
            super.onCreate(bundle);
            if (bundle != null) {
                this.f331b = bundle.getInt("position");
            }
            this.f330a = (DataViewModel) new ViewModelProvider(requireActivity()).get(DataViewModel.class);
        }

        @Override // androidx.fragment.app.Fragment
        @Nullable
        public View onCreateView(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, @Nullable Bundle bundle) {
            View inflate = layoutInflater.inflate(R.layout.chart_view, viewGroup, false);
            ButterKnife.a(this, inflate);
            this.switch_btn.setOnClickListener(new d(this.chart, this.data_view, null));
            this.day.setOnClickListener(this);
            this.month.setOnClickListener(this);
            this.year.setOnClickListener(this);
            this.history.setOnClickListener(this);
            b();
            this.data_view_container.setLayoutManager(new LinearLayoutManager(requireContext()));
            this.data_view_container.setAdapter(new b(null));
            return inflate;
        }

        @Override // androidx.fragment.app.Fragment
        public void onResume() {
            super.onResume();
            onClick(this.day);
        }

        @Override // androidx.fragment.app.Fragment
        public void onSaveInstanceState(@NonNull Bundle bundle) {
            super.onSaveInstanceState(bundle);
            bundle.putInt("position", this.f331b);
        }

        public ChartFragment(int i) {
            this.f331b = i;
        }
    }

    /* loaded from: classes.dex */
    public class ChartFragment_ViewBinding implements Unbinder {

        /* renamed from: b  reason: collision with root package name */
        public ChartFragment f336b;

        @UiThread
        public ChartFragment_ViewBinding(ChartFragment chartFragment, View view) {
            this.f336b = chartFragment;
            chartFragment.day = (TextView) c.b.a.c(view, R.id.day, "field 'day'", TextView.class);
            chartFragment.month = (TextView) c.b.a.c(view, R.id.month, "field 'month'", TextView.class);
            chartFragment.year = (TextView) c.b.a.c(view, R.id.year, "field 'year'", TextView.class);
            chartFragment.history = (TextView) c.b.a.c(view, R.id.history, "field 'history'", TextView.class);
            chartFragment.switch_btn = (AppCompatImageView) c.b.a.c(view, R.id.switch_btn, "field 'switch_btn'", AppCompatImageView.class);
            chartFragment.data_view = c.b.a.b(view, R.id.data_view, "field 'data_view'");
            chartFragment.chart = (BarChart) c.b.a.c(view, R.id.chart, "field 'chart'", BarChart.class);
            chartFragment.data_view_container = (RecyclerView) c.b.a.c(view, R.id.data_view_container, "field 'data_view_container'", RecyclerView.class);
        }
    }

    /* loaded from: classes.dex */
    public class a extends FragmentPagerAdapter {

        /* renamed from: a  reason: collision with root package name */
        public final /* synthetic */ String[] f337a;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(FragmentManager fragmentManager, String[] strArr) {
            super(fragmentManager);
            this.f337a = strArr;
        }

        @Override // androidx.viewpager.widget.PagerAdapter
        public int getCount() {
            return this.f337a.length;
        }

        @Override // androidx.fragment.app.FragmentPagerAdapter
        @NonNull
        public Fragment getItem(int i) {
            return new ChartFragment(i);
        }

        @Override // androidx.viewpager.widget.PagerAdapter
        public CharSequence getPageTitle(int i) {
            return this.f337a[i];
        }
    }

    /* loaded from: classes.dex */
    public static class b extends ListAdapter<c, C0009b> {

        /* loaded from: classes.dex */
        public class a extends DiffUtil.ItemCallback<c> {
            /* renamed from: a */
            public boolean areContentsTheSame(@NonNull c cVar, @NonNull c cVar2) {
                return false;
            }

            /* renamed from: b */
            public boolean areItemsTheSame(@NonNull c cVar, @NonNull c cVar2) {
                return false;
            }
        }

        /* renamed from: com.sermatec.sunmate.ui.report.ReportFragment$b$b  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public class C0009b extends RecyclerView.ViewHolder {
            public /* synthetic */ C0009b(b bVar, View view, a aVar) {
                this(view);
            }

            public C0009b(@NonNull View view) {
                super(view);
            }
        }

        public /* synthetic */ b(a aVar) {
            this();
        }

        /* renamed from: a */
        public void onBindViewHolder(@NonNull C0009b bVar, int i) {
            c item = getItem(i);
            ViewGroup viewGroup = (ViewGroup) bVar.itemView;
            if (item != null) {
                ((AppCompatTextView) viewGroup.findViewById(R.id.order)).setText(item.f340a);
                ((AppCompatTextView) viewGroup.findViewById(R.id.time)).setText(item.f341b);
                ((AppCompatTextView) viewGroup.findViewById(R.id.value)).setText(item.f342c);
            }
        }

        @NonNull
        /* renamed from: b */
        public C0009b onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new C0009b(this, LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.single_value, viewGroup, false), null);
        }

        public b() {
            super(new a());
        }
    }

    /* loaded from: classes.dex */
    public static class c {

        /* renamed from: a  reason: collision with root package name */
        public String f340a;

        /* renamed from: b  reason: collision with root package name */
        public String f341b;

        /* renamed from: c  reason: collision with root package name */
        public String f342c;

        public c(String str, String str2, String str3) {
            this.f340a = str;
            this.f341b = str2;
            this.f342c = str3;
        }
    }

    /* loaded from: classes.dex */
    public static class d implements View.OnClickListener {

        /* renamed from: a  reason: collision with root package name */
        public int f343a;

        /* renamed from: b  reason: collision with root package name */
        public View f344b;

        /* renamed from: c  reason: collision with root package name */
        public View f345c;

        public /* synthetic */ d(View view, View view2, a aVar) {
            this(view, view2);
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            if (this.f343a == -1) {
                this.f343a = this.f344b.getMeasuredHeight();
                this.f345c.getLayoutParams().height = this.f343a;
            }
            if (this.f344b.getVisibility() == 0) {
                this.f344b.setVisibility(8);
                this.f345c.setVisibility(0);
                return;
            }
            this.f345c.setVisibility(8);
            this.f344b.setVisibility(0);
        }

        public d(View view, View view2) {
            this.f343a = -1;
            this.f344b = view;
            this.f345c = view2;
        }
    }

    @Override // androidx.fragment.app.Fragment
    @Nullable
    public View onCreateView(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, @Nullable Bundle bundle) {
        this.f329a = (DataViewModel) new ViewModelProvider(requireActivity()).get(DataViewModel.class);
        View inflate = layoutInflater.inflate(R.layout.fragment_report, viewGroup, false);
        ViewPager viewPager = (ViewPager) inflate.findViewById(R.id.view_pager);
        viewPager.setAdapter(new a(getChildFragmentManager(), new String[]{getString(R.string.pv_report), getString(R.string.grid_report), getString(R.string.load_report)}));
        ((TabLayout) inflate.findViewById(R.id.tabs)).setupWithViewPager(viewPager);
        return inflate;
    }
}
