package com.sermatec.sunmate.entity;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes.dex */
public class CdSetting implements Serializable {
    private int fcFriEnable;
    private int fcMonEnable;
    private int fcOpen;
    private int fcSatEnable;
    private String fcSocLimit;
    private int fcSunEnable;
    private int fcThuEnable;
    private String fcTimeGap;
    private int fcTimeRangeLen;
    private int fcTusEnable;
    private int fcWenEnable;
    private int fdFriEnable;
    private int fdMonEnable;
    private int fdOpen;
    private int fdSatEnable;
    private String fdSocLimit;
    private int fdSunEnable;
    private int fdThuEnable;
    private String fdTimeGap;
    private int fdTimeRangeLen;
    private int fdTusEnable;
    private int fdWenEnable;

    public int getFcFriEnable() {
        return this.fcFriEnable;
    }

    public int getFcMonEnable() {
        return this.fcMonEnable;
    }

    public int getFcOpen() {
        return this.fcOpen;
    }

    public int getFcSatEnable() {
        return this.fcSatEnable;
    }

    public String getFcSocLimit() {
        return this.fcSocLimit;
    }

    public int getFcSunEnable() {
        return this.fcSunEnable;
    }

    public int getFcThuEnable() {
        return this.fcThuEnable;
    }

    public String getFcTimeGap() {
        return this.fcTimeGap;
    }

    public int getFcTimeRangeLen() {
        return this.fcTimeRangeLen;
    }

    public List<TimeRange> getFcTimeRanges() {
        ArrayList arrayList = new ArrayList();
        try {
            JSONArray parseArray = JSON.parseArray(this.fcTimeGap);
            for (int i = 0; i < this.fcTimeRangeLen; i++) {
                JSONObject jSONObject = parseArray.getJSONObject(i);
                String[] split = jSONObject.getString("startTime").split(":");
                String[] split2 = jSONObject.getString("endTime").split(":");
                arrayList.add(new TimeRange((Integer.parseInt(split[0]) * 60) + Integer.parseInt(split[1]), (Integer.parseInt(split2[0]) * 60) + Integer.parseInt(split2[1])));
            }
        } catch (Exception unused) {
        }
        return arrayList;
    }

    public int getFcTusEnable() {
        return this.fcTusEnable;
    }

    public int getFcWenEnable() {
        return this.fcWenEnable;
    }

    public int getFdFriEnable() {
        return this.fdFriEnable;
    }

    public int getFdMonEnable() {
        return this.fdMonEnable;
    }

    public int getFdOpen() {
        return this.fdOpen;
    }

    public int getFdSatEnable() {
        return this.fdSatEnable;
    }

    public String getFdSocLimit() {
        return this.fdSocLimit;
    }

    public int getFdSunEnable() {
        return this.fdSunEnable;
    }

    public int getFdThuEnable() {
        return this.fdThuEnable;
    }

    public String getFdTimeGap() {
        return this.fdTimeGap;
    }

    public int getFdTimeRangeLen() {
        return this.fdTimeRangeLen;
    }

    public List<TimeRange> getFdTimeRanges() {
        ArrayList arrayList = new ArrayList();
        try {
            JSONArray parseArray = JSON.parseArray(this.fdTimeGap);
            for (int i = 0; i < this.fdTimeRangeLen; i++) {
                JSONObject jSONObject = parseArray.getJSONObject(i);
                String[] split = jSONObject.getString("startTime").split(":");
                String[] split2 = jSONObject.getString("endTime").split(":");
                arrayList.add(new TimeRange((Integer.parseInt(split[0]) * 60) + Integer.parseInt(split[1]), (Integer.parseInt(split2[0]) * 60) + Integer.parseInt(split2[1])));
            }
        } catch (Exception unused) {
        }
        return arrayList;
    }

    public int getFdTusEnable() {
        return this.fdTusEnable;
    }

    public int getFdWenEnable() {
        return this.fdWenEnable;
    }

    public void setFcFriEnable(int i) {
        this.fcFriEnable = i;
    }

    public void setFcMonEnable(int i) {
        this.fcMonEnable = i;
    }

    public void setFcOpen(int i) {
        this.fcOpen = i;
    }

    public void setFcSatEnable(int i) {
        this.fcSatEnable = i;
    }

    public void setFcSocLimit(String str) {
        this.fcSocLimit = str;
    }

    public void setFcSunEnable(int i) {
        this.fcSunEnable = i;
    }

    public void setFcThuEnable(int i) {
        this.fcThuEnable = i;
    }

    public void setFcTimeGap(String str) {
        this.fcTimeGap = str;
    }

    public void setFcTimeRangeLen(int i) {
        this.fcTimeRangeLen = i;
    }

    public void setFcTusEnable(int i) {
        this.fcTusEnable = i;
    }

    public void setFcWenEnable(int i) {
        this.fcWenEnable = i;
    }

    public void setFdFriEnable(int i) {
        this.fdFriEnable = i;
    }

    public void setFdMonEnable(int i) {
        this.fdMonEnable = i;
    }

    public void setFdOpen(int i) {
        this.fdOpen = i;
    }

    public void setFdSatEnable(int i) {
        this.fdSatEnable = i;
    }

    public void setFdSocLimit(String str) {
        this.fdSocLimit = str;
    }

    public void setFdSunEnable(int i) {
        this.fdSunEnable = i;
    }

    public void setFdThuEnable(int i) {
        this.fdThuEnable = i;
    }

    public void setFdTimeGap(String str) {
        this.fdTimeGap = str;
    }

    public void setFdTimeRangeLen(int i) {
        this.fdTimeRangeLen = i;
    }

    public void setFdTusEnable(int i) {
        this.fdTusEnable = i;
    }

    public void setFdWenEnable(int i) {
        this.fdWenEnable = i;
    }
}
