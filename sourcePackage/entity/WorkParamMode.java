package com.sermatec.sunmate.entity;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes.dex */
public class WorkParamMode implements Serializable {
    private int adjustMethod;
    private String ah;
    private int batteryCharge;
    private int batteryProtocol;
    private String chargePower;
    private String con;
    private int dcBatteryType;
    private int elecCode;
    private String elecPrice;
    private int gridSwitch;
    private int meterProtocol;
    private String price1;
    private String price2;
    private String price3;
    private String price4;
    private int refluxs;
    private String soc;
    private int style;

    public int getAdjustMethod() {
        return this.adjustMethod;
    }

    public String getAh() {
        return this.ah;
    }

    public int getBatteryCharge() {
        return this.batteryCharge;
    }

    public int getBatteryProtocol() {
        return this.batteryProtocol;
    }

    public String getChargePower() {
        return this.chargePower;
    }

    public String getCon() {
        return this.con;
    }

    public int getDcBatteryType() {
        return this.dcBatteryType;
    }

    public int getElecCode() {
        return this.elecCode;
    }

    public String getElecPrice() {
        return this.elecPrice;
    }

    public int getGridSwitch() {
        return this.gridSwitch;
    }

    public int getMeterProtocol() {
        return this.meterProtocol;
    }

    public String getPrice1() {
        return this.price1;
    }

    public String getPrice2() {
        return this.price2;
    }

    public String getPrice3() {
        return this.price3;
    }

    public String getPrice4() {
        return this.price4;
    }

    public int getRefluxs() {
        return this.refluxs;
    }

    public String getSoc() {
        return this.soc;
    }

    public int getStyle() {
        return this.style;
    }

    public List<TimeType> getTimeType() {
        ArrayList arrayList = new ArrayList();
        String str = this.elecPrice;
        if (str != null) {
            JSONArray parseArray = JSON.parseArray(str);
            for (int i = 0; i < parseArray.size(); i++) {
                try {
                    JSONObject jSONObject = parseArray.getJSONObject(i);
                    String[] split = jSONObject.getString("startTime").split(":");
                    int parseInt = (Integer.parseInt(split[0]) * 60) + Integer.parseInt(split[1]);
                    String[] split2 = jSONObject.getString("endTime").split(":");
                    int parseInt2 = (Integer.parseInt(split2[0]) * 60) + Integer.parseInt(split2[1]);
                    int intValue = jSONObject.getIntValue("electricityTypeValue");
                    if (intValue > 0 && intValue < 5) {
                        arrayList.add(new TimeType(parseInt, parseInt2, intValue));
                    }
                } catch (Exception unused) {
                }
            }
        }
        return arrayList;
    }

    public void setAdjustMethod(int i) {
        this.adjustMethod = i;
    }

    public void setAh(String str) {
        this.ah = str;
    }

    public void setBatteryCharge(int i) {
        this.batteryCharge = i;
    }

    public void setBatteryProtocol(int i) {
        this.batteryProtocol = i;
    }

    public void setChargePower(String str) {
        this.chargePower = str;
    }

    public void setCon(String str) {
        this.con = str;
    }

    public void setDcBatteryType(int i) {
        this.dcBatteryType = i;
    }

    public void setElecCode(int i) {
        this.elecCode = i;
    }

    public void setElecPrice(String str) {
        this.elecPrice = str;
    }

    public void setGridSwitch(int i) {
        this.gridSwitch = i;
    }

    public void setMeterProtocol(int i) {
        this.meterProtocol = i;
    }

    public void setPrice1(String str) {
        this.price1 = str;
    }

    public void setPrice2(String str) {
        this.price2 = str;
    }

    public void setPrice3(String str) {
        this.price3 = str;
    }

    public void setPrice4(String str) {
        this.price4 = str;
    }

    public void setRefluxs(int i) {
        this.refluxs = i;
    }

    public void setSoc(String str) {
        this.soc = str;
    }

    public void setStyle(int i) {
        this.style = i;
    }
}
