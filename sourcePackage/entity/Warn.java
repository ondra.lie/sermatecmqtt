package com.sermatec.sunmate.entity;

import android.os.Parcel;
import android.os.Parcelable;
import io.netty.util.internal.logging.MessageFormatter;
import java.io.Serializable;

/* loaded from: classes.dex */
public class Warn implements Parcelable, Serializable {
    public static final Parcelable.Creator<Warn> CREATOR = new a();
    private String desc;
    private String deviceName;
    private int prior;
    private String signalName;

    /* loaded from: classes.dex */
    public class a implements Parcelable.Creator<Warn> {
        /* renamed from: a */
        public Warn createFromParcel(Parcel parcel) {
            return new Warn(parcel);
        }

        /* renamed from: b */
        public Warn[] newArray(int i) {
            return new Warn[i];
        }
    }

    public Warn() {
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public String getDesc() {
        return this.desc;
    }

    public String getDeviceName() {
        return this.deviceName;
    }

    public int getPrior() {
        return this.prior;
    }

    public String getSignalName() {
        return this.signalName;
    }

    public void setDesc(String str) {
        this.desc = str;
    }

    public void setDeviceName(String str) {
        this.deviceName = str;
    }

    public void setPrior(int i) {
        this.prior = i;
    }

    public void setSignalName(String str) {
        this.signalName = str;
    }

    public String toString() {
        return "Warn{deviceName='" + this.deviceName + "', signalName='" + this.signalName + "', desc='" + this.desc + "', prior=" + this.prior + MessageFormatter.DELIM_STOP;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.deviceName);
        parcel.writeString(this.signalName);
        parcel.writeString(this.desc);
        parcel.writeInt(this.prior);
    }

    public Warn(Parcel parcel) {
        this.deviceName = parcel.readString();
        this.signalName = parcel.readString();
        this.desc = parcel.readString();
        this.prior = parcel.readInt();
    }
}
