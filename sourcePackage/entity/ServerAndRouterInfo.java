package com.sermatec.sunmate.entity;

import java.io.Serializable;

/* loaded from: classes.dex */
public class ServerAndRouterInfo implements Serializable {
    private String ip;
    private String password;
    private String port;
    private String ssid;
    private int style;

    public String getIp() {
        return this.ip;
    }

    public String getPassword() {
        return this.password;
    }

    public String getPort() {
        return this.port;
    }

    public String getSsid() {
        return this.ssid;
    }

    public int getStyle() {
        return this.style;
    }

    public void setIp(String str) {
        this.ip = str;
    }

    public void setPassword(String str) {
        this.password = str;
    }

    public void setPort(String str) {
        this.port = str;
    }

    public void setSsid(String str) {
        this.ssid = str;
    }

    public void setStyle(int i) {
        this.style = i;
    }
}
