package com.sermatec.sunmate.entity;

import b.a.a.a.a;
import java.io.Serializable;

/* loaded from: classes.dex */
public class EffectDate implements Comparable<EffectDate>, Serializable {
    private int effectMode;
    private int weekday;

    public EffectDate(int i, int i2) {
        this.weekday = i;
        this.effectMode = i2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return obj != null && getClass() == obj.getClass() && this.weekday == ((EffectDate) obj).weekday;
    }

    public int getEffectMode() {
        return this.effectMode;
    }

    public int getWeekday() {
        return this.weekday;
    }

    public int hashCode() {
        return this.weekday;
    }

    public void setEffectMode(int i) {
        this.effectMode = i;
    }

    public void setWeekday(int i) {
        this.weekday = i;
    }

    public int compareTo(EffectDate effectDate) {
        return a.a(this.weekday, effectDate.weekday);
    }

    public EffectDate() {
    }
}
