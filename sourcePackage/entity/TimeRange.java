package com.sermatec.sunmate.entity;

import b.a.a.a.a;
import com.alibaba.fastjson.JSONObject;
import java.io.Serializable;

/* loaded from: classes.dex */
public class TimeRange implements Comparable<TimeRange>, Serializable {
    private int endTime;
    private int startTime;

    public TimeRange() {
    }

    public int getEndTime() {
        return this.endTime;
    }

    public int getStartTime() {
        return this.startTime;
    }

    public void setEndTime(int i) {
        this.endTime = i;
    }

    public void setStartTime(int i) {
        this.startTime = i;
    }

    public JSONObject toJSONObject() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("startTime", (Object) ((this.startTime / 60) + ":" + (this.startTime % 60)));
        jSONObject.put("endTime", (Object) ((this.endTime / 60) + ":" + (this.endTime % 60)));
        return jSONObject;
    }

    public TimeRange(int i, int i2) {
        this.startTime = i;
        this.endTime = i2;
    }

    public int compareTo(TimeRange timeRange) {
        return a.a(this.startTime, timeRange.startTime);
    }
}
