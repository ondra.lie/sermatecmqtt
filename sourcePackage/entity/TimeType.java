package com.sermatec.sunmate.entity;

import b.a.a.a.a;
import com.alibaba.fastjson.JSONObject;
import java.io.Serializable;

/* loaded from: classes.dex */
public class TimeType implements Comparable<TimeType>, Serializable {
    private int electricityTypeValue;
    private int endTime;
    private int startTime;

    public TimeType() {
    }

    public int getElectricityTypeValue() {
        return this.electricityTypeValue;
    }

    public int getEndTime() {
        return this.endTime;
    }

    public int getStartTime() {
        return this.startTime;
    }

    public void setElectricityTypeValue(int i) {
        this.electricityTypeValue = i;
    }

    public void setEndTime(int i) {
        this.endTime = i;
    }

    public void setStartTime(int i) {
        this.startTime = i;
    }

    public JSONObject toJSONObject() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("startTime", (Object) ((this.startTime / 60) + ":" + (this.startTime % 60)));
        jSONObject.put("endTime", (Object) ((this.endTime / 60) + ":" + (this.endTime % 60)));
        jSONObject.put("electricityTypeValue", (Object) String.valueOf(this.electricityTypeValue));
        return jSONObject;
    }

    public TimeType(int i, int i2, int i3) {
        this.startTime = i;
        this.endTime = i2;
        this.electricityTypeValue = i3;
    }

    public int compareTo(TimeType timeType) {
        return a.a(this.startTime, timeType.startTime);
    }
}
