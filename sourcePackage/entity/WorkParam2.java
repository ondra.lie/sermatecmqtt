package com.sermatec.sunmate.entity;

import java.io.Serializable;

/* loaded from: classes.dex */
public class WorkParam2 implements Serializable {
    private String acidBatEOD;
    private String acidBatEqVol;
    private String acidBatFloatVol;
    private int backUpOrder;
    private int backUpOutEnable;
    private int batActive;
    private int batPowerAdjustable;
    private String batVolDownLimit;
    private int batVolSettable;
    private String batVolUpLimit;
    private String cPowerLimit;
    private String dPowerLimit;
    private String eSCCurUpLimit;
    private int meterMonitor;
    private String offGridSoc;
    private int parallelAddr;
    private int parallelFlag;
    private int parallelType;
    private int pvSourceChoice;
    private int shadowScan;
    private int sideWayEnable;
    private String socDiff;
    private int threePhase;

    public String getAcidBatEOD() {
        return this.acidBatEOD;
    }

    public String getAcidBatEqVol() {
        return this.acidBatEqVol;
    }

    public String getAcidBatFloatVol() {
        return this.acidBatFloatVol;
    }

    public int getBackUpOrder() {
        return this.backUpOrder;
    }

    public int getBackUpOutEnable() {
        return this.backUpOutEnable;
    }

    public int getBatActive() {
        return this.batActive;
    }

    public int getBatPowerAdjustable() {
        return this.batPowerAdjustable;
    }

    public String getBatVolDownLimit() {
        return this.batVolDownLimit;
    }

    public int getBatVolSettable() {
        return this.batVolSettable;
    }

    public String getBatVolUpLimit() {
        return this.batVolUpLimit;
    }

    public int getMeterMonitor() {
        return this.meterMonitor;
    }

    public String getOffGridSoc() {
        return this.offGridSoc;
    }

    public int getParallelAddr() {
        return this.parallelAddr;
    }

    public int getParallelFlag() {
        return this.parallelFlag;
    }

    public int getParallelType() {
        return this.parallelType;
    }

    public int getPvSourceChoice() {
        return this.pvSourceChoice;
    }

    public int getShadowScan() {
        return this.shadowScan;
    }

    public int getSideWayEnable() {
        return this.sideWayEnable;
    }

    public String getSocDiff() {
        return this.socDiff;
    }

    public int getThreePhase() {
        return this.threePhase;
    }

    public String getcPowerLimit() {
        return this.cPowerLimit;
    }

    public String getdPowerLimit() {
        return this.dPowerLimit;
    }

    public String geteSCCurUpLimit() {
        return this.eSCCurUpLimit;
    }

    public void setAcidBatEOD(String str) {
        this.acidBatEOD = str;
    }

    public void setAcidBatEqVol(String str) {
        this.acidBatEqVol = str;
    }

    public void setAcidBatFloatVol(String str) {
        this.acidBatFloatVol = str;
    }

    public void setBackUpOrder(int i) {
        this.backUpOrder = i;
    }

    public void setBackUpOutEnable(int i) {
        this.backUpOutEnable = i;
    }

    public void setBatActive(int i) {
        this.batActive = i;
    }

    public void setBatPowerAdjustable(int i) {
        this.batPowerAdjustable = i;
    }

    public void setBatVolDownLimit(String str) {
        this.batVolDownLimit = str;
    }

    public void setBatVolSettable(int i) {
        this.batVolSettable = i;
    }

    public void setBatVolUpLimit(String str) {
        this.batVolUpLimit = str;
    }

    public void setMeterMonitor(int i) {
        this.meterMonitor = i;
    }

    public void setOffGridSoc(String str) {
        this.offGridSoc = str;
    }

    public void setParallelAddr(int i) {
        this.parallelAddr = i;
    }

    public void setParallelFlag(int i) {
        this.parallelFlag = i;
    }

    public void setParallelType(int i) {
        this.parallelType = i;
    }

    public void setPvSourceChoice(int i) {
        this.pvSourceChoice = i;
    }

    public void setShadowScan(int i) {
        this.shadowScan = i;
    }

    public void setSideWayEnable(int i) {
        this.sideWayEnable = i;
    }

    public void setSocDiff(String str) {
        this.socDiff = str;
    }

    public void setThreePhase(int i) {
        this.threePhase = i;
    }

    public void setcPowerLimit(String str) {
        this.cPowerLimit = str;
    }

    public void setdPowerLimit(String str) {
        this.dPowerLimit = str;
    }

    public void seteSCCurUpLimit(String str) {
        this.eSCCurUpLimit = str;
    }
}
